package com.timesquare;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import com.timesquare.database.Database;
import com.timesquare.setting.CameraPreview;
import com.timessquare.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Goodies extends Activity{
	
	private LinearLayout setting;
	private FrameLayout qr_containner;
	private TextView menu, scanning, accessCamera, other_goodies;
	private ImageView QRscan;
	
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;
	ImageScanner scanner;	
	private boolean previewing = true;
	
	static {
        System.loadLibrary("iconv");
    } 
	
	// Camera Auto Focus
	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};
	
	// Camera Preview 
	PreviewCallback previewCb = new PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);
            
            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();
                
                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {
                	String code = sym.getData();
                	if (isAlphaDigit(code)){
                		Intent intent = new Intent(Goodies.this, GoodiesItem.class);
                    	intent.putExtra(Database.TAG_GOODIES_CODE, code);
                    	Goodies.this.startActivityForResult(intent, 1);
                	}else{
                		invalidQR();
                	}
                	
                }
            }
        }
    };
	
    // Camera Focus
    AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header);
		View.inflate(this, R.layout.goodies, (ViewGroup) findViewById(R.id.header));
	
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
		
		
		// Initialize the object
		setting = (LinearLayout) findViewById(R.id.header_setting);
		TextView setting_text = (TextView) findViewById(R.id.header_setting_text);
		TextView title = (TextView) findViewById(R.id.header_title);
		menu = (TextView) findViewById(R.id.header_menu);
		TextView warning = (TextView) findViewById(R.id.goodies_warning);
		qr_containner = (FrameLayout) findViewById(R.id.goodies_qr_container);
		TextView findQRTitle = (TextView) findViewById(R.id.goodies_findQRTitle);
		QRscan = (ImageView) findViewById(R.id.goodies_QRscan);
		scanning = (TextView) findViewById(R.id.goodies_scanning);
		accessCamera = (TextView) findViewById(R.id.goodies_accessCamera);
		other_goodies = (TextView) findViewById(R.id.goodies_otherGoodies);
		
		// Set Variable
		setting_text.setTypeface(font);
		title.setTypeface(font);
		menu.setTypeface(font);
		warning.setTypeface(font);
		findQRTitle.setTypeface(font);
		scanning.setTypeface(font);
		accessCamera.setTypeface(font);
		other_goodies.setTypeface(font);
		
		title.setText(R.string.mainmenu_goodies);
		
		QRscan.setVisibility(View.VISIBLE);
		scanning.setVisibility(View.GONE);
		accessCamera.setVisibility(View.VISIBLE);
		
		
		
		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
 		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_setting:
					Goodies.this.startActivity(new Intent(Goodies.this, Setting.class));
					break;
				case R.id.header_menu:
					Goodies.this.finish();
					break;
				case R.id.goodies_QRscan:
					QRscan.setVisibility(View.GONE);
					scanning.setVisibility(View.VISIBLE);
					accessCamera.setVisibility(View.GONE);
					// initial the camera
					mCamera = getCameraInstance();
					autoFocusHandler = new Handler();	
					
					scanner = new ImageScanner();
					scanner.setConfig(0, Config.X_DENSITY, 3);
					scanner.setConfig(0, Config.Y_DENSITY, 3);
					
					mPreview = new CameraPreview(getApplicationContext(), mCamera, previewCb, autoFocusCB);
					qr_containner.addView(mPreview);
					break;
				case R.id.goodies_otherGoodies:
					Goodies.this.startActivityForResult(new Intent(Goodies.this, GoodiesList.class), 1 );
					break;
				default:
					break;
				}
				
			}
		};
		setting.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		QRscan.setOnTouchListener(onTouchListener);
		other_goodies.setOnTouchListener(onTouchListener);
		
		setting.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
		QRscan.setOnClickListener(onClickListener);
		other_goodies.setOnClickListener(onClickListener);

		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		// if the camera is released, reset it
		if (mCamera == null && QRscan.getVisibility() == View.GONE){
			mCamera = getCameraInstance();
			autoFocusHandler = new Handler();		
			
			scanner = new ImageScanner();
			scanner.setConfig(0, Config.X_DENSITY, 3);
			scanner.setConfig(0, Config.Y_DENSITY, 3);
			
			mPreview = new CameraPreview(getApplicationContext(), mCamera, previewCb, autoFocusCB);
			qr_containner.addView(mPreview);
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		// release the camera
		releaseCamera();
		qr_containner.removeView(mPreview);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 1){
			Goodies.this.finish();
		}
	}
	
	// Create and Open Camera 
	public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e){
        }
        return c;
    }
	
	// Release the Camera
	private void releaseCamera(){
		if (mCamera != null){
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}
	
	// Check if the String contain digit and char only
	private boolean isAlphaDigit(String name) {
	    char[] chars = name.toCharArray();

	    for (char c : chars) {
	        if(!Character.isLetter(c) && !Character.isDigit(c)) {
	            return false;
	        }
	    }

	    return true;
	}
	
	private void invalidQR(){
		scanning.setText(getResources().getString(R.string.invalidQR));
		mCamera.setPreviewCallback(previewCb);		
		mCamera.startPreview();
		previewing = true;
		mCamera.autoFocus(autoFocusCB);   		
		Log.i("Goodies", "QR: invalid QR");
	}
	
	
}
