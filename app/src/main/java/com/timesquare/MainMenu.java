package com.timesquare;


import java.util.Locale;

import com.timesquare.database.Database;
import com.timesquare.setting.FontCache;
import com.timessquare.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Main Menu
 * - Go Calendar
 * - Go Shop'n dine
 * - Go What's up
 * - Go Goodies
 * - Go Member
 * - Access
 * - Setting
 * - Special event
 */
public class MainMenu extends Activity{
	
	private TextView calendar, shopndine, whatsup, goodies, member, carpark;
	private TextView setting_text, access, specialEvent;
	private LinearLayout setting;
	private ImageView lineCalendar, lineShopndine, lineWhatsup, lineGoodies, lineMember, lineCarpark,  disable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainmenu);
		
		// Get Intent
		Intent intent = getIntent();
		
		// Get Font Type
		Typeface font = FontCache.get("fonts/AGaramondPro-Regular.otf", this);
//		Log.i("MM", font.toString());
//				Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font));
		
		// Initialize all the object
		calendar = (TextView) findViewById(R.id.mainmenu_calendar);
		shopndine = (TextView) findViewById(R.id.mainmenu_shopndine);
		whatsup = (TextView) findViewById(R.id.mainmenu_whatsup);
		goodies = (TextView) findViewById(R.id.mainmenu_goodies);
		member = (TextView) findViewById(R.id.mainmenu_member);
		carpark = (TextView) findViewById(R.id.mainmenu_carpark);
		setting_text = (TextView) findViewById(R.id.mainmenu_setting_text);
		setting = (LinearLayout) findViewById(R.id.mainmenu_setting);
		access = (TextView) findViewById(R.id.mainmenu_access);
		specialEvent = (TextView) findViewById(R.id.mainmenu_specialEvent);
		lineCalendar = (ImageView) findViewById(R.id.mainmenu_calendar_underline);
		lineShopndine = (ImageView) findViewById(R.id.mainmenu_shopndine_underline);
		lineWhatsup = (ImageView) findViewById(R.id.mainmenu_whatsup_underline);
		lineGoodies = (ImageView) findViewById(R.id.mainmenu_goodies_underline);
		lineMember = (ImageView) findViewById(R.id.mainmenu_member_underline);
		lineCarpark = (ImageView) findViewById(R.id.mainmenu_carpark_underline);
		disable = (ImageView) findViewById(R.id.mainmenu_disable);
		
		// Set the object
		if (intent.getIntExtra(Database.TAG_SPECIAL_EVENT_STATUS, 0) != 1){
			specialEvent.setVisibility(View.GONE);
		}
		
		// Set Font Type
		calendar.setTypeface(font);
		shopndine.setTypeface(font);          
		whatsup.setTypeface(font);
		goodies.setTypeface(font);
		member.setTypeface(font);
		carpark.setTypeface(font);
		setting_text.setTypeface(font);
		access.setTypeface(font);
		specialEvent.setTypeface(font);
		
		// Set line animation
		TranslateAnimation animation = new TranslateAnimation(TranslateAnimation.ABSOLUTE, 0f, TranslateAnimation.ABSOLUTE, 400.0f, TranslateAnimation.RELATIVE_TO_PARENT, 0f, TranslateAnimation.RELATIVE_TO_PARENT, 0f);
		animation.setDuration(5000);
		animation.setRepeatCount(-1);
		animation.setRepeatMode(Animation.REVERSE);
		animation.setInterpolator(new LinearInterpolator());
		lineCalendar.setAnimation(animation);
		
		animation.setDuration(7000);
		lineGoodies.setAnimation(animation);
		
		animation.setDuration(6000);
		lineWhatsup.setAnimation(animation);
		
		animation = new TranslateAnimation(TranslateAnimation.ABSOLUTE, 0f, TranslateAnimation.ABSOLUTE, -300.0f, TranslateAnimation.RELATIVE_TO_PARENT, 0f, TranslateAnimation.RELATIVE_TO_PARENT, 0f);
		animation.setDuration(5000);
		animation.setRepeatCount(-1);
		animation.setRepeatMode(Animation.REVERSE);
		animation.setInterpolator(new LinearInterpolator());
		lineShopndine.setAnimation(animation);
		
		animation.setDuration(7000);
		lineMember.setAnimation(animation);

		animation.setDuration(5000);
		lineCarpark.setAnimation(animation);

		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
		View.OnClickListener handler = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch(v.getId()){
				case R.id.mainmenu_calendar:
					MainMenu.this.startActivityForResult(new Intent(MainMenu.this, Calendar.class), 2);
					break;
				case R.id.mainmenu_shopndine:
					MainMenu.this.startActivity(new Intent(MainMenu.this, ShopNDine.class));
					break;
				case R.id.mainmenu_whatsup:
					MainMenu.this.startActivity(new Intent(MainMenu.this, Whatsup.class));
					break;
				case R.id.mainmenu_goodies:
					MainMenu.this.startActivity(new Intent(MainMenu.this, GoodiesList.class));
					break;
				case R.id.mainmenu_member:
					MainMenu.this.startActivity(new Intent(MainMenu.this, Member.class));
					break;
				case R.id.mainmenu_carpark:
					MainMenu.this.startActivity(new Intent(MainMenu.this, Carpark.class));
					break;
				case R.id.mainmenu_setting:
					MainMenu.this.startActivity(new Intent(MainMenu.this, Setting.class));
					break;
//				case R.id.mainmenu_access:
//					MainMenu.this.startActivity(new Intent(MainMenu.this, Access.class));
//					break;
				case R.id.mainmenu_specialEvent:
//					MainMenu.this.startActivity(new Intent(MainMenu.this, SpecialEvent.class));
					MainMenu.this.startActivity(new Intent(MainMenu.this, SEChooser.class));
					break;		
				case R.id.mainmenu_disable:
					String url = "http://www.webforall.gov.hk/en/scheme/2015/judgingcriteria/mobile";
					if (Locale.getDefault().getLanguage().equals("zh")){
						url = "http://www.webforall.gov.hk/scheme/2015/judgingcriteria/mobile";
					}else{
						url = "http://www.webforall.gov.hk/en/scheme/2015/judgingcriteria/mobile";
					}
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(url));
					startActivity(i);
					break;
				default:
					break;
				}				
			}
		};
		calendar.setOnTouchListener(onTouchListener);
		shopndine.setOnTouchListener(onTouchListener);
		whatsup.setOnTouchListener(onTouchListener);          
		goodies.setOnTouchListener(onTouchListener);
		member.setOnTouchListener(onTouchListener);
		carpark.setOnTouchListener(onTouchListener);
		setting.setOnTouchListener(onTouchListener);
		access.setOnTouchListener(onTouchListener);
		specialEvent.setOnTouchListener(onTouchListener);	
		disable.setOnTouchListener(onTouchListener);
		
		calendar.setOnClickListener(handler);
		shopndine.setOnClickListener(handler);
		whatsup.setOnClickListener(handler);          
		goodies.setOnClickListener(handler);
		member.setOnClickListener(handler);
		carpark.setOnClickListener(handler);
		setting.setOnClickListener(handler);
		access.setOnClickListener(handler);
		specialEvent.setOnClickListener(handler);
		disable.setOnClickListener(handler);
	}
	

}
