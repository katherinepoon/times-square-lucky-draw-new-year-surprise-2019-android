package com.timesquare;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.timesquare.database.Database;
import com.timesquare.setting.*;
import com.timessquare.R;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

import cz.msebera.android.httpclient.Header;

/**
 * Created by sllin on 1/9/15.
 */
public class Catch100New extends Activity{

    WebView webview;
    String htmlTxt = "";

    private boolean scan_mode = false;
    FrameLayout qr_container, web_container;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;

    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;
    ImageScanner scanner;
    WebSettings webSettings;
    private boolean previewing = true;

    JsonHttpResponseHandler jsonHttpResponseHandler = new JsonHttpResponseHandler(){
        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Toast.makeText(Catch100New.this, responseString, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            try {
                // action: value= "push"(if need to go to next page)
                if(response.getString(Database.TAG_CATCH_JSON_ACTION).equals(Database.TAG_CATCH_JSON_PUSH)){
                    htmlTxt = response.getString(Database.TAG_CATCH_JSON_HTML);

                    // action: "alert" (if error happened and need to show an alert))
                }else if(response.getString(Database.TAG_CATCH_JSON_ACTION).equals(Database.TAG_CATCH_JSON_ALERT)){
                    Toast.makeText(Catch100New.this, response.getString(Database.TAG_CATCH_JSON_CONTENT), Toast.LENGTH_SHORT).show();
                }else if(response.getString(Database.TAG_CATCH_JSON_ACTION).equals(Database.TAG_CATCH_JSON_DEFAULT)){
                    Catch100New.this.startActivity(new Intent(Catch100New.this, SpecialEvent.class));
                    Catch100New.this.finish();
                }

                final String html64 = URLEncoder.encode(htmlTxt, Database.TAG_CATCH_CHARSET).replaceAll("\\+", " ");
                webViewHandler(html64);

            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.catch100new);

        qr_container = (FrameLayout) findViewById(R.id.catch_qr_container_new);
        web_container = (FrameLayout) findViewById(R.id.catch_qr_web_container);
        webview = (WebView) findViewById(R.id.catch_qr_webview);
        surfaceView = (SurfaceView) findViewById(R.id.catch_qr_surfaceView);


        initWebView();
        getWeb();

    }

    private void initWebView(){
        webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);


        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                final RequestParams params = new RequestParams();

                // "X" button
                if (url.startsWith(Database.TAG_CATCH_ACTION + Database.TAG_CATCH_DISMISS)) {
                    Catch100New.this.finish();
                    return true;

                    // Start button
                } else if (url.startsWith(Database.TAG_CATCH_ACTION + Database.TAG_CATCH_POST)) {
                    String android_id = "A" + Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    params.put(Database.TAG_CATCH_NEXT_PARAM1, Locale.getDefault().getLanguage());
                    params.put(Database.TAG_CATCH_NEXT_PARAM2, android_id);
                    loadWebPage(params, Database.TAG_CATCH_NEXT_URL);
                    return true;

                    // "Camera" button
                } else if (url.startsWith(Database.TAG_CATCH_ACTION + Database.TAG_CATCH_SCAN)) {

                    Log.i("CAMERA", "START");
                    releaseCamera();
                    startScan();
                    return true;
                }

                return super.shouldOverrideUrlLoading(view, url);
            }
        });


    }

    private void webViewHandler(final String htmlStr){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    webview.loadData(htmlStr, Database.TAG_CATCH_CHARFORMAT, Database.TAG_CATCH_CHARSET);
                }
            });

    }


    public void getWeb(){
        RequestParams params = new RequestParams();
        params.put(Database.TAG_CATCH_START_PARAM, Locale.getDefault().getLanguage());
        loadWebPage(params, Database.TAG_CATCH_START_URL);
    }



    private void loadWebPage(RequestParams params, String appendURL){

        Database.postQR(appendURL, params, jsonHttpResponseHandler);
    }


    // Camera Auto Focus
    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };

    // Camera Preview
    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();

                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {
                    String code = sym.getData();
                    String android_id =  "A" + Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

                    RequestParams params = new RequestParams();
                    params.put(Database.TAG_CATCH_SCAN_PARAM1, Locale.getDefault().getLanguage());
                    params.put(Database.TAG_CATCH_SCAN_PARAM2, android_id);
                    params.put(Database.TAG_CATCH_SCAN_PARAM3, code);
                    loadWebPage(params, Database.TAG_CATCH_SCAN_URL);
                    endScan();
                }
            }
        }
    };

    // Camera Focus
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };

    private void startScan(){
        // initial the camera
        mCamera = getCameraInstance();
        autoFocusHandler = new Handler();

        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        mPreview = new CameraPreview(getApplicationContext(), mCamera, previewCb, autoFocusCB);
        qr_container.addView(mPreview);

        web_container.setVisibility(View.GONE);
    }

    private void endScan(){
        releaseCamera();
        qr_container.removeAllViews();
        web_container.setVisibility(View.VISIBLE);
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e){
            e.printStackTrace();
        }
        return c;
    }

    // Release the Camera
    private void releaseCamera(){
        if (mCamera != null){
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // if the camera is released, reset it
        if (mCamera == null && scan_mode){
            mCamera = getCameraInstance();
            autoFocusHandler = new Handler();

            scanner = new ImageScanner();
            scanner.setConfig(0, Config.X_DENSITY, 3);
            scanner.setConfig(0, Config.Y_DENSITY, 3);

            mPreview = new CameraPreview(getApplicationContext(), mCamera, previewCb, autoFocusCB);
            qr_container.addView(mPreview);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // release the camera
        releaseCamera();
        qr_container.removeAllViews();
    }


}
