package com.timesquare;

import uk.co.senab.photoview.PhotoViewAttacher;

import com.sllin.imageloader.ImageLoader;
import com.timesquare.database.Database;
import com.timessquare.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WhatsupItem extends Activity{
	
//	private TextView menu;
//	private LinearLayout setting;
//	private ImageView image;
//	
//	private ImageLoader imageLoader;
//	PhotoViewAttacher attacher;
//	private String url = Database.PATH_IMAGE;
	
	private TextView back;
	private ImageView image;
	
	private ImageLoader imageLoader;
	PhotoViewAttacher attacher;
	private String url = Database.PATH_IMAGE;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.whatsup_image);
		
		
		image = (ImageView) findViewById(R.id.whatsup_image_img);
		back = (TextView) findViewById(R.id.whatsup_image_back);
		
		imageLoader = new ImageLoader(this);
		attacher = new PhotoViewAttacher(image);
		imageLoader.DisplayImage(url +"/"+ getIntent().getStringExtra(Database.TAG_WHATSUP_IMAGE), image, true);
		image.setContentDescription(getIntent().getStringExtra(Database.TAG_WHATSUP_CONTENT));
				
//		setContentView(R.layout.header);	
//		View.inflate(this, R.layout.image_viewer, (ViewGroup) findViewById(R.id.header));
//		
//		// Get Font Type
//		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
//		
//		// Initialize the object
//		setting = (LinearLayout) findViewById(R.id.header_setting);
//		TextView setting_text = (TextView) findViewById(R.id.header_setting_text);
//		TextView title = (TextView) findViewById(R.id.header_title);
//		menu = (TextView) findViewById(R.id.header_menu);
//		TextView warning = (TextView) findViewById(R.id.imageViewer_warning);
//		TextView hint = (TextView) findViewById(R.id.imageViewer_zoomHint);
//		image = (ImageView) findViewById(R.id.imageViewer_image);
//		
//		// set variable
//		title.setTypeface(font);
//		warning.setTypeface(font);
//		setting_text.setTypeface(font);
//		menu.setTypeface(font);
//		hint.setTypeface(font);
//		
//		title.setText(R.string.mainmenu_whatsup);
//		
//		imageLoader = new ImageLoader(this);
//		attacher = new PhotoViewAttacher(image);
//		imageLoader.DisplayImage(url +"/"+ getIntent().getStringExtra(Database.TAG_WHATSUP_IMAGE), image, true);
//		
 		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
 		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.whatsup_image_back:
					WhatsupItem.this.finish();
					break;
				default:
					break;
				}
				
			}
		};
		
		back.setOnTouchListener(onTouchListener);
		back.setOnClickListener(onClickListener);
		
//		setting.setOnTouchListener(onTouchListener);
//		menu.setOnTouchListener(onTouchListener);
//		setting.setOnClickListener(onClickListener);
//		menu.setOnClickListener(onClickListener);
		
		
	};

}
