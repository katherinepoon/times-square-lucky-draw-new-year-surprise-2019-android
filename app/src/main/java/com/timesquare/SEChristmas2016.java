package com.timesquare;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.timesquare.database.Database;
import com.timesquare.setting.FontCache;
import com.timesquare.setting.ImageProcess;
import com.timessquare.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

/**
 * Created by sllin on 10/11/2016.
 */

public class SEChristmas2016 extends Activity implements View.OnClickListener {

    ImageView flower, title, congratulation, price1, price2, price3, price4, back;
    LinearLayout drawABC;
    TextView tvPrice, tvDescription, license, datetime;

    View[] mainPageView;

    View[] drawPageView;

    Typeface typeface;

    TextView[] tvABC;

    final String LICENSE_NUMBER = "48057-48060";
    final int gray = Color.parseColor("#A7A9AB");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.christmas2016);

        flower = (ImageView) findViewById(R.id.mas2016_flower);
        title = (ImageView) findViewById(R.id.mas2016_title);
        congratulation = (ImageView) findViewById(R.id.mas2016_congratulation);
        price1 = (ImageView) findViewById(R.id.mas2016_price1);
        price2 = (ImageView) findViewById(R.id.mas2016_price2);
        price3 = (ImageView) findViewById(R.id.mas2016_price3);
        price4 = (ImageView) findViewById(R.id.mas2016_price4);
        back = (ImageView) findViewById(R.id.mas2016_back);
        drawABC = (LinearLayout) findViewById(R.id.mas2016_drawABC);
        tvPrice = (TextView) findViewById(R.id.mas2016_price_text);
        tvDescription = (TextView) findViewById(R.id.mas2016_price_description);
        license = (TextView) findViewById(R.id.mas2016_license);
        datetime = (TextView) findViewById(R.id.mas2016_datetime);

//        flower.setTag(R.drawable.flower_2016);
//        title.setTag(R.drawable.title2016);
//        congratulation.setTag(R.drawable.win2016);
//        price1.setTag(R.drawable.price500_2016);
//        price2.setTag(R.drawable.price2000_2016);
//        price3.setTag(R.drawable.price5000_2016);
//        price4.setTag(R.drawable.price8000_2016);
//        back.setTag(R.drawable.back2016);


        typeface = FontCache.get(getString(R.string.promo_2017font), this);

        mainPageView = new View[]{
                flower, title, price1, price2, price3, price4
        };

        drawPageView = new View[]{
                flower, congratulation, drawABC, tvPrice, tvDescription, datetime, back
        };

        setABC();
        tvPrice.setTypeface(typeface);
        datetime.setTypeface(typeface, Typeface.BOLD);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        setWidthHeight(flower, width /5, height * 1/3);
        setWidthHeight(title, width * 3/5, height * 1/3);
        setWidthHeight(congratulation, width * 3/5, height * 1/3);
        setWidthHeight(price1, width * 1/6, height * 1/4);
        setWidthHeight(price2, width * 1/6, height * 1/4);
        setWidthHeight(price3, width * 1/6, height * 1/4);
        setWidthHeight(price4, width * 1/4, height * 1/4);
        setWidthHeight(back, width *  1/7, height * 1/9);
        setWidthHeight(drawABC, width * 3/5, height * 1/6);
        setWidthHeight(tvPrice, ViewGroup.LayoutParams.WRAP_CONTENT, height * 2/8);
//        setWidthHeight(tvDescription, ViewGroup.LayoutParams.WRAP_CONTENT, height * 2/9);
//        setWidthHeight(license, width *1/4, height * 2/9);
        setWidthHeight(datetime, ViewGroup.LayoutParams.WRAP_CONTENT, height * 1/9);

//        hideItems(false, false);
        setAnimation(true);

        price1.setOnClickListener(this);
        price2.setOnClickListener(this);
        price3.setOnClickListener(this);
        price4.setOnClickListener(this);
        back.setOnClickListener(this);

        license.setText(String.format(Locale.getDefault(), getString(R.string.promo_license), LICENSE_NUMBER));

    }



    private void setABC(){
        int asciiA = 'A';
        int asciiI = 'I';
        tvABC = new TextView[asciiI - asciiA + 1];
        for (int i = asciiA; i <= asciiI; i++){
            TextView view = new TextView(this);

            Log.d("C2016setABC", String.valueOf((char)i) );
            view.setText(String.valueOf((char)i));
            view.setTextColor(gray);
            view.setTextSize(78f);
            view.setGravity(Gravity.CENTER);
            view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1));
            view.setTypeface(typeface);
            tvABC[i - asciiA] = view;
            drawABC.addView(view);
        }

//        for (TextView view : tvABC){
//
//            Log.d("C2016", view.getTextSize() + "");
//        }

        Log.d("C2016setABC", drawABC.getChildCount() + ":childCount");

    }

    private void updateAlphabatStatus(int number, final boolean isYours){

        if (number < 0 || number >= tvABC.length){
            Toast.makeText(this, R.string.unknownError, Toast.LENGTH_SHORT).show();
            onClick(back);
        }
        if (number == tvABC.length){
            number--;
        }
        final int NUMBER = number;

        tvABC[number].setVisibility(View.INVISIBLE);
        if (isYours){
            tvABC[number].setTextColor(Color.WHITE);
        }
        tvABC[number].postDelayed(new Runnable() {
            @Override
            public void run() {
                tvABC[NUMBER].setVisibility(View.VISIBLE);
            }
        }, (long)(500 + Math.random() * 500));
    }

    private void setWidthHeight(View v, int width, int height){
        ViewGroup.LayoutParams params = v.getLayoutParams();
        params.width = width;
        params.height = height;
        v.setLayoutParams(params);
    }

    private void hideItems(boolean isMainPageView, boolean setVisible){
//        View[] views = isMainPageView ? mainPageView : drawPageView;
        int effect = setVisible ? View.VISIBLE : View.INVISIBLE;
        for (View v : isMainPageView ? mainPageView : drawPageView){
            Log.d("C2016hideItems", "v");
            v.setVisibility(effect);
        }
    }

    private void setAnimation(boolean isMainPageView){
        final Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1100);

        View[] views = isMainPageView ? mainPageView : drawPageView;



        for (int i = 0; i < views.length; i++){
            final View v = views[i];
            v.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (v instanceof ImageView && ((ImageView) v).getDrawable() == null){
                        setImage((ImageView)v);
                    }
                    v.setVisibility(View.VISIBLE);
                    v.setAnimation(fadeIn);
                }
            }, isMainPageView ? 150 * i : 100 * i);
        }
    }

    private void setImage(ImageView v){
        Bitmap bitmap = ImageProcess.decodeBitmapFromResource(getResources(), (int)v.getTag(), v.getLayoutParams().width, v.getLayoutParams().height);
        if (bitmap != null) {
            (v).setImageBitmap(bitmap);
        }
    }

    public void confirmationBox(final int number){
        int[] strings = new int[]{
                -1, R.string.promo_2017_1chance, R.string.promo_2017_2chance, R.string.promo_2017_3chance, -1, R.string.promo_2017_5chance
        };
        int[] stringsZh = new int[]{
                -1, R.string.promo_2017_1chance_zh, R.string.promo_2017_2chance_zh, R.string.promo_2017_3chance_zh, -1, R.string.promo_2017_5chance_zh
        };

        if (number <= 0 || number > strings.length || number == 4){
            return;
        }

        new AlertDialog.Builder(this)
                .setMessage(String.format(getString(R.string.promo_confirm_box_2017), getString(strings[number]), getString(stringsZh[number])))
                .setPositiveButton(getString(R.string.promo_confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startDrawPrice(number);
                    }
                })
                .setNegativeButton(getString(R.string.promo_cancel), null)
                .show();
    }

    private TimerTask flashAlphabatTimer(String target, final boolean isYours){
        if (target == null || target.length() < 1){
            return new TimerTask() {
                @Override
                public void run() {

                }
            };
        }
        int num = target.toUpperCase().charAt(0) - 'A';
        return flashAlphabatTimer(num, isYours);
    }

    private TimerTask flashAlphabatTimer(final int target, final boolean isYours){
        return new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int number = target;
                        if (number < 0){
                            number = (int) (Math.random() * tvABC.length);
                        }
                        updateAlphabatStatus(number, isYours);
                    }
                });

            }
        };
    }

    private void startDrawPrice(int number){
        hideItems(true, false);
        drawABC.setVisibility(View.VISIBLE);

        TimerTask timerTask = flashAlphabatTimer(-1, false);

        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 150);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                timer.cancel();
            }
        };

//        for (View v : drawPageView){
//            if (v instanceof ImageView && ((ImageView) v).getDrawable() == null)
//            setImage((ImageView)v);
//        }

        new View(this).postDelayed(runnable, 10000);

        Database.winterDrawGet(Database.API_WINTER_DRAW_DRAW + "/" + number, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
//                    Log.d("C2016winterTemp", new String(responseBody));
                    JSONObject jsonObject = new JSONObject(new String(responseBody));
                    // check no error.
                    if (!jsonObject.optBoolean(Database.TAG_ERROR, true)){
//                    if (true){

                        // check price availability
                        if (jsonObject.optBoolean(Database.TAG_DRAW_SUCCESS)){
//                        if (true){
                            JSONArray jarr = jsonObject.optJSONArray(Database.TAG_DATA);
//                            if (true) {
                            if (jarr != null) {
                                String allPriceNo = "";
                                String allPriceName = "";
                                String allPriceNameZh = "";
                                String allRunningNumber = "";
                                String drawTime = "";
//                                for (int i = 0; i < 3; i++) {
                                for (int i = 0; i < jarr.length(); i++) {
                                    JSONObject jobj = jarr.optJSONObject(i);

                                    int prizeId = jobj.optInt(Database.TAG_PRIZE_ID) - 1;
                                    String runningNo = jobj.optString(Database.TAG_NEXT_RUNNING_NUM);

                                    drawTime = jobj.optString(Database.TAG_DRAW_CREATE_AT);

                                    String prizeNo = jobj.optString(Database.TAG_PRIZE_NUM);
                                    String prizeName = jobj.optString(Database.TAG_PRIZE_NAME);
                                    String prizeNameZh = jobj.optString(Database.TAG_PRIZE_NAME_ZH);

//                                    String runningNo = "123";

//                                    drawTime = "2017-09-08 12:13:00";

//                                    String prizeNo = "A";
//                                    String prizeName = "F&B";
//                                    String prizeNameZh = "禮卷";

                                    String delimiter = "", delimiterEn = "", delimiterZH = "", delimiterRunNo = "";
                                    if (i == jarr.length() - 2){
//                                    if (i == 3 - 2){
                                        delimiter = "&";
                                        delimiterRunNo = " & ";
                                        delimiterEn = " and ";
                                        delimiterZH = "及";
                                    }else if (i < jarr.length() - 2){
//                                    }else if (i < 3 - 2){
                                        delimiter = ",";
                                        delimiterRunNo = ", ";
                                        delimiterEn = ", ";
                                        delimiterZH = ", ";
                                    }

                                    allPriceNo += prizeNo + delimiter;
                                    allPriceName += prizeName + delimiterEn;
                                    allPriceNameZh += prizeNameZh + delimiterZH;
                                    allRunningNumber += prizeNo + runningNo + delimiterRunNo;

                                    new Timer().schedule(flashAlphabatTimer(prizeNo, true), 1000 + 500 * i);
                                }

                                datetime.setText(String.format(getString(R.string.promo_datetime), drawTime.split(" ")[0], drawTime.split(" ")[1], allRunningNumber));
                                tvDescription.setText(String.format(getString(R.string.promo_prize_item), allPriceNameZh, allPriceName));
                                tvPrice.setText(allPriceNo);

                                new View(SEChristmas2016.this).postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        timer.cancel();
                                        doneDraw();
                                    }
//                                }, 1000 + 500 * (3 + 1));
                                }, 1000 + 500 * (jarr.length() + 1));

                            }
                            return;
                        }else{
                            tvPrice.setText(getString(R.string.promo_out_of_quota));
                            timer.cancel();
                            tvPrice.setVisibility(View.VISIBLE);
                            return;
                        }

                    }
                } catch (Exception e) {
//                } catch (JSONException e) {
                    e.printStackTrace();
                }

                onFailure(statusCode, headers, responseBody, null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(SEChristmas2016.this, getString(R.string.unknownError), Toast.LENGTH_LONG).show();
                timer.cancel();
                back.setVisibility(View.VISIBLE);
            }
        });
    }

    private void reset(){
        for (TextView textView : tvABC){
            textView.setTextColor(gray);
        }
        tvPrice.setText("");
        tvDescription.setText("");
        datetime.setText("");
        drawABC.setAlpha(1f);
    }

    private void doneDraw(){
        for (View v : tvABC){
            v.setVisibility(View.VISIBLE);
        }
        drawABC.setAlpha(0.45f);
            setAnimation(false);
//        datetime.setVisibility(View.VISIBLE);
//        tvPrice.setVisibility(View.VISIBLE);
//        tvDescription.setVisibility(View.VISIBLE);
//        back.setVisibility(View.VISIBLE);
//        congratulation.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        int number = 1;
        switch (v.getId()){
            case R.id.mas2016_price1:
                number = 1;
                break;
            case R.id.mas2016_price2:
                number = 2;
                break;
            case R.id.mas2016_price3:
                number = 3;
                break;
            case R.id.mas2016_price4:
                number = 5;
                break;
            case R.id.mas2016_back:
                hideItems(false, false);
                setAnimation(true);
                reset();
                return;
            default:
                return;
        }
        confirmationBox(number);
    }
}
