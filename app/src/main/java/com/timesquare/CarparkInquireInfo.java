package com.timesquare;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.timesquare.database.Database;
import com.timessquare.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class CarparkInquireInfo extends Activity {

    private Button submitButton;
    private Button octopusRadiobutton;
    private Button creditCardRadiobutton;

    private EditText octopusNumField;
    private EditText creditcardNumField1;
    private EditText creditcardNumField2;
    private EditText carNumField;

    private LinearLayout resultLayout;
    private TextView resultTime;
    private TextView resultDura;
    private TextView resultResponseTime;
    private CarparkInquireInfo activity;

    private LinearLayout octopusInputLayout;
    private LinearLayout creditCardInputLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.header);
        View.inflate(this, R.layout.carpark_inquireinfo, (ViewGroup) findViewById(R.id.header));

        // Get Font Type
        Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font));


        // Initialize the object
        submitButton = (Button)findViewById(R.id.carpark_submitRequest);
        octopusRadiobutton = (Button)findViewById(R.id.carpark_radiobutton_octopus);
        creditCardRadiobutton = (Button)findViewById(R.id.carpark_radiobutton_creditcard);

        resultTime = (TextView)findViewById(R.id.carpark_result_timeText);
        resultDura = (TextView)findViewById(R.id.carpark_result_duraText);
        resultResponseTime = (TextView)findViewById(R.id.carpark_result_responseTime);

        carNumField = (EditText)findViewById(R.id.carpark_carNumInput);
        octopusNumField = (EditText)findViewById(R.id.carpark_octopusInput);
        creditcardNumField1 = (EditText)findViewById(R.id.carpark_creditcardInput1);
        creditcardNumField2 = (EditText)findViewById(R.id.carpark_creditcardInput2);

        octopusInputLayout = (LinearLayout)findViewById(R.id.carpark_octopusInputLayout);
        creditCardInputLayout = (LinearLayout)findViewById(R.id.carpark_creditcardInputLayout);
        resultLayout = (LinearLayout)findViewById(R.id.carpark_resultLayout);

        activity = this;


        //config header
        TextView title = (TextView) findViewById(R.id.header_title);
        TextView menu = (TextView) findViewById(R.id.header_menu);
        LinearLayout setting = (LinearLayout) findViewById(R.id.header_setting);
        setting.setVisibility(View.INVISIBLE);
        title.setText(R.string.mainmenu_carpark);
        menu.setText(R.string.close);
        title.setTypeface(font);
        menu.setTypeface(font);




        //hide result part
        resultLayout.setVisibility(View.GONE);

        //default select octopus
        octopusRadiobutton.setSelected(true);


        // Initialize the listener
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.header_menu:
                        CarparkInquireInfo.this.finish();
                        break;
                    case R.id.carpark_submitRequest:
                        submitOnClick();
                        break;
                    case R.id.carpark_radiobutton_creditcard:
                        selectRequiryInfo(InquireMode.CREDIT_CARD);
                        break;
                    case R.id.carpark_radiobutton_octopus:
                        selectRequiryInfo(InquireMode.OCTOPUS);
                        break;
                    default:
                        break;
                }

            }
        };



        menu.setOnClickListener(onClickListener);
        submitButton.setOnClickListener(onClickListener);
        octopusRadiobutton.setOnClickListener(onClickListener);
        creditCardRadiobutton.setOnClickListener(onClickListener);

    }

    private void selectRequiryInfo(InquireMode mode){
        if(mode.equals(InquireMode.CREDIT_CARD)){
            octopusRadiobutton.setSelected(false);
            creditCardRadiobutton.setSelected(true);
            octopusInputLayout.setVisibility(View.GONE);
            creditCardInputLayout.setVisibility(View.VISIBLE);
        }
        else{
            octopusRadiobutton.setSelected(true);
            creditCardRadiobutton.setSelected(false);
            creditCardInputLayout.setVisibility(View.GONE);
            octopusInputLayout.setVisibility(View.VISIBLE);
        }
        inquiryMode = mode;
    }

    private InquireMode inquiryMode = InquireMode.OCTOPUS;

    private enum InquireMode{
        OCTOPUS, CREDIT_CARD;
    }

    private void submitOnClick(){
        //Hide the soft keyboard
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);

        resultLayout.setVisibility(View.INVISIBLE);
        String cardStr = "";
        if(inquiryMode.equals(InquireMode.OCTOPUS)){
            cardStr = this.octopusNumField.getText().toString();
        }
        else{
            String cardStr1 = creditcardNumField1.getText().toString();
            String cardStr2 = creditcardNumField2.getText().toString();

            if(!cardStr1.isEmpty() || !cardStr2.isEmpty()) {
                cardStr = cardStr1 + "/" + cardStr2;
            }
        }
        String carNum = this.carNumField.getText().toString();

        if(cardStr.isEmpty() || carNum.isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.carpark_noInputError), Toast.LENGTH_SHORT).show();
            return;
        }

        if(isOnline()){
            new GetCarparkCheckingResult(this).execute(cardStr, carNum);
        }
        else{
            Log.i("CarparkInquireInfo", "No Internet Connection");
            Toast.makeText(this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
            return;
        }


    }

    String resultStr;


    private class GetCarparkCheckingResult extends AsyncTask<String, Void, String> {

        private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        private SimpleDateFormat sdf_en = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        private SimpleDateFormat sdf_zh = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
        private String NO_RECORD_STRING = "\"0\"";

        private String URL_OCT = Database.CARPARK_API_URL + Database.API_CARPARK_BY_OCTOPUS;
        private String URL_CREDITCARD = Database.CARPARK_API_URL + Database.API_CARPARK_BY_CREADITCARD;

        private ProgressDialog loading;

        public GetCarparkCheckingResult(Context context) {
            loading = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            // show the loading dialog
            loading.setMessage(getResources().getString(R.string.loading));
            loading.setCancelable(false);
            loading.show();
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {
            String cardStr = params[0];
            String carNum = params[1];
            String urlString = "";
            if(inquiryMode.equals(InquireMode.OCTOPUS)){
                urlString = URL_OCT + "/" + carNum + "/" + cardStr;
            }
            else{
                urlString = URL_CREDITCARD + "/" + carNum + "/" + cardStr;
            }

            HttpGet httpGet = new HttpGet(urlString);
            HttpClient httpClient = new DefaultHttpClient();
            String result = "error";

            try {
                // Send request and get data
                HttpResponse httpResponse = httpClient.execute(httpGet);
                result = EntityUtils.toString(httpResponse.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }


        private void showErr(){
            Toast.makeText(activity, getResources().getString(R.string.carpark_noRecordFoundError), Toast.LENGTH_SHORT).show();
            resultLayout.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(String result) {
            if (loading.isShowing()) {
                loading.dismiss();
            }

            // Check if the information get correctly
            if (!result.equals("error")) {
                if (!result.equals(NO_RECORD_STRING) && result.length() < 35) {  //recoard found
                    result = result.substring(1, result.length() - 1);
                    String[] resultData = result.split(";");
                    if(resultData == null || resultData.length < 3)
                    {
                        showErr();
                        super.onPostExecute(result);
                        return;
                    }

                    //cal time
                    Calendar parkTime = Calendar.getInstance();
                    Calendar responseTime = Calendar.getInstance();
                    int min = Integer.parseInt(resultData[1]);
                    int hour = min / 60;
                    min = min % 60;

                    try {
                        parkTime.setTime(sdf.parse(resultData[0]));
                        responseTime.setTime(sdf.parse(resultData[2]));
                    } catch (ParseException e) {
                        showErr();
                        super.onPostExecute(result);
                        return;
                    }

                    //check language setting
                    SharedPreferences sharedPreferences = getSharedPreferences("setting", Context.MODE_PRIVATE);
                    String langauge = sharedPreferences.getString(com.timesquare.setting.Setting.langauge, "en");   //en / zh

                    if(langauge.equals("zh")){
                        resultTime.setText(sdf_zh.format(parkTime.getTime()));
                        resultResponseTime.setText(sdf_zh.format(responseTime.getTime()));
                        resultDura.setText(hour + "小時" + min + "分鐘");
                    }
                    else{
                        resultTime.setText(sdf_en.format(parkTime.getTime()));
                        resultResponseTime.setText(sdf_en.format(responseTime.getTime()));
                        resultDura.setText(hour + " hours " + min + " minutes");
                    }
                    resultLayout.setVisibility(View.VISIBLE);
                }
                else{
                    showErr();
                }
            }
            else{
                Log.i("CarparkInquireInfo", "Get CarparkInquireInfo: Get Response Error");
                showErr();
            }
            super.onPostExecute(result);
        }

    }

    /**
     * Check if there are internet connection
     * @return boolean
     */
    public boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

}
