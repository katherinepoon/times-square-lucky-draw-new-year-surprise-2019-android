package com.timesquare

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.widget.TextViewCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.*
import android.widget.*
import com.loopj.android.http.AsyncHttpResponseHandler
import com.squareup.picasso.Picasso
import com.timesquare.database.Database
import com.timesquare.setting.FontCache
import com.timessquare.R
import cz.msebera.android.httpclient.Header
import org.json.JSONObject
import android.view.animation.AlphaAnimation
import android.view.animation.DecelerateInterpolator
import java.util.*


/**
 *
 * Created by sllin on 16/10/2017.
 */

class SEChristmas2017 : AppCompatActivity(), View.OnClickListener{

    private val root by lazy { RelativeLayout(this) }
    private val removableContent by lazy { RelativeLayout(this) }
    private val titleLayout by lazy { LinearLayout(this) }
    private val congratLayout by lazy { LinearLayout(this) }
    private val abcLayout by lazy { LinearLayout(this) }
    private val prizeSelectLayout by lazy { LinearLayout(this) }
    private val prizeCap by lazy { TextView(this) }
    private val prizeDetail by lazy { TextView(this) }
    private val back by lazy { ImageView(this) }

    private val dimen : DisplayMetrics by lazy { initial() }

    private val tvABC = ArrayList<TextView>()
    private val prizeDate by lazy { TextView(this) }

    private val LICENSE = "049745 - 8"

    private val prizeArrRes = intArrayOf( -1, R.drawable.c2017_gift1, R.drawable.c2017_gift2, R.drawable.c2017_gift3, -1, R.drawable.c2017_gift4 )
    private val typeface by lazy { FontCache.get(getString(R.string.promo_2017font), this) }
    private val typeface2 by lazy { FontCache.get(getString(R.string.promo_2017font2), this) }

    private fun initial() : DisplayMetrics {
        val dim = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(dim)
        return dim
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(root)

        styling()

        creatingFirstPage()

        creatingDrawPage()

        creatingThirdPage()

        backToHome()
    }

    private fun styling(){

        removableContent.layoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        titleLayout.orientation = LinearLayout.VERTICAL
        congratLayout.orientation = LinearLayout.VERTICAL
        abcLayout.orientation = LinearLayout.HORIZONTAL

        val padding = dimen.widthPixels * 29 / 900
        titleLayout.setPadding(padding, padding, padding, padding)
        titleLayout.gravity = Gravity.CENTER

        val titleParam = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        titleParam.addRule(RelativeLayout.CENTER_HORIZONTAL)
        titleLayout.layoutParams = titleParam

        abcLayout.gravity = Gravity.CENTER
        val abcParam = RelativeLayout.LayoutParams(dimen.widthPixels * 18/19, ViewGroup.LayoutParams.WRAP_CONTENT)
        abcParam.addRule(RelativeLayout.CENTER_IN_PARENT)
        abcLayout.layoutParams = abcParam

        val congratParam = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        congratLayout.layoutParams = congratParam
        congratLayout.gravity = Gravity.CENTER
        congratLayout.setPadding(padding, 3*padding, padding, 0)

        prizeSelectLayout.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, dimen.widthPixels *13/36)
        val prizeSelectParam = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        prizeSelectParam.addRule(RelativeLayout.CENTER_VERTICAL)
        prizeSelectLayout.layoutParams = prizeSelectParam
        prizeSelectLayout.setPadding(padding, padding, padding, padding)

        prizeDate.typeface = typeface
        prizeDate.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14f)
        prizeDate.setTextColor(Color.BLACK)
        prizeDate.setBackgroundColor(Color.WHITE)
        val dateParam = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dateParam.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        dateParam.addRule(RelativeLayout.ALIGN_PARENT_END)
        dateParam.rightMargin = padding/3
        dateParam.bottomMargin = padding/3
        prizeDate.layoutParams = dateParam

//        prizeCap.typeface = typeface2
        prizeCap.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 90f)
        prizeCap.gravity = Gravity.CENTER
        prizeCap.setTextColor(Color.parseColor("#FBB643"))
        prizeCap.id = View.generateViewId()
        prizeCap.setBackgroundColor(Color.argb(51, 0,0,0))
        val capParam = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        capParam.addRule(RelativeLayout.CENTER_IN_PARENT)
        prizeCap.layoutParams = capParam

//        prizeDetail.typeface = typeface2
        prizeDetail.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 17f)
        prizeDetail.gravity = Gravity.CENTER
        prizeDetail.setPadding(padding, 0, padding, 0)
        prizeDetail.id = View.generateViewId()
        prizeDetail.setTextColor(Color.parseColor("#FBB643"))
        prizeDetail.setLineSpacing(1f, 1.2f)
        val detailParam = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        detailParam.addRule(RelativeLayout.BELOW, prizeCap.id)
        prizeDetail.layoutParams = detailParam

        val backParam = RelativeLayout.LayoutParams(dimen.widthPixels * 35/256, dimen.widthPixels * 35/256 *3/7)
        backParam.topMargin = dimen.widthPixels * 10/256
        backParam.addRule(RelativeLayout.BELOW, prizeDetail.id)
        backParam.addRule(RelativeLayout.CENTER_HORIZONTAL)
        back.layoutParams = backParam
        Picasso.with(this).load(R.drawable.c2017_back).into(back)
        back.setBackgroundColor(Color.parseColor("#2A2063"))
        back.tag = R.drawable.c2017_back
        back.setOnClickListener(this)

    }

    private fun creatingFirstPage(){

        val background = ImageView(this)
        Picasso.with(this).load(R.drawable.c2017_bg).resize(dimen.widthPixels, dimen.heightPixels).into(background)
        root.addView(background)

        val licenseView = TextView(this)
        licenseView.typeface = typeface
        licenseView.textSize = 9f
        licenseView.setTextColor(Color.BLACK)
        licenseView.setPadding(dimen.widthPixels * 29/900, 0,0, dimen.widthPixels * 15/900)
        licenseView.text = String.format(getString(R.string.promo_license), LICENSE)
        val licenseParam = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        licenseParam.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        licenseView.layoutParams = licenseParam
        root.addView(licenseView)

        root.addView(removableContent)

        val title1 = ImageView(this)
        Picasso.with(this).load(R.drawable.c2017_title1).resize(dimen.widthPixels * 29/50, dimen.widthPixels * 29/50 * 262/1380).into(title1)

        val title2 = ImageView(this)
        Picasso.with(this).load(R.drawable.c2017_title2).resize(dimen.widthPixels * 14/25, dimen.widthPixels * 14/25 * 40/1372).into(title2)

        titleLayout.addView(title1)
        titleLayout.addView(title2)

        for (i in 0 until prizeArrRes.size){
            if (prizeArrRes[i] == -1){
                continue
            }
            val img = ImageView(this)
            val param = LinearLayout.LayoutParams(dimen.widthPixels *17/90, dimen.widthPixels *17/90 * 190/201)

//            if (prizeSelectLayout.childCount % 4 == 0){
                param.gravity = Gravity.BOTTOM
//            }else if (prizeSelectLayout.childCount % 2 == 0){
//                param.gravity = Gravity.TOP
//            }else {
//                param.gravity = Gravity.CENTER
//            }
            img.layoutParams = param

            Picasso.with(this).load(prizeArrRes[i])
//                    .resize(dimen.widthPixels *17/90, dimen.heightPixels /3)
//                    .onlyScaleDown()
                    .fit().into(img)

            // set OnClick
            img.tag = i
            img.setOnClickListener(this)

            prizeSelectLayout.addView(img)

            val lines = ImageView(this)
            val lineParam = LinearLayout.LayoutParams(0, dimen.heightPixels / 5, 1f)
            lineParam.gravity = Gravity.CENTER
            lines.layoutParams = lineParam
            Picasso.with(this).load(R.drawable.c2017_line).resize(2, dimen.heightPixels / 5).onlyScaleDown().into(lines)

            prizeSelectLayout.addView(lines)
        }
        
        // remove last line
        if (prizeSelectLayout.childCount % 2 == 0){
            prizeSelectLayout.removeViewAt(prizeSelectLayout.childCount - 1)
        }


    }

    private fun flashAlphabetTimer(target: String?, isYours: Boolean): TimerTask {
        if (target == null || target.isEmpty()) {
            return object : TimerTask() {
                override fun run() {

                }
            }
        }
        val num = target.toUpperCase()[0] - 'A'
        return flashAlphabetTimer(num, isYours)
    }

    private fun flashAlphabetTimer(target: Int, isYours: Boolean): TimerTask {
        return object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    var number = target
                    if (number < 0 || number >= tvABC.size) {
                        number = (Math.random() * tvABC.size).toInt()
                    }
                    updateAlphabetStatus(number, isYours)
                }

            }
        }
    }

    private fun creatingDrawPage(){
        val asciiA: Int = 'A'.toInt()
        val asciiI: Int = 'Q'.toInt()
        for (i in asciiA..asciiI) {
            val view = AutoResizeTextView(this)
//            view.layoutParams = LinearLayout.LayoutParams(dimen.widthPixels / 17, dimen.heightPixels * 130/768)
            view.layoutParams = LinearLayout.LayoutParams(dimen.widthPixels / 20, dimen.widthPixels / 20 *13/10)

            view.text = i.toChar().toString()
            view.setTextColor(Color.WHITE)

            view.alpha = 0.6f
            view.maxLines = 1
            view.textSize = 120f

            view.gravity = Gravity.CENTER

//            view.setTypeface(typeface2, Typeface.BOLD)
            view.typeface = typeface2
            tvABC.add(i - asciiA, view)
            abcLayout.addView(view)
        }

    }

    private fun creatingThirdPage(){
        val congratImage = ImageView(this)
        congratImage.layoutParams = LinearLayout.LayoutParams(dimen.widthPixels * 6/10, dimen.widthPixels *6/10 * 93/682)
        Picasso.with(this).load(R.drawable.c2017_congrat1).into(congratImage)

        val congratImage2 = ImageView(this)
        val image2Param = LinearLayout.LayoutParams(dimen.widthPixels /6, dimen.widthPixels /6 *37/128)
        image2Param.bottomMargin = dimen.heightPixels /11
        congratImage2.layoutParams = image2Param
        Picasso.with(this).load(R.drawable.c2017_congrat2).into(congratImage2)

        congratLayout.addView(congratImage)
        congratLayout.addView(congratImage2)

    }

    private fun updateAlphabetStatus(number: Int, isYours: Boolean) {

        var number = number

        if (number < 0 || number >= tvABC.size) {
            Toast.makeText(this, R.string.unknownError, Toast.LENGTH_SHORT).show()
            removableContent.addView(back)
        }
        if (number == tvABC.size) {
            number--
        }

        if (tvABC[number].alpha < 1f) {
            tvABC[number].visibility = View.INVISIBLE
        }
        if (isYours) {
            tvABC[number].alpha = 1f
            tvABC[number].visibility = View.VISIBLE

        }
        tvABC[number].postDelayed({ tvABC[number].visibility = View.VISIBLE }, (500 + Math.random() * 500).toLong())
    }

    private fun drawPrize(times : Int){

        removableContent.removeAllViews()

        removableContent.addView(titleLayout)
        removableContent.addView(abcLayout)
        removableContent.addView(prizeCap)

        prizeCap.text = ""
        prizeDetail.text = ""

        for (v in tvABC) {
            v.visibility = View.VISIBLE
            v.alpha = 0.6f
        }

        val timerTask = flashAlphabetTimer(-1, false)

        val timer = Timer()
        timer.scheduleAtFixedRate(timerTask, 0, 150)

//        val runnable = Runnable { timer.cancel() }

        Database.winterDrawGet(Database.API_WINTER_DRAW_DRAW + "/" + times, object : AsyncHttpResponseHandler(){
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                if (responseBody == null){
                    return onFailure(statusCode, headers, responseBody, null)
                }
                val res = String(responseBody)
                val json = JSONObject(res)

                if (!json.optBoolean(Database.TAG_ERROR, true)){
                    if (json.optBoolean(Database.TAG_DRAW_SUCCESS, false)){

                        val data = json.optJSONArray(Database.TAG_DATA)

                        var allPriceNo = ""
                        var allPriceName = ""
                        var allPriceNameZh = ""
                        var allRunningNumber = ""
                        var drawTime = ""

                        for (i in 0 until data.length()){
                            val jobj = data.optJSONObject(i)

                            val runningNo = jobj.optString(Database.TAG_NEXT_RUNNING_NUM)

                            drawTime = jobj.optString(Database.TAG_DRAW_CREATE_AT)

                            val prizeNo = jobj.optString(Database.TAG_PRIZE_NUM)
                            val prizeName = jobj.optString(Database.TAG_PRIZE_NAME)
                            val prizeNameZh = jobj.optString(Database.TAG_PRIZE_NAME_ZH)

                            var delimiter = ""
                            var delimiterEn = ""
                            var delimiterZH = ""
                            var delimiterRunNo = ""

                            if (i == data.length() - 2) {
                                delimiter = "&"
                                delimiterRunNo = " & "
                                delimiterEn = " and "
                                delimiterZH = "及"
                            } else if (i < data.length() - 2) {
                                delimiter = ","
                                delimiterRunNo = ", "
                                delimiterEn = ", "
                                delimiterZH = ", "
                            }

                            allPriceNo += prizeNo + delimiter
                            allPriceName += prizeName + delimiterEn
                            allPriceNameZh += prizeNameZh + delimiterZH
                            allRunningNumber += prizeNo + runningNo + delimiterRunNo

//                            val currentPriceReceived = allPriceNo

//                            Timer().schedule(flashAlphabetTimer(prizeNo, true), (1000 + 1000 * i).toLong())
//                            Timer().schedule(object : TimerTask() {
//                                override fun run() {
//                                    runOnUiThread{
//                                        prizeCap.text = currentPriceReceived
//                                    }
//
//                                }
//                            }, (1000 + 1000 * i).toLong())
                        }

                        prizeDate.text = (String.format(getString(R.string.promo_datetime),
                                drawTime.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0],
                                drawTime.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1],
                                allRunningNumber))

                        prizeDetail.text = (String.format(getString(R.string.promo_prize_item), allPriceNameZh, allPriceName))

                        View(this@SEChristmas2017).postDelayed({
                            timer.cancel()
                            doneDraw()
                            prizeCap.text = (allPriceNo)

                        }, 2000)
//                        }, (1000 + 1000 * (data.length() -1)).toLong())


                    }
                    else {
                        // all prize drawn
                        prizeCap.text = ""
                        prizeDetail.text = (getString(R.string.promo_out_of_quota))
                        timer.cancel()

                        removableContent.addView(prizeDetail)

                    }
                }
                else {
                    Toast.makeText(this@SEChristmas2017, json.optString(Database.TAG_MESSAGE, "Server error"), Toast.LENGTH_LONG).show()
                    timer.cancel()
                    prizeDetail.text = ""
                    removableContent.addView(prizeDetail)
                    removableContent.addView(back)
                }
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                timer.cancel()
                Toast.makeText(this@SEChristmas2017, "No connection to server", Toast.LENGTH_LONG).show()
                prizeDetail.text = ""
                removableContent.addView(prizeDetail)
                removableContent.addView(back)

            }
        })
    }

    private fun doneDraw() {
        for (v in tvABC) {
            v.visibility = View.VISIBLE
//            if (v.alpha < 1f) {
                v.alpha = 0.2f
//            }else {
//                v.alpha = 0.7f
//            }
        }
        removableContent.removeAllViews()

        removableContent.postDelayed({
            removableContent.addView(abcLayout)
            removableContent.addView(congratLayout)
            removableContent.addView(prizeCap)
            removableContent.addView(prizeDetail)
            removableContent.addView(back)
            removableContent.addView(prizeDate)
        }, 100)


    }

    private fun backToHome(){
        removableContent.removeAllViews()

        setAnimation(titleLayout)
        removableContent.addView(titleLayout)

        Timer().schedule(object :TimerTask(){
            override fun run() {
                runOnUiThread{
                    setAnimation(prizeSelectLayout)
                    removableContent.addView(prizeSelectLayout)
                }
            }
        }, 500L)
    }

    private fun setAnimation(v: View){
        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator() //add this
        fadeIn.duration = 1100
        v.animation = fadeIn
    }


    override fun onClick(v: View?) {
        if (v?.tag == null){
            return
        }

        val prizeIndex = v.tag as? Int ?: -1
        if (prizeIndex > -1 && prizeIndex < prizeArrRes.size){
            val dialogArr = intArrayOf(-1, R.string.promo_2017_1chance, R.string.promo_2017_2chance, R.string.promo_2017_3chance, -1, R.string.promo_2017_5chance)
            val dialogArrZh = intArrayOf(-1, R.string.promo_2017_1chance_zh, R.string.promo_2017_2chance_zh, R.string.promo_2017_3chance_zh, -1, R.string.promo_2017_5chance_zh)

            val message = String.format(getString(R.string.promo_confirm_box_2017), getString(dialogArr[prizeIndex]), getString(dialogArrZh[prizeIndex]))

            AlertDialog.Builder(this)
                    .setMessage(message)
                    .setPositiveButton(R.string.promo_confirm, { _, _ ->
                        drawPrize(prizeIndex)
                    })
                    .setNegativeButton(R.string.promo_cancel, null)
                    .create()
                    .show()


        }


        when(v.tag){
            R.drawable.c2017_back ->{
                backToHome()
            }
            else -> {
                return
            }
        }
    }


}