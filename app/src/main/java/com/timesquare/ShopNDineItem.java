package com.timesquare;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.sllin.imageloader.ImageLoader;
import com.timesquare.database.Database;
import com.timesquare.setting.ViewPagerAdapter;
import com.timessquare.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class ShopNDineItem extends Activity{
	
	private TextView back, menu, shop_title, shop_number;
	private LinearLayout container;
	private Intent intent;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header_withback);
		View.inflate(this, R.layout.shopndine_item, (ViewGroup) findViewById(R.id.header_withback));
		
		// Get intent 
		intent = getIntent();
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 		
		
		// Initialize the object
		back = (TextView) findViewById(R.id.header_withback_back);
		TextView title = (TextView) findViewById(R.id.header_withback_title);
		menu = (TextView) findViewById(R.id.header_withback_menu);
		shop_title = (TextView) findViewById(R.id.shopndine_item_title);
		shop_number = (TextView) findViewById(R.id.shopndine_item_number);
		container = (LinearLayout) findViewById(R.id.shopndine_item_container);
		
		// Set Variable
		title.setTypeface(font);
		back.setTypeface(font);
		menu.setTypeface(font);
		shop_title.setTypeface(font);
		shop_number.setTypeface(font);
		
		title.setText(R.string.mainmenu_shopndine);
		
		if (!intent.getExtras().getString(Database.TAG_SHOP_ID).equals(null)){
			if (isOnline()){
				new getShop(this, intent.getExtras().getString(Database.TAG_SHOP_ID)).execute("");		
			}else{
				Log.i("ShopNDineItem", "No Internet Connection");
				Toast.makeText(this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
			}
				
		}
		
		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
 		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_withback_back:
					ShopNDineItem.this.finish();
					break;
				case R.id.header_withback_menu:
					setResult(1);
					ShopNDineItem.this.finish();
					break;
				default:
					break;
				}
				
			}
		};
		
		back.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		
		back.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);

	}
	
	/**
	 * Check if there are internet connection
	 * @return boolean
	 */
	public boolean isOnline(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	
	private class getShop extends AsyncTask<String, Void, String>{
		
		private Context context;
		private String URL = Database.DATABASE_URL + Database.API_SHOP;
		private ImageLoader imageLoader;
		private String url_image = Database.PATH_IMAGE;
		private ProgressDialog loading;
		
		public getShop(Context context, String shop_id) {
			this.context = context;
			URL += "/"+shop_id;
			loading = new ProgressDialog(context);
			this.imageLoader = new ImageLoader(context);
		}
		
		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(context.getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";
			
			try {
				// Send request and get data
				HttpResponse httpResponse = httpClient.execute(httpGet);
				result = EntityUtils.toString(httpResponse.getEntity());				
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {			
			if (loading.isShowing()){
				loading.dismiss();
			}
			try{
				// Check if the information get correctly
				if (!result.equals("error")){
					JSONObject jsonObject = new JSONObject(result);
					// Check if there are error in the response
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						// Get and write the goodies information
						shop_title.setText(jsonObject.getString(Database.TAG_SHOP_NAME));
						shop_number.setText(jsonObject.getString(Database.TAG_SHOP_NUMBER));		
						
						String imagelist = jsonObject.getString(Database.TAG_SHOP_IMAGE);
						String[] image = imagelist.split(",");
						for (int i = 0 ; i< image.length ; i++){
							ImageView imageView = new ImageView(context);
							LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
							layoutParams.setMargins(5, 5, 5, 5);
							layoutParams.gravity = Gravity.CENTER;
							imageView.setLayoutParams(layoutParams);
							imageLoader.DisplayImage(url_image + "/" + image[i], imageView);
							imageView.setContentDescription(getString(R.string.shop_image));
							container.addView(imageView);
						}
						TextView textView = new TextView(context);
						textView.setTextColor(context.getResources().getColor(R.color.ThemeColor_DeepGreen));
						if (Locale.getDefault().getLanguage() == "zh"){
							textView.setText(jsonObject.getString(Database.TAG_SHOP_DESCRIPTION_CHI));
						}else{
							textView.setText(jsonObject.getString(Database.TAG_SHOP_DESCRIPTION_ENG));						
						}
						container.addView(textView);						
											
					}else{
						Log.i("ShopNDine", "Get ShopNDine: "+jsonObject.getString(Database.TAG_MESSAGE));
					}
				}else{
					Log.i("ShopNDine", "Get ShopNDine: Get Response Error");
				}
			}catch (JSONException e) {
				e.printStackTrace();
			}	
			super.onPostExecute(result);
		}
	
	}

}
