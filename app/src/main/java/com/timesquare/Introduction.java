package com.timesquare;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.timesquare.database.Database;
import com.timesquare.localdatabase.LocalDatabaseHelper;
import com.timesquare.setting.Setting;
import com.timessquare.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

/**
 * Loading Page
 * - Load Special Event
 * - Move to the Main Menu
 *
 *
 * UPDATE 20180528 (Jethro):
 * - go straight to NYS2017.kt
 */
public class Introduction extends Activity{

	private double critical_version = 0;

	private boolean isShopLoaded =false;
	private boolean isSpecialEventLoaded = false;

	private Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		setContentView(R.layout.introduction);

		SharedPreferences prefs = getSharedPreferences(com.timesquare.setting.Setting.filename, MODE_PRIVATE);

		Locale locale = new Locale(prefs.getString(Setting.langauge, "en"));
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());


//		final Handler handler = new Handler();
//		handler.postDelayed(new Runnable() {
//
//	        public void run() {
//	        	// get Special Event and go to the main menu
//				if (isOnline()){
//					new getSpecialEvent(Introduction.this).execute("");
//					new getShop(Introduction.this).execute("");
//				}else{
//					Log.i("Introduction", "No Internet Connection");
//					Toast.makeText(Introduction.this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
//				}
//	        }
//	    }, 1000);


		if (isOnline()){
			new getVersion(this).execute("");
		}else{
			Log.i("Introduction", "No Internet Connection");
			Toast.makeText(Introduction.this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
		}

	}

	private void checkVersion(){

		PackageInfo pInfo;
		double version = 0;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			version = Double.parseDouble(pInfo.versionName);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (critical_version != 0.0 && version < critical_version){
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			// set title
			alertDialogBuilder.setTitle(this.getResources().getString(R.string.update));
			// set dialog message
			alertDialogBuilder
					.setCancelable(true);
			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();
			// show it
			alertDialog.show();
		}else{
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {

				public void run() {
					// get Special Event and go to the main menu
					if (isOnline()){
						new getSpecialEvent(Introduction.this).execute("");
						new getShop(Introduction.this).execute("");
					}else{
						Log.i("Introduction", "No Internet Connection");
						Toast.makeText(Introduction.this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
					}
				}
			}, 1000);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK){

		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Check if there are internet connection
	 * @return boolean
	 */
	public boolean isOnline(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	private class getVersion extends AsyncTask<String, 	Void, String>{

		private Context context;
		private String URL = Database.DATABASE_URL + Database.API_VERSION;
		private ProgressDialog loading;

		public getVersion(Context context) {
			this.context = context;
			loading = new ProgressDialog(Introduction.this);
		}

		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(context.getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			//get event JSON from server
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";

			try {
				HttpResponse httpResponse = httpClient.execute(httpGet);
				StatusLine statusLine = httpResponse.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					result = EntityUtils.toString(httpResponse.getEntity());
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// if no error, write event data to the local database
			if(!result.equals("error")){
				try {
					JSONObject jsonObject = new JSONObject(result);
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						critical_version = Double.parseDouble(jsonObject.getString(Database.TAG_ANDROID_CRITICAL_VERSION));
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			checkVersion();
			if (loading.isShowing()){
				loading.dismiss();
			}
		}

	}

	private class getShop extends AsyncTask<String, Void, String>{

		private Context context;
		private String URL = Database.DATABASE_URL + Database.API_SHOP;
		private ProgressDialog loading;

		private LocalDatabaseHelper db;

		public getShop(Context context) {
			this.context = context;
			db = new LocalDatabaseHelper(context);
			loading = new ProgressDialog(Introduction.this);
		}

		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(context.getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			//get event JSON from server
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";

			try {
				HttpResponse httpResponse = httpClient.execute(httpGet);
				StatusLine statusLine = httpResponse.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					result = EntityUtils.toString(httpResponse.getEntity());
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// if no error, write event data to the local database
			if(!result.equals("error")){
				try {
					JSONObject jsonObject = new JSONObject(result);
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						db.deleteAllShop();
						JSONArray jsonArray = jsonObject.getJSONArray(Database.TAG_SHOP);
						List<ContentValues> v = new ArrayList<ContentValues>();
						for (int i=0; i<jsonArray.length(); i++){
							JSONObject shop = jsonArray.getJSONObject(i);
							ContentValues values = new ContentValues();
							values.put(LocalDatabaseHelper.SHOP_ID, shop.getInt(Database.TAG_SHOP_ID));
							values.put(LocalDatabaseHelper.SHOP_NUMBER, shop.getString(Database.TAG_SHOP_NUMBER));
							values.put(LocalDatabaseHelper.SHOP_NAME, shop.getString(Database.TAG_SHOP_NAME));
							values.put(LocalDatabaseHelper.SHOP_FLOOR_ID, shop.getString(Database.TAG_SHOP_FLOOR_ID));
							values.put(LocalDatabaseHelper.SHOP_CATEGORY_ID, shop.getString(Database.TAG_SHOP_CATEGORY_ID));
//							db.addShop(values, 0);
							v.add(values);
						}
						db.addShop(v);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (loading.isShowing()){
				loading.dismiss();
			}
			isShopLoaded = true;
			if (isShopLoaded && isSpecialEventLoaded){
				Introduction.this.startActivity(intent);
				Introduction.this.finish();
			}
		}

	}


	/**
	 * Check and get the special Event
	 */
	private class getSpecialEvent extends AsyncTask<String, Void, String>{

		private String URL = Database.DATABASE_URL + Database.API_SPECIAL_EVENT;

		private ProgressDialog loading;

		public getSpecialEvent(Context context){
			loading = new ProgressDialog(context);
		}

		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";

			try {
				// Send request and get data
				HttpResponse httpResponse = httpClient.execute(httpGet);
				result = EntityUtils.toString(httpResponse.getEntity());
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (loading.isShowing()){
				loading.dismiss();
			}
			try{
				// Check if the information get correctly
				if (!result.equals("error")){
					JSONObject jsonObject = new JSONObject(result);
					// Check if there are error in the response
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){


						// get the special event and move to main menu
						intent = new Intent(Introduction.this, MainMenu.class);
						intent = new Intent(Introduction.this, NYS2017.class);
//						startActivity(Intent(this, NYS2017::class.java))


						intent.putExtra(Database.TAG_SPECIAL_EVENT_NAME, jsonObject.getString(Database.TAG_SPECIAL_EVENT_NAME));
						intent.putExtra(Database.TAG_SPECIAL_EVENT_STATUS, jsonObject.getInt(Database.TAG_SPECIAL_EVENT_STATUS));
						isSpecialEventLoaded = true;
						if (isShopLoaded && isSpecialEventLoaded){
							Introduction.this.startActivity(intent);
							Introduction.this.finish();
						}
					}else{
						Log.i("Introduction", "Get Special Event: "+jsonObject.getString(Database.TAG_MESSAGE));
					}
				}else{
					Log.i("Introduction", "Get Special Event: Get Response Error");
				}
			}catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

}
