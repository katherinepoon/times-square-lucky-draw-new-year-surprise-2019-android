package com.timesquare;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.timesquare.database.Database;
import com.timesquare.localdatabase.Floors;
import com.timesquare.localdatabase.LocalDatabaseHelper;
import com.timesquare.localdatabase.ShopCategory;
import com.timesquare.setting.ShopAdapter;
import com.timessquare.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ShopNDine extends Activity{
	
	private LinearLayout setting;
	private TextView menu, by,search;
	private ExpandableListView list;
	
	private boolean isByFloor = true;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header);
		View.inflate(this, R.layout.shopndine, (ViewGroup) findViewById(R.id.header));
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 		
		
		// Initialize the object
		setting = (LinearLayout) findViewById(R.id.header_setting);
		TextView setting_text = (TextView) findViewById(R.id.header_setting_text);
		TextView title = (TextView) findViewById(R.id.header_title);
		menu = (TextView) findViewById(R.id.header_menu);
		by = (TextView) findViewById(R.id.shopndine_by);
		list = (ExpandableListView) findViewById(R.id.shopndine_list);
		search = (TextView) findViewById(R.id.shopndine_search);
		
		// set variable
		setting_text.setTypeface(font);
		title.setTypeface(font);
		menu.setTypeface(font);
		by.setTypeface(font);
		
		title.setText(R.string.mainmenu_shopndine);
		list.setGroupIndicator(null);
		
		
//		if (isOnline()){
//			new getFloors(this, list).execute("");
//		}else{
//			Log.i("ShopNDine", "No Internet Connection");
//			Toast.makeText(this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
//		}
//		
//		isByFloor = true;
		
		if (isOnline()){
			new getShopCate(ShopNDine.this, list).execute("");
		}else{
			Log.i("ShopNDine", "No Internet Connection");
			Toast.makeText(this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
		}
		
		by.setText(R.string.Shopndine_ByFloor);
		isByFloor = false;
		
		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
 		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_setting:
					ShopNDine.this.startActivity(new Intent(ShopNDine.this, Setting.class));
					break;
				case R.id.header_menu:
					ShopNDine.this.finish();
					break;
				case R.id.shopndine_by:
					if (isByFloor){
						new getShopCate(ShopNDine.this, list).execute("");
						by.setText(R.string.Shopndine_ByFloor);
						isByFloor = false;
					}else{
						new getFloors(ShopNDine.this, list).execute("");	
						by.setText(R.string.Shopndine_byCate);
						isByFloor = true;
					}							
					break;
				case R.id.shopndine_search:
					ShopNDine.this.startActivityForResult(new Intent(ShopNDine.this, ShopNDineSearch.class), 1);
					break;
				default:
					break;
				}
				
			}
		};
		setting.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		by.setOnTouchListener(onTouchListener);
		search.setOnTouchListener(onTouchListener);
		setting.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
		by.setOnClickListener(onClickListener);
		search.setOnClickListener(onClickListener);
		
		
	};
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 1){
			ShopNDine.this.finish();
		}
	}
	
	/**
	 * Check if there are internet connection
	 * @return boolean
	 */
	public boolean isOnline(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	
	private class getShopCate extends AsyncTask<String, Void, String>{

		private Context context;
		private String URL = Database.DATABASE_URL + Database.API_SHOP_CATEGORY;
		private ProgressDialog loading;
		
		private ExpandableListView list;
		private LocalDatabaseHelper db;
		
		public getShopCate(Context context, ExpandableListView list) {
			this.context = context;
			db = new LocalDatabaseHelper(context);
			loading = new ProgressDialog(context);
			this.list = list;
		}
		
		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(context.getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(String... params) {
			//get event JSON from server
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";
			
			try {
				HttpResponse httpResponse = httpClient.execute(httpGet);
				StatusLine statusLine = httpResponse.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					result = EntityUtils.toString(httpResponse.getEntity());
				}						
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// if no error, write event data to the local database
			if(!result.equals("error")){
				try {
					JSONObject jsonObject = new JSONObject(result);			
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						JSONArray jsonArray = jsonObject.getJSONArray(Database.TAG_SHOP_CATEGORY);
						List<ShopCategory> listData = new ArrayList<ShopCategory>();
						for (int i=0; i<jsonArray.length(); i++){
							JSONObject shopCategory = jsonArray.getJSONObject(i);	
							String shop_category_id = shopCategory.getString(Database.TAG_SHOP_CATEGORY_ID);
							ShopCategory s = new ShopCategory(shop_category_id, shopCategory.getString(Database.TAG_SHOP_CATEGORY_NAME), db.getShopByCateID(shop_category_id));
							listData.add(s);
						}				
						ShopAdapter adapter = new ShopAdapter(context, null, listData, 1);
						list.setAdapter(adapter);
						adapter.notifyDataSetChanged();
					}	
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
			
			if (loading.isShowing()){
				loading.dismiss();
			}
		}
		
	}
	
	private class getFloors extends AsyncTask<String, Void, String>{

		private Context context;
		private String URL = Database.DATABASE_URL + Database.API_FLOORS;
		private ProgressDialog loading;
		
		private ExpandableListView list;
		private LocalDatabaseHelper db;
		
		public getFloors(Context context, ExpandableListView list) {
			this.context = context;
			db = new LocalDatabaseHelper(context);
			loading = new ProgressDialog(context);
			this.list = list;
		}
		
		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(context.getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(String... params) {
			//get event JSON from server
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";
			
			try {
				HttpResponse httpResponse = httpClient.execute(httpGet);
				StatusLine statusLine = httpResponse.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					result = EntityUtils.toString(httpResponse.getEntity());
				}						
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// if no error, write event data to the local database
			if(!result.equals("error")){
				try {
					JSONObject jsonObject = new JSONObject(result);			
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						JSONArray jsonArray = jsonObject.getJSONArray(Database.TAG_FLOOR);
						List<Floors> listData = new ArrayList<Floors>();
						for (int i=0; i<jsonArray.length(); i++){
							JSONObject floor = jsonArray.getJSONObject(i);	
							String shop_floor_id = floor.getString(Database.TAG_FLOOR_ID);
							Floors f = new Floors(shop_floor_id, floor.getString(Database.TAG_FLOOR_NAME), db.getShopByFloorID(shop_floor_id));
							listData.add(f);
						}				
						ShopAdapter adapter = new ShopAdapter(context, listData, null, 0);
						list.setAdapter(adapter);
						adapter.notifyDataSetChanged();
					}	
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
			
			if (loading.isShowing()){
				loading.dismiss();
			}
		}
		
	}
	


}
