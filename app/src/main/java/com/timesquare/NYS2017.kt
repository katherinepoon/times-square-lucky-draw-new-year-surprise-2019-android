package com.timesquare

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.InputFilter
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.timessquare.R
import android.support.design.widget.TextInputEditText
import android.text.method.DigitsKeyListener
import android.text.InputType
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayout
import android.text.TextPaint
import android.util.DisplayMetrics
import android.util.Log
import com.crashlytics.android.Crashlytics
//import com.google.android.gms.R.id.date
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.squareup.picasso.Picasso
import com.timesquare.database.Database
import com.timesquare.setting.FontCache
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject
import java.text.DateFormat.getDateTimeInstance
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import java.util.Calendar
import kotlin.collections.ArrayList


/**
 *
 * Created by sllin on 13/10/2017.
 */

/**
 * Confirm expenses:
 * submitTicket() --> params: int numberOfTickets
 *
 *
 */

class NYS2017 : AppCompatActivity(), View.OnClickListener {
    private val root: LinearLayout by lazy { findViewById<LinearLayout>(R.id.luckydraw_layout) }

    private val fontNormal by lazy { FontCache.get("fonts/AvantGardeMdITC.otf", this) }
    val dimen : DisplayMetrics by lazy { DisplayMetrics().apply { windowManager.defaultDisplay.getMetrics(this) } }

    var backPressFn : () -> Unit = { finish() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.luckydraw2018)

        showMenu()


    }


    private fun showMenu() {
        backPressFn = { finish() }
        resetRootView()

        root.addView(textViewTitle(R.string.event_nys2017_nys, 0.3f, Gravity.CENTER, 44f))
        root.addView(textViewBtn(R.string.event_nys2017_btn_registration, 0.02f))
        root.addView(textViewBtn(R.string.event_nys2017_btn_lucky_draw, 0.02f))

        var tradeLicense = TextView(this@NYS2017)

        tradeLicense.apply {
            setPadding(5, 5, 5, 5)
            gravity = Gravity.START
            this.text = "Trade Promotion Competition Licence No. : 051460\n" + "推廣生意的競賽牌照號碼 :  051460"
            setTextColor(ContextCompat.getColor(this@NYS2017, R.color.hint_white))
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
                layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                        .apply {
                            setMargins(30, 0, 30, 15)
                            gravity = Gravity.CENTER
                        }


        }

        root.addView(tradeLicense)

    }

    private fun addSpending() {
        backPressFn = { showMenu() }
        resetRootView()

        root.addView(textViewHeader(R.string.event_nys2017_spending_amount))

        val scrollView = ScrollView(this)
        scrollView.layoutParams = LinearLayout.LayoutParams(dimen.widthPixels /2, ViewGroup.LayoutParams.WRAP_CONTENT)
                .apply { weight = 1f; gravity = Gravity.CENTER }
        root.addView(scrollView)

        val layout = LinearLayout(this).apply { orientation = LinearLayout.VERTICAL }
        scrollView.addView(layout)

        val addExpenses = buttonWide(R.string.event_nys2017_more_receipt, { add5spendingRows(layout) })

        addExpenses.callOnClick()

        root.addView(addExpenses)
        root.addView(buttonNarrow(R.string.event_nys2017_confirm, { calcSpendingAmount(layout) }))


    }

    private fun add5spendingRows(view: LinearLayout) {
        for (i in 0..4) {
            val hint = String.format(getString(R.string.event_nys2017_spending_num), view.childCount + 1)
            view.addView(editText(hint).apply {
                setPadding(0, 20, 0, 20)
            })
        }
    }

    private fun calcSpendingAmount(view: LinearLayout) {
        var receiptNumber = 0
        var totalAmount = 0f
        var totalAmountStr = ""
        var hasError = false

        val df = DecimalFormat("#,###.00")

        (0 until view.childCount).forEach {
            val textInputLayout = view.getChildAt(it) as? TextInputLayout
            val edt = textInputLayout?.editText
            if (edt != null && edt.text.toString().isNotEmpty()) {
                try {
                    val amount = edt.text.toString().toFloat()
                    val amountStr = df.format(amount)
                    if (amount > 0 && amount < 10000000000.0) {
                        if (amount < 500) {
                            textInputLayout.error = getString(R.string.event_nys2017_spending_lower_limit)
                            hasError = true
                        } else {
                            textInputLayout.error = null
                            receiptNumber += 1
                            totalAmount += amount
                            totalAmountStr += amountStr + " "
                        }
                    }
                } catch (e: Exception) {
                    textInputLayout.error = getString(R.string.event_nys2017_invalid_value)
                    hasError = true
                }
            }
        }

        if (hasError) {
            Toast.makeText(this, R.string.ErrorInvalidFieldsExist, Toast.LENGTH_SHORT).show()
            return
        } else if (receiptNumber == 0) {
            Toast.makeText(this, R.string.ErrorNoInputtedValue, Toast.LENGTH_SHORT).show()
            return
        }

        totalAmountStr = totalAmountStr.trim()
        totalAmountStr = totalAmountStr.replace(" ", " + ")
        totalAmountStr += "\n= " + String.format("%,.2f", totalAmount) + "\n"

        val dialogStr = String.format(getString(R.string.event_nys2017_confirm_dialog), receiptNumber) + totalAmountStr

        AlertDialog.Builder(this)
                .setMessage(dialogStr)
                .setPositiveButton(R.string.event_nys2017_confirm, { _, _ -> submitTicket(totalAmount.toInt()) })
                .setNegativeButton(R.string.event_nys2017_cancel, null)
                .show()
    }

    private fun submitTicket(numberOfTicketReceived: Int) {
        var dateNow = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().time)


        Database.nys2017("add/$numberOfTicketReceived", object : AsyncHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                if (responseBody != null) {
                    val json = JSONObject(String(responseBody))
                    if (!json.optBoolean(Database.TAG_ERROR, true)) {
//                        ticketResult(json.optJSONArray(Database.TAG_RESULT), json.optString(Database.TAG_DATETIME))
                        ticketResult(json.optJSONArray(Database.TAG_RESULT), dateNow.toString())

                    } else {
                        Toast.makeText(this@NYS2017, json.optString(Database.TAG_MESSAGE), Toast.LENGTH_LONG).show()
                    }

                } else {
                    onFailure(statusCode, headers, responseBody, null)
                }
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                Log.i("NYS2017", String(responseBody ?: ByteArray(0)))
                Toast.makeText(this@NYS2017, getString(R.string.unknownError), Toast.LENGTH_LONG).show()

            }
        })
    }

    private fun ticketResult(result: JSONArray, date: String) {
        backPressFn = { addSpending() }
        resetRootView()

        val root2 = LinearLayout(this).apply {
            orientation = LinearLayout.VERTICAL
            weightSum = 13f

        }


        root2.layoutParams = LinearLayout.LayoutParams(dimen.widthPixels, ViewGroup.LayoutParams.MATCH_PARENT)
                .apply {
                    gravity = Gravity.CENTER

                }


        root.addView(ScrollView(this@NYS2017).apply {
            this.layoutParams = LinearLayout.LayoutParams(
//                    ViewGroup.LayoutParams.MATCH_PARENT
                    dimen.widthPixels
                    , ViewGroup.LayoutParams.MATCH_PARENT)

            addView(root2)
        })


        root2.addView(textViewTitle(R.string.event_nys2017_ticket_num, 3f, Gravity.LEFT, 36f))

        var numbers = ""
        val colNum : Int = 3

        (0 until result.length()).forEach {
            numbers += result[it]

            when (it % colNum < colNum -1) {
                true -> numbers += "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
                false -> numbers += "\n"
            }
        }
        numbers = numbers.trim()

        val runNumber = textViewTicket(numbers).apply {
            gravity = Gravity.START
            layoutParams = LinearLayout.LayoutParams(dimen.widthPixels,
                    (dimen.heightPixels*0.4).toInt(), 7f)
        }

        root2.addView(runNumber)

        val summaryView = LinearLayout(this).apply {
            layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 3f)
            orientation = LinearLayout.HORIZONTAL
            gravity = Gravity.CENTER
            weightSum = 9f


            val ticketSummary = LinearLayout(this@NYS2017).apply {

                orientation = LinearLayout.VERTICAL
                gravity = Gravity.START

                this.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT
                        , 1f
                )

                val strDate = date.split(" ")

                this.addView(textViewTicket(String.format(getString(R.string.event_nys2017_total_ticket_gained), result.length()), true)
                        .apply {
                            gravity = Gravity.START
                        })


                this.addView(textViewTicket(String.format(getString(R.string.event_nys2017_ticket_datetime), strDate[0], strDate[minOf(1, strDate.size)]))
                        .apply {
                            gravity = Gravity.START
                        }
                        )

            }


            val buttons = LinearLayout(this@NYS2017).apply {
                orientation = LinearLayout.HORIZONTAL
                gravity = Gravity.START


                this.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT
                        , 8f
                )


                this.addView(buttonNarrow(R.string.event_nys2017_home, { showMenu() }))
                this.addView(buttonNarrow(R.string.event_nys2017_next_registration, { addSpending() }))
            }

            this.addView(ticketSummary)
            this.addView(buttons)
        }

        root2.addView(summaryView)



    }

    private fun luckyDrawMenu() {
        backPressFn = { showMenu() }
        resetRootView()

        if (readSharedPrefHasDrawn() == 1) //hasDrawn --> use stored result
        {
            showDrawResult(readSharedPrefResults())
        }
        else (
        AlertDialog.Builder(this)
                .setMessage(R.string.event_nys2017_confirm_draw_now)
                .setPositiveButton(R.string.event_nys2017_draw_now, { _, _ -> drawTicket() })
                .setNegativeButton(R.string.event_nys2017_cancel, { _, _ -> showMenu() })
                .show()
                )
    }

    private fun drawTicket() {
        val dialog = AlertDialog.Builder(this)
                .setMessage(R.string.event_nys2017_drawing_number)
                .setView(ProgressBar(this, null, android.R.attr.progressBarStyle))
                .show()

        if (readSharedPrefHasDrawn() == 1) //hasDrawn --> use stored result
            {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                showDrawResult(readSharedPrefResults())
            }
        else if (readSharedPrefHasDrawn() == 0) {

            Database.nys2017("draw/1", object : AsyncHttpResponseHandler() {
                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                    if (dialog.isShowing) {
                        dialog.dismiss()
                    }
                    if (responseBody == null) {
                        return onFailure(statusCode, headers, responseBody, null)
                    }

                    val json = JSONObject(String(responseBody))
                    if (!json.optBoolean(Database.TAG_ERROR, true) && json.optBoolean(Database.TAG_DRAW_SUCCESS, false)) {
                        showDrawResult(arrayToResults(json.optJSONArray(Database.TAG_RESULT)))


                        /**
                         * hasDrawn change to 1 + store results
                         */
                        writeSharedPrefHasDrawn()
                        writeSharedPrefResults(arrayToResults(json.optJSONArray(Database.TAG_RESULT)))

                    } else {
                        Toast.makeText(this@NYS2017, json.optString(Database.TAG_MESSAGE, getString(R.string.unknownError)), Toast.LENGTH_LONG).show()
                    }

                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                    if (dialog.isShowing) {
                        dialog.dismiss()
                    }
                    Toast.makeText(this@NYS2017, R.string.unknownError, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    /**
     * JSONArray to string
     *  -- create return value for number of results
     */
    private fun arrayToResults(array: JSONArray): String{
        val resultArray = ArrayList<DrawNumber>()
        (0 until array.length()).forEach {
            val result = array[it] as JSONObject
            val drawNumber = DrawNumber(result.optInt(Database.TAG_TICKET_ID).toString().padStart(5, '0'), this)

            resultArray.add(drawNumber)
        }
        resultArray.sortBy { it.number }

        var numbers = ""
        val colNum : Int = 3

        (0 until resultArray.size).forEach {
            numbers += resultArray[it].number

            when (it % colNum < colNum -1) {

            /**
             * reduce column width for running ticket numbers
             */

//                true -> numbers += "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
                true -> numbers += "\t\t\t\t\t\t\t\t"

                false -> numbers += "\n"
            }
        }
        numbers = numbers.trim()

        return numbers
    }

    private fun showDrawResult(results: String){
        backPressFn = { showMenu() }
        writeSharedPref()

        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        val curDate = dateFormat.format(Date())
        val timeFormat = SimpleDateFormat("hh:mm")
        val curTime = timeFormat.format(Date())

        resetRootView()

        val root2 = LinearLayout(this).apply {
            orientation = LinearLayout.VERTICAL
            weightSum = 13f

        }
        root2.layoutParams = LinearLayout.LayoutParams(dimen.widthPixels, ViewGroup.LayoutParams.MATCH_PARENT)
                .apply {
                    gravity = Gravity.CENTER

                }
        root.addView(ScrollView(this@NYS2017).apply {
            this.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            addView(root2)
        })


        root2.addView(textViewTitle(R.string.event_nys2018_lucky_draw_result, 3f, Gravity.LEFT, 36f))

        val runNumber = textViewTicket(results).apply {
            gravity = Gravity.START
            layoutParams = LinearLayout.LayoutParams(dimen.widthPixels,
                    (dimen.heightPixels*0.5).toInt(), 7f)
        }


        root2.addView(runNumber)

        val summaryView = LinearLayout(this).apply {

            layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 3f)
            orientation = LinearLayout.HORIZONTAL
            gravity = Gravity.CENTER
            weightSum = 6f




            val ticketSummary = LinearLayout(this@NYS2017).apply {

                orientation = LinearLayout.VERTICAL
                gravity = Gravity.START

                this.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT
                        , 3f
                )

                this.addView(textViewTicket(String.format(getString(R.string.event_nys2018_total_ticket_drawn), 30, true))
                        .apply {
                            gravity = Gravity.START
                        })

                this.addView(textViewTicket(
                        (String.format(getString(R.string.event_nys2017_ticket_datetime), curDate, curTime))
                        )
                        .apply { gravity = Gravity.START })
            }

            val buttons = LinearLayout(this@NYS2017).apply {
                orientation = LinearLayout.HORIZONTAL
                gravity = Gravity.START

                this.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT
                        , 2f
                )

                this.addView(buttonWide(R.string.event_nys2017_home, { showMenu() }))

            }

            this.addView(ticketSummary)
            this.addView(buttons)
        }

        root2.addView(summaryView)


    }

    private fun resetRootView() = root.run {
        removeAllViews()
        setPadding(
//                150
                30
                ,
//                100
                40
                ,
//                150
                30
                ,
//                100
                40
        )

//        this.addView(textViewTicket("TEST/DEV"))
//                .apply {
//            gravity = Gravity.END
//        }


    }


    private fun editText(hint: String): TextInputLayout {
        val textInputLayout = TextInputLayout(this)
        val edt = TextInputEditText(this)
        edt.hint = (hint)

        edt.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(9))

        edt.setSingleLine()
        edt.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
        edt.keyListener = DigitsKeyListener.getInstance("0123456789.")

//        edt.setHintTextColor(resources.getColor(R.color.hint_white))
//        edt.setTextColor(resources.getColor(R.color.Catch_white))


        textInputLayout.addView(edt)



        return textInputLayout
    }

    private fun textView(text: String): TextView =
            TextView(this).apply {
                setPadding(30, 15, 30, 15)
                gravity = Gravity.CENTER
                this.text = text
                setTextColor(ContextCompat.getColor(this@NYS2017, R.color.ThemeColor_black))
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
                layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                        .apply {
                            setMargins(30, 0, 30, 15)
                            gravity = Gravity.CENTER
                        }
            }

    private fun textViewTicket(text: String, bold: Boolean? = null): TextView =
            TextView(this).apply {
                setPadding(30, 15, 30, 15)
                gravity = Gravity.CENTER
                this.text = text
                setTextColor(ContextCompat.getColor(this@NYS2017, R.color.hint_white))
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f)
//                layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//                        .apply {
//                            setMargins(30, 0, 30, 15)
//                            gravity = Gravity.CENTER
//                        }

                bold?.run { setTypeface(null, Typeface.BOLD) }

            }

    private fun textView(text: Int): TextView = textView(getString(text)).apply { tag = text }


    private fun textViewTitle(text: Int, weight: Float?, grav: Int, textSize: Float): TextView = textView(getString(text)).apply {
        tag = text
        setTextColor(ContextCompat.getColor(this@NYS2017, R.color.Catch_white))
        setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        setTypeface(null, Typeface.BOLD)

        val param = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        weight?.run { param.weight = weight }
        param.gravity = grav

        this.layoutParams = param
    }

    private fun textViewHeader(text: Int): TextView = textView(getString(text)).apply {
        tag = text
        setTextColor(ContextCompat.getColor(this@NYS2017, R.color.Catch_white))
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 30f)
        setTypeface(null, Typeface.BOLD)
    }


    private fun textViewBtn(text: Int, weight: Float?): TextView =
            textView(text).apply {
                setBackgroundResource(R.drawable.ic_button)
//                setBackgroundColor(Color.parseColor("#FFFFFF"))

                setTextSize(TypedValue.COMPLEX_UNIT_SP, 30f)

                setOnClickListener(this@NYS2017)

                val param = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                weight?.run { param.weight = weight }
                param.gravity = Gravity.CENTER
                param.setMargins(0, 20, 0, 20)

                this.layoutParams = param

            }


    private fun buttonWide(text: Int, onClick: () -> Unit): TextView {
        val textView = textView(text)
                .apply {
                                        setBackgroundResource(R.drawable.ic_button_addexpense)
//                    setBackgroundColor(Color.parseColor("#FFFFFF"))
                    setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
                }

        textView.layoutParams = (textView.layoutParams as LinearLayout.LayoutParams)
                .apply {
                    width = ViewGroup.LayoutParams.WRAP_CONTENT
                    height = ViewGroup.LayoutParams.WRAP_CONTENT
                    gravity = Gravity.CENTER
                }

        textView.setOnClickListener { onClick.invoke() }

        return textView

    }

    private fun buttonNarrow(text: Int, onClick: () -> Unit): TextView {
        val textView = textView(text)
                .apply {
                    setBackgroundResource(R.drawable.ic_button_confirm)
                    setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)

                }

        textView.layoutParams = (textView.layoutParams as LinearLayout.LayoutParams)
                .apply {
                    width = ViewGroup.LayoutParams.WRAP_CONTENT
                    height = ViewGroup.LayoutParams.WRAP_CONTENT
                    gravity = Gravity.CENTER
                }

        textView.setOnClickListener { onClick.invoke() }

        return textView

    }


    override fun onClick(v: View?) {
        when (v?.tag) {
            R.string.event_nys2017_btn_registration -> {
                addSpending()
            }
            R.string.event_nys2017_btn_lucky_draw -> {
                luckyDrawMenu()
            }
            R.string.event_nys2017_draw_now,
            R.string.event_nys2017_view_result-> {
                drawTicket()
            }

        }
    }

    class DrawNumber internal constructor(val number: String, thiss: Context) {
        val tv = TextView(thiss).apply {
            text = number
            setTextColor(Color.WHITE)
            background = ContextCompat.getDrawable(thiss, R.drawable.nys17_circle)
            setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12f)
            gravity = Gravity.CENTER
            typeface = FontCache.get("fonts/Helvetica.dfont", thiss)
        }
        var redeemed: Int = 0
        fun redeem() {
            redeemed = 1
            tv.paintFlags = tv.paintFlags or TextPaint.STRIKE_THRU_TEXT_FLAG
            tv.setTextColor(Color.LTGRAY)
        }
    }

    class ListAdapter internal constructor(val thiss: Context, var array: ArrayList<DrawNumber>) : BaseAdapter() {

        override fun getView(position: Int, view: View?, parent: ViewGroup?): View {
            val checkBox: CheckBox
            if (view == null) {
                checkBox = CheckBox(thiss)
                checkBox.setTextColor(ContextCompat.getColor(thiss, R.color.ThemeColor_black))
                checkBox.gravity = Gravity.CENTER
                checkBox.setOnCheckedChangeListener({ _ , checked ->
                    checkBox.paintFlags = if (checked){
                        checkBox.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    }else{
                        checkBox.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                    }
                })
            } else {
                checkBox = view as CheckBox
            }

            checkBox.text = array[position].tv.text
            checkBox.isChecked = array[position].redeemed > 0

            return checkBox
        }

        override fun getItem(pos: Int): Any = array[pos]

        override fun getItemId(p0: Int): Long = 0

        override fun getCount(): Int = array.size

    }

    private val prefDate = "NYS2017-" + SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).format(Date())

    private val hasDrawn = "HasDrawn" + SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).format(Date())

    private val drawnResults = "DrawnResults" + SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).format(Date())

    private fun writeSharedPref() = getPreferences(Context.MODE_PRIVATE).edit().putInt(prefDate, 1).apply()

    private fun readSharedPref() : Int =
            getPreferences(Context.MODE_PRIVATE).getInt(prefDate, 0)

    /**
     * sharedPref hasDrawn:
     *  0 -- has not drawn tickets yet
     *  1 -- has drawn tickets
     */
    private fun writeSharedPrefHasDrawn() = getPreferences(Context.MODE_PRIVATE).edit().putInt(hasDrawn, 1).apply()

    private fun readSharedPrefHasDrawn() = getPreferences(Context.MODE_PRIVATE).getInt(hasDrawn, 0)

    private fun writeSharedPrefResults(results: String) = getPreferences(Context.MODE_PRIVATE).edit().putString(drawnResults, results).apply()

    private fun readSharedPrefResults() = getPreferences(Context.MODE_PRIVATE).getString(drawnResults, "")

    override fun onBackPressed() {
        backPressFn.invoke()
    }

}