package com.timesquare

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.timessquare.R

/**
 * Created by sllin on 7/11/2017.
 */

class SEChooser : AppCompatActivity(), View.OnClickListener{

    private val root : LinearLayout by lazy { findViewById<LinearLayout>(R.id.empty_root) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.header)
        View.inflate(this, R.layout.empty, findViewById<ViewGroup>(R.id.header) )

        findViewById<View>(R.id.header_setting).setOnClickListener(this)
        findViewById<View>(R.id.header_menu).setOnClickListener(this)

        generateView()

    }


    private fun generateView(){
        intArrayOf(R.string.event_2017_christmas, R.string.event_2017_nys)
                .forEach { item: Int ->
                    val textView = TextView(this)
                    val param = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
                    param.setMargins(30, 30, 30, 30)
                    textView.layoutParams = param
                    textView.setPadding(30,30,30,30)
                    textView.gravity = Gravity.CENTER
                    textView.setText(item)
                    textView.tag = item
//                    textView.setTextColor(ContextCompat.getColor(this, R.color.ThemeColor_DeepGreen))
                    textView.setTextColor(ContextCompat.getColor(this, R.color.hint_white))

//                    textView.background = ContextCompat.getDrawable(this, R.drawable.carpark_greenborder)
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30f)
                    textView.setOnClickListener(this)

                    root.addView(textView)
                }

    }


    override fun onClick(v: View?) {
        when (v?.tag){
            R.string.event_2017_christmas -> {
                startActivity(Intent(this, SEChristmas2017::class.java))
            }
            R.string.event_2017_nys -> {
                startActivity(Intent(this, NYS2017::class.java))
            }
        }
        when (v?.id){
            R.id.header_setting -> {
                this.startActivity(Intent(this, Setting::class.java))
            }

            R.id.header_menu -> {
                this.finish()
            }
        }
    }
}