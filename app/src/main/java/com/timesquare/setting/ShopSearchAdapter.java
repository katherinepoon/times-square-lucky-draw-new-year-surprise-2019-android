package com.timesquare.setting;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.timesquare.ShopNDineItem;
import com.timesquare.database.Database;
import com.timesquare.localdatabase.Shop;
import com.timessquare.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class ShopSearchAdapter extends BaseAdapter implements Filterable{
	
	private List<Shop> shops = new ArrayList<Shop>();
	private List<Shop> filteredShops = new ArrayList<Shop>();
	private LayoutInflater inflater;
	private Context context;
	
	public ShopSearchAdapter(Context context, List<Shop> shops) {
		this.inflater = LayoutInflater.from(context);
		this.shops = shops;
		this.filteredShops = shops;
		this.context = context;
	}

	@Override
	public int getCount() {
		return filteredShops.size();
	}

	@Override
	public Object getItem(int position) {
		return filteredShops.get(position);
	}

	@Override
	public long getItemId(int position) {
		return Long.parseLong(filteredShops.get(position).getShopID());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int p = position;
		
		if (convertView == null){
			convertView = inflater.inflate(R.layout.shopndine_list_shop_item, parent, false);
		}
		TextView shopNumber =  (TextView) convertView.findViewById(R.id.shopndine_list_shop_item_shop_number);
		TextView shopName = (TextView) convertView.findViewById(R.id.shopndine_list_shop_item_shop_name);
		shopNumber.setText(filteredShops.get(position).getShopNumber());
		shopName.setText(filteredShops.get(position).getShopName());
		
		convertView.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, ShopNDineItem.class);
				intent.putExtra(Database.TAG_SHOP_ID, filteredShops.get(p).getShopID());
				((Activity)context).startActivityForResult(intent, 1);								
			}
		});
	
		return convertView;
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				filteredShops = (List<Shop>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				String lowerConstraint = constraint.toString().toLowerCase(Locale.US);
				
				if (constraint == null || constraint.length() <= 0){
					results.values = shops;
					results.count = shops.size();
				}else{
					List<Shop> filteredData = new ArrayList<Shop>();
					for (Shop s : shops){
						if (s.getShopName().toLowerCase(Locale.US).contains(lowerConstraint) || s.getShopNumber().toLowerCase(Locale.US).contains(lowerConstraint)){
							filteredData.add(s);
						}
					}
					results.values = filteredData;
					results.count = filteredData.size();
				}
				
				return results;
			}
		};
	}
	
	

}
