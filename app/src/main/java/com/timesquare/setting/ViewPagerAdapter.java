package com.timesquare.setting;

import java.util.HashMap;
import java.util.List;

import com.sllin.imageloader.ImageLoader;
import com.timesquare.database.Database;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewPagerAdapter extends PagerAdapter{
	
	private List<HashMap<String, String>> listData;	
	private ImageLoader imageLoader;
	private Context context;
	private int cate;
	
	private String url = Database.PATH_IMAGE;
	
	public ViewPagerAdapter(Context context, List<HashMap<String, String>> listData, int cate) {
		this.listData = listData;
		this.context = context;
		this.imageLoader = new ImageLoader(context);
		// cate: 0 for goodies
		this.cate = cate;
	}

	@Override
	public int getCount() {
		if (cate == 0){
			if (!listData.get(0).get(Database.TAG_GOODIES_IMAGE).equals(null)){
				return listData.get(0).size();
			}else{
				return 1;
			}
		}else if (cate == 1){
			return listData.size();
		}
		return 0;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public Object instantiateItem(View container, int position) {
		if (cate == 0){
			switch (position){
			case 0:
				if (!listData.get(0).get(Database.TAG_GOODIES_IMAGE).equals(null)){
					ImageView image = new ImageView(context);
					imageLoader.DisplayImage(url + "/" + listData.get(0).get(Database.TAG_GOODIES_IMAGE), image);
					((ViewPager)container).addView(image, 0);
					return image;
				}else{
					TextView text = new TextView(context);
					text.setText(listData.get(0).get(Database.TAG_GOODIES_DESCRIPTION));
					((ViewPager)container).addView(text, 0);
					return text;
				}
			case 1:
				TextView text = new TextView(context);
				text.setText(listData.get(0).get(Database.TAG_GOODIES_DESCRIPTION));
				((ViewPager)container).addView(text, 1);
				return text;
			default:
				break;
			}
		}else if (cate == 1){
			if (position == 0){
				TextView text = new TextView(context);
				text.setText(listData.get(position).get(Database.TAG_SHOP));
				((ViewPager)container).addView(text, 1);
				return text;
			}else{
				ImageView image = new ImageView(context);
				imageLoader.DisplayImage(url + "/" + listData.get(position).get(Database.TAG_SHOP), image);
				((ViewPager)container).addView(image, 0);
				return image;
			}
		}
		return null;
	}
	
	@Override
	public void destroyItem(View container, int position, Object object) {
		((ViewPager) container).removeView((View)object);
	}
	
	
	
}
