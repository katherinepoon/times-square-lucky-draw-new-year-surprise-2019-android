package com.timesquare.setting;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

/**
 * Created by sllin on 14/11/2016.
 */

public class FontCache {
    private static Hashtable<String, Typeface> fontCache = new Hashtable<>();

    public static Typeface get(String name, Context context){
        Log.d("FontCache", name);
        Typeface typeface = fontCache.get(name);
        if (typeface == null){
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), name);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
            fontCache.put(name, typeface);
        }
        return typeface;
    }

}
