package com.timesquare.setting;

import java.util.List;

import com.timesquare.ShopNDineItem;
import com.timesquare.database.Database;
import com.timesquare.localdatabase.Floors;
import com.timesquare.localdatabase.ShopCategory;
import com.timessquare.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ShopAdapter extends BaseExpandableListAdapter{
	
	private List<Floors> listFloorsData;
	private List<ShopCategory> listShopCategoryData;
	private LayoutInflater inflater;
	private Context context;
	private int cate;
	private Typeface font;
	
	public ShopAdapter(Context context, List<Floors> listFloorsData, List<ShopCategory> listShopCategoryData, int cate) {
		this.inflater = LayoutInflater.from(context);
		this.listFloorsData = listFloorsData;
		this.listShopCategoryData = listShopCategoryData;
		// cate: 0 for by floors, 1 for by cate
		this.cate = cate;
		this.context = context;
		this.font = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.font)); 
	}
	
	public int getCate(){
		return this.cate;
	}
	
	@Override
	public int getGroupCount() {
		if (cate == 0){
			if (listFloorsData != null){
				return listFloorsData.size();
			}else{
				return 0;
			}
		}else if (cate == 1){
			if (listShopCategoryData != null){
				return listShopCategoryData.size();
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (cate == 0){
			if (listFloorsData != null){
				return listFloorsData.get(groupPosition).getShopList().size();
			}else{
				return 0;
			}
		}else if (cate == 1){
			if (listShopCategoryData != null){
				return listShopCategoryData.get(groupPosition).getShopList().size();
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	@Override
	public Object getGroup(int groupPosition) {
		if (cate == 0){
			if (listFloorsData != null){
				return listFloorsData.get(groupPosition);
			}else{
				return null;
			}
		}else if (cate == 1){
			if (listShopCategoryData != null){
				return listShopCategoryData.get(groupPosition);
			}else{
				return null;
			}
		}else{
			return null;
		}
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		if (cate == 0){
			if (listFloorsData != null){
				return listFloorsData.get(groupPosition).getShopList().get(childPosition);
			}else{
				return null;
			}
		}else if (cate == 1){
			if (listShopCategoryData != null){
				return listShopCategoryData.get(groupPosition).getShopList().get(childPosition);
			}else{
				return null;
			}
		}else{
			return null;
		}
	}

	@Override
	public long getGroupId(int groupPosition) {
		if (cate == 0){
			if (listFloorsData != null){
				return Integer.parseInt(listFloorsData.get(groupPosition).getFloorID());
			}else{
				return 0;
			}
		}else if (cate == 1){
			if (listShopCategoryData != null){
				return Integer.parseInt(listShopCategoryData.get(groupPosition).getShopCategoryID());
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		if (cate == 0){
			if (listFloorsData != null){
				return Integer.parseInt(listFloorsData.get(groupPosition).getShopList().get(childPosition).getShopID());
			}else{
				return 0;
			}
		}else if (cate == 1){
			if (listShopCategoryData != null){
				return Integer.parseInt(listShopCategoryData.get(groupPosition).getShopList().get(childPosition).getShopID());
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null){
			convertView = inflater.inflate(R.layout.shopndine_list_item, parent, false);
		}
		ImageView indicator = (ImageView) convertView.findViewById(R.id.shopndine_list_item_indicator);
		TextView title = (TextView) convertView.findViewById(R.id.shopndine_list_item_title);
		title.setTypeface(font);
		if (isExpanded){
			indicator.setImageResource(R.drawable.shop_indicator_true);
			title.setTextColor(context.getResources().getColor(R.color.ThemeColor_yellow));
		}else{
			indicator.setImageResource(R.drawable.shop_indicator_false);
			title.setTextColor(context.getResources().getColor(R.color.ThemeColor_deepBlue));
		}
		if (cate == 0){
			if (listFloorsData != null){
				title.setText(listFloorsData.get(groupPosition).getFloorName());
			}
		}else if (cate == 1){
			if (listShopCategoryData != null){
				title.setText(listShopCategoryData.get(groupPosition).getShopCategoryName());
			}
		}	
		
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final int g = groupPosition;
		final int c = childPosition;	
		if (convertView == null){
//			convertView = new TextView(context);
//			convertView.setPadding(10, 0, 0, 0);
//			((TextView) convertView).setTextColor(context.getResources().getColor(R.color.ThemeColor_yellow));
			convertView = inflater.inflate(R.layout.shopndine_list_shop_item, parent, false);
			
		}
		
		TextView shopNumber =  (TextView) convertView.findViewById(R.id.shopndine_list_shop_item_shop_number);
		TextView shopName = (TextView) convertView.findViewById(R.id.shopndine_list_shop_item_shop_name);
				
		
		if (cate == 0){
			if (listFloorsData != null){
//				((TextView) convertView).setText(listFloorsData.get(groupPosition).getShopList().get(childPosition).getShopName());
				shopNumber.setText(listFloorsData.get(groupPosition).getShopList().get(childPosition).getShopNumber());
				shopName.setText(listFloorsData.get(groupPosition).getShopList().get(childPosition).getShopName());
			}
		}else if (cate == 1){
			if (listShopCategoryData != null){
//				((TextView) convertView).setText(listShopCategoryData.get(groupPosition).getShopList().get(childPosition).getShopName());
				shopNumber.setText(listShopCategoryData.get(groupPosition).getShopList().get(childPosition).getShopNumber());
				shopName.setText(listShopCategoryData.get(groupPosition).getShopList().get(childPosition).getShopName());
			}
		}	
		
		convertView.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, ShopNDineItem.class);
				if (cate == 0){
					if (listFloorsData != null){
						intent.putExtra(Database.TAG_SHOP_ID, listFloorsData.get(g).getShopList().get(c).getShopID());
						((Activity)context).startActivityForResult(intent, 1);
					}
				}else if (cate == 1){
					if (listShopCategoryData != null){
						intent.putExtra(Database.TAG_SHOP_ID, listShopCategoryData.get(g).getShopList().get(c).getShopID());
						((Activity)context).startActivityForResult(intent, 1);
					}
				}								
			}
		});
		
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

}
