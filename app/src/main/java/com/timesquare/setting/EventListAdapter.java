package com.timesquare.setting;

import java.util.HashMap;
import java.util.List;

import com.timesquare.CalendarDetail;
import com.timesquare.database.Database;
import com.timesquare.localdatabase.Event;
import com.timessquare.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class EventListAdapter extends BaseAdapter{
	
	private List<Event> listData;
	private LayoutInflater inflater;
	private Context context;
	private Typeface font;

	public EventListAdapter(Context context, List<Event> listData) {
		this.inflater = LayoutInflater.from(context);
		this.listData = listData;
		this.context = context;
		this.font = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.font)); 		
	}
	
	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return listData.get(position).getEventID();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int p = position;
		if (convertView == null){
			convertView = inflater.inflate(R.layout.calendar_event, parent, false);
		}
		TextView time = (TextView) convertView.findViewById(R.id.calendar_event_time);
		TextView name = (TextView) convertView.findViewById(R.id.calendar_event_name);
		TextView edit = (TextView) convertView.findViewById(R.id.calendar_event_edit);
		time.setTypeface(font);
		name.setTypeface(font);
		edit.setTypeface(font);
 		
		time.setText(listData.get(position).getEventDateTime());
		name.setText(listData.get(position).getEventTitle());		
		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				
				
			}
		};
//		edit.setOnTouchListener(onTouchListener);
//		edit.setOnClickListener(onClickListener);
		
		convertView.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, CalendarDetail.class);
				intent.putExtra(Database.TAG_EVENT_TITLE, listData.get(p).getEventTitle());
				intent.putExtra(Database.TAG_EVENT_DATE_TIME, listData.get(p).getEventDateTime());
				intent.putExtra(Database.TAG_EVENT_DETAIL, listData.get(p).getEventDetail());
				((Activity)context).startActivityForResult(intent, 1);
			}
		});
		
		return convertView;
	}

	public void setDate(List<Event> listData){
		this.listData = listData;
	}
	
}
