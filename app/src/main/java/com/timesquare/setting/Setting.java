package com.timesquare.setting;

public class Setting {
	
	public static final String filename = "setting";
	
	public static final String langauge = "language";

    public static final String CATCH_DATE = "catch_date";
    public static final String CATCH_DATETIME = "catch_datetime";
    public static final String CATCH_NUMBER = "catch_number";

    public static final String CATCH_REDEEM_1 = "redeem_1";
    public static final String CATCH_REDEEM_2 = "redeem_2";
    public static final String CATCH_REDEEM_3 = "redeem_3";
    public static final String CATCH_REDEEM_4 = "redeem_4";
    public static final String CATCH_REDEEM_5 = "redeem_5";
}
