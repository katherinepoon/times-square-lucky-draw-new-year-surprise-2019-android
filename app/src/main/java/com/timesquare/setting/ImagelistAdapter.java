package com.timesquare.setting;

import java.util.HashMap;
import java.util.List;

import com.sllin.imageloader.ImageLoader;
import com.timesquare.GoodiesItem;
import com.timesquare.WhatsupItem;
import com.timesquare.database.Database;
import com.timessquare.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImagelistAdapter extends BaseAdapter{
	
	private List<HashMap<String, String>> listData;
	private LayoutInflater inflater;
	private ImageLoader imageLoader;
	private Context context;
	private int cate;
	private Typeface font;
	
	
	private static final String url = Database.PATH_IMAGE;

	public ImagelistAdapter(Context context, List<HashMap<String, String>> listData, int cate) {
		this.inflater = LayoutInflater.from(context);
		this.listData = listData;
		this.imageLoader = new ImageLoader(context);
		// cate: 0 for whatsup, 1 for goodies
		this.cate = cate;
		this.context = context;
		this.font = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.font)); 
	}
	
	@Override
	public int getCount() {
		return listData.size();
	}
	
	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null){
			convertView = inflater.inflate(R.layout.imagelist_item, parent, false);
		}
		ImageView thumb = (ImageView) convertView.findViewById(R.id.imagelist_item_thumb);
		TextView title = (TextView) convertView.findViewById(R.id.imagelsit_item_title);
		TextView image = (TextView) convertView.findViewById(R.id.imagelsit_item_image);
		title.setTypeface(font);
		if (cate == 0){
			imageLoader.DisplayImage(url + "/" + listData.get(position).get(Database.TAG_WHATSUP_THUMB), thumb);
			title.setText(listData.get(position).get(Database.TAG_WHATSUP_TITLE));	
			image.setText(listData.get(position).get(Database.TAG_WHATSUP_IMAGE));
			image.setContentDescription(listData.get(position).get(Database.TAG_WHATSUP_CONTENT));	
			
			convertView.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, WhatsupItem.class);
					intent.putExtra(Database.TAG_WHATSUP_IMAGE, ((TextView)v.findViewById(R.id.imagelsit_item_image)).getText().toString());
					intent.putExtra(Database.TAG_WHATSUP_TITLE, ((TextView)v.findViewById(R.id.imagelsit_item_title)).getText().toString());
					intent.putExtra(Database.TAG_WHATSUP_CONTENT, ((TextView)v.findViewById(R.id.imagelsit_item_image)).getContentDescription().toString());
					((Activity) context).startActivityForResult(intent, 1);
				}
			});
		}else if (cate == 1){
			imageLoader.DisplayImage(url + "/" + listData.get(position).get(Database.TAG_GOODIES_THUMB), thumb);
			title.setText(listData.get(position).get(Database.TAG_GOODIES_TITLE));
			image.setText(listData.get(position).get(Database.TAG_GOODIES_CODE));	
			
			convertView.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, GoodiesItem.class);
					intent.putExtra(Database.TAG_GOODIES_CODE, ((TextView)v.findViewById(R.id.imagelsit_item_image)).getText().toString());
					((Activity) context).startActivityForResult(intent, 1);
				}
			});
		}
		
		
		return convertView;
	}

}
