package com.timesquare;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import com.timesquare.database.Database;
import com.timesquare.setting.CameraPreview;
import com.timesquare.setting.ImagelistAdapter;
import com.timessquare.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Member extends Activity {

	private LinearLayout setting;
	private FrameLayout qr_containner;
	private TextView menu, scanning, accessCamera;
	private ImageView QRscan;
    private ListView list;

	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;
	ImageScanner scanner;
	private boolean previewing = true;

	static {
		System.loadLibrary("iconv");
	}

	// Camera Auto Focus
	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	// Camera Preview
	PreviewCallback previewCb = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();
			Size size = parameters.getPreviewSize();

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = scanner.scanImage(barcode);

			if (result != 0) {
				previewing = false;
				mCamera.setPreviewCallback(null);
				mCamera.stopPreview();

				SymbolSet syms = scanner.getResults();
				for (Symbol sym : syms) {
					String code = sym.getData();
					if (isAlphaDigit(code)) {
						Intent intent = new Intent(Member.this, MemberGet.class);
						intent.putExtra(Database.TAG_MEMBER_CODE, code);
						Member.this.startActivityForResult(intent, 1);
					} else {
						invalidQR();
					}

				}
			}
		}
	};

	// Camera Focus
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header);
		View.inflate(this, R.layout.member,
				(ViewGroup) findViewById(R.id.header));

		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources()
				.getString(R.string.font));

		// Initialize the object
		setting = (LinearLayout) findViewById(R.id.header_setting);
		TextView setting_text = (TextView) findViewById(R.id.header_setting_text);
		TextView title = (TextView) findViewById(R.id.header_title);
		menu = (TextView) findViewById(R.id.header_menu);
		TextView warning = (TextView) findViewById(R.id.member_warning);
		qr_containner = (FrameLayout) findViewById(R.id.member_qr_container);
		TextView findQRTitle = (TextView) findViewById(R.id.member_findQR);
		QRscan = (ImageView) findViewById(R.id.member_QRscan);
		scanning = (TextView) findViewById(R.id.member_scanning);
		accessCamera = (TextView) findViewById(R.id.member_accessCamera);
        list = (ListView) findViewById(R.id.member_imagelist_list);

        if (isOnline()){
            new GetWhatsup(this).execute("");
        }else{
            Log.i("Whatsup", "No Internet Connection");
            Toast.makeText(this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
        }

		// Set Variable
		setting_text.setTypeface(font);
		title.setTypeface(font);
		menu.setTypeface(font);
		warning.setTypeface(font);
		findQRTitle.setTypeface(font);
		scanning.setTypeface(font);
		accessCamera.setTypeface(font);

		title.setText(R.string.mainmenu_member);

		QRscan.setVisibility(View.VISIBLE);
		scanning.setVisibility(View.GONE);
		accessCamera.setVisibility(View.VISIBLE);

		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP) {
					v.setAlpha(1f);
				}
				return false;
			}
		};

		View.OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.header_setting:
					Member.this.startActivity(new Intent(Member.this,
							Setting.class));
					break;
				case R.id.header_menu:
					Member.this.finish();
					break;
				case R.id.member_QRscan:
					QRscan.setVisibility(View.GONE);
					scanning.setVisibility(View.VISIBLE);
					accessCamera.setVisibility(View.GONE);
					// initial the camera
					mCamera = getCameraInstance();
					autoFocusHandler = new Handler();

					scanner = new ImageScanner();
					scanner.setConfig(0, Config.X_DENSITY, 3);
					scanner.setConfig(0, Config.Y_DENSITY, 3);

					mPreview = new CameraPreview(getApplicationContext(),
							mCamera, previewCb, autoFocusCB);
					qr_containner.addView(mPreview);
					break;
				default:
					break;
				}

			}
		};
		setting.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		QRscan.setOnTouchListener(onTouchListener);

		setting.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
		QRscan.setOnClickListener(onClickListener);

	}

	@Override
	protected void onResume() {
		super.onResume();
		// if the camera is released, reset it
		if (mCamera == null && QRscan.getVisibility() == View.GONE) {
			mCamera = getCameraInstance();
			autoFocusHandler = new Handler();

			scanner = new ImageScanner();
			scanner.setConfig(0, Config.X_DENSITY, 3);
			scanner.setConfig(0, Config.Y_DENSITY, 3);

			mPreview = new CameraPreview(getApplicationContext(), mCamera,
					previewCb, autoFocusCB);
			qr_containner.addView(mPreview);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		// release the camera
		releaseCamera();
		qr_containner.removeView(mPreview);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 1) {
			Member.this.finish();
		}
	}

	// Create and Open Camera
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
		}
		return c;
	}

	// Release the Camera
	private void releaseCamera() {
		if (mCamera != null) {
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	// Check if the String contain digit and char only
	private boolean isAlphaDigit(String name) {
		char[] chars = name.toCharArray();

		for (char c : chars) {
			if (!Character.isLetter(c) && !Character.isDigit(c)) {
				return false;
			}
		}

		return true;
	}

	private void invalidQR() {
		scanning.setText(getResources().getString(R.string.invalidQR));
		mCamera.setPreviewCallback(previewCb);
		mCamera.startPreview();
		previewing = true;
		mCamera.autoFocus(autoFocusCB);
		Log.i("Member", "QR: invalid QR");
	}

    public boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    private class GetWhatsup extends AsyncTask<String, Void, String> {

        private String URL = Database.DATABASE_URL + Database.API_WHATSUP;

        private ProgressDialog loading;

        public GetWhatsup(Context context) {
            loading = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            // show the loading dialog
            loading.setMessage(getResources().getString(R.string.loading));
            loading.setCancelable(false);
            loading.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpGet httpGet = new HttpGet(URL);
            HttpClient httpClient = new DefaultHttpClient();
            String result = "error";

            try {
                // Send request and get data
                HttpResponse httpResponse = httpClient.execute(httpGet);
                result = EntityUtils.toString(httpResponse.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if (loading.isShowing()){
                loading.dismiss();
            }
            try{
                // Check if the information get correctly
                if (!result.equals("error")){
                    JSONObject jsonObject = new JSONObject(result);
                    // Check if there are error in the response
                    if (!jsonObject.getBoolean(Database.TAG_ERROR)){
                        // Get and write the whatsup information
                        List<HashMap<String, String>> listData = new ArrayList<HashMap<String,String>>();
                        JSONArray jsonArray = jsonObject.getJSONArray(Database.TAG_WHATSUP);
                        for (int i=0; i < jsonArray.length() ; i++){
                            if (jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_VIC).equals("1")){
                                HashMap<String, String> data = new HashMap<String, String>();
                                data.put(Database.TAG_WHATSUP_ID, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_ID));
                                data.put(Database.TAG_WHATSUP_VIC, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_VIC));
                                data.put(Database.TAG_WHATSUP_TITLE, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_TITLE));
                                data.put(Database.TAG_WHATSUP_CONTENT, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_CONTENT));
                                data.put(Database.TAG_WHATSUP_IMAGE, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_IMAGE));
                                data.put(Database.TAG_WHATSUP_THUMB, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_THUMB));
                                data.put(Database.TAG_WHATSUP_END_DATE, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_END_DATE));
                                listData.add(data);
                            }
                        }
                        // use the information to set the list view
                        ImagelistAdapter adapter = new ImagelistAdapter(Member.this, listData, 0);
                        list.setAdapter(adapter);
                    }else{
                        Log.i("Whatsup", "Get Whatsup: "+jsonObject.getString(Database.TAG_MESSAGE));
                    }
                }else{
                    Log.i("Whatsup", "Get Whatsup: Get Response Error");
                }
            }catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }


    }

}
