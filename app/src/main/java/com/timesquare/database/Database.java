package com.timesquare.database;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;


public class Database {
	
	// Database URL
	public static final String DATABASE_URL = "http://timessquare.sllin.com/timessquare";
    public static final String DATABASE_NEW_URL = "http://timessquare.sllin.com/ts/api";
    private static final String WINTER_TEMP = "http://timessquare.sllin.com/timessquare_luckydraw/winter_luckydraw2017";
    private static final String NYS_2017_URL = "http://timessquare.sllin.com/timessquare_ticket_luckydraw/ticket_luckydraw2017/";
	public static final String DATABASEVIC_URL = "http://timessquare.sllin.com/timessquare_vic";
	public static final String CARPARK_API_URL = "http://210.5.174.246/tsapp/getentrytime.svc";

    // Version
    public static final String DATABASE_VERSION = "/v1.0";

	// API
	public static final String API_EVENT = "/event";
	public static final String API_FLOORS = "/floors";
	public static final String API_SHOP_CATEGORY = "/shop_categories";
	public static final String API_SHOP = "/shop";
	public static final String API_WHATSUP = "/whatsup";
	public static final String API_GOODIES = "/other";
	public static final String API_GET_GOODIES_BY_CODE = "/code";
	public static final String API_REG_CUSTOMER_GOODIES_CODE = "/goodies/item";
	public static final String API_SPECIAL_EVENT = "/special_event";
	public static final String API_SPECIAL_EVENT_ITEM = "/special_event_item_code";
	public static final String API_VERSION = "/version";
	public static final String API_GET_MEMBER_BY_CARD = "/v1.0.0/member";
    public static final String API_CATCH = "/catch";
	public static final String API_CARPARK_BY_CREADITCARD = "/ByLPNwCreditCard";
	public static final String API_CARPARK_BY_OCTOPUS = "/ByLPNwOctopus";
	public static final String API_WINTER_DRAW_2015 = "/winter_luckydraw2016";
	public static final String API_WINTER_DRAW_2016 = "/winter_luckydraw2016";
	public static final String API_WINTER_DRAW_DRAW = "/draw";

	// FILE PATH
	public static final String PATH_IMAGE = "http://timessquare.sllin.com/timessquare_image";
	public static final String PATH_SPECIAL_EVENT = "http://timessquare.sllin.com/timessquare_gaudi";
	
	// COMMON TAG
    public static final String TAG_STATUS = "status";
	public static final String TAG_ERROR = "error";
	public static final String TAG_MESSAGE = "message";
    public static final String TAG_MESSAGE_CHI = "message_chi";
    public static final String TAG_DATA = "data";
	public static final String TAG_DEVICE_TYPE = "device_type";
	
	// EVENT TAG
	public static final String TAG_EVENT = "event";
	public static final String TAG_EVENT_ID = "event_id";
	public static final String TAG_EVENT_DATE_TIME = "event_date_time";
	public static final String TAG_EVENT_TITLE = "event_title";
	public static final String TAG_EVENT_DETAIL = "event_detail";
	
	// Floor
	public static final String TAG_FLOOR = "floors"; 
	public static final String TAG_FLOOR_ID = "floor_id"; 
	public static final String TAG_FLOOR_NAME = "floor_name"; 
	
	// Shop Category
	public static final String TAG_SHOP_CATEGORY = "shop_categories";  
	public static final String TAG_SHOP_CATEGORY_NAME = "shop_category_name"; 
	
	// Shop
	public static final String TAG_SHOP = "shops";
	public static final String TAG_SHOP_ID = "shop_id";
	public static final String TAG_SHOP_NUMBER = "shop_number";
	public static final String TAG_SHOP_NAME = "shop_name";
	public static final String TAG_SHOP_FLOOR_ID = "shop_floor_id";
	public static final String TAG_SHOP_DESCRIPTION_ENG = "shop_description_eng";
	public static final String TAG_SHOP_DESCRIPTION_CHI = "shop_description_chi";
	public static final String TAG_SHOP_IMAGE = "shop_image";
	public static final String TAG_SHOP_PHONE = "shop_phone";
	public static final String TAG_SHOP_CATEGORY_ID = "shop_category_id";

	// WHATSUP TAG
	public static final String TAG_WHATSUP = "whatsup";
	public static final String TAG_WHATSUP_ID = "whatsup_id";
    public static final String TAG_WHATSUP_VIC = "whatsup_vic";
	public static final String TAG_WHATSUP_CAR_PARK = "whatsup_car_park";
	public static final String TAG_WHATSUP_TITLE = "whatsup_title";
	public static final String TAG_WHATSUP_IMAGE = "whatsup_image";
	public static final String TAG_WHATSUP_THUMB = "whatsup_thumb";
	public static final String TAG_WHATSUP_CONTENT = "whatsup_content";
	public static final String TAG_WHATSUP_END_DATE = "whatsup_end_date";
	
	// GOODIES TAG
	public static final String TAG_GOODIES = "goodies";
	public static final String TAG_GOODIES_ID = "goodies_id";
	public static final String TAG_GOODIES_TITLE = "goodies_title";
	public static final String TAG_GOODIES_DESCRIPTION = "goodies_description";
	public static final String TAG_GOODIES_DESCRIPTION_CHI = "goodies_description_chi";
	public static final String TAG_GOODIES_IMAGE = "goodies_image";
	public static final String TAG_GOODIES_THUMB = "goodies_thumb";
	public static final String TAG_GOODIES_CODE = "goodies_code";
	public static final String TAG_GOODIES_REDEEMABLE = "goodies_redeemable";
	public static final String TAG_GOODIES_NEEDED_DAYS = "goodies_needed_days";
	public static final String TAG_GOODIES_CUSTOMER_CODE = "goodies_code";		
	
	// MEMBER TAG
	public static final String TAG_MEMBER = "card_pin";		
	public static final String TAG_MEMBER_NAME = "member_last_name";		
	public static final String TAG_MEMBER_POINT = "member_point";		
	public static final String TAG_MEMBER_EXPIREDATE = "member_point_expiry_date";		
	public static final String TAG_MEMBER_EMAIL = "member_email";
	public static final String TAG_MEMBER_CODE = "card_code";
	public static final String TAG_MEMBER_TEL = "member_tel";


	// SPECIAL EVENT TAG
	public static final String TAG_SPECIAL_EVENT_NAME = "special_event_name";
	public static final String TAG_SPECIAL_EVENT_STATUS = "special_event_status";
	public static final String TAG_SPECIAL_EVENT_ITEM_ID = "special_event_item_id";
	public static final String TAG_SPECIAL_EVENT_ITEM_TYPE = "special_event_item_type";
	public static final String TAG_SPECIAL_EVENT_ITEM_NAME = "special_event_item_name";
	public static final String TAG_SPECIAL_EVENT_ITEM_IMAGE = "special_event_item_image";
	public static final String TAG_SPECIAL_EVENT_ITEM_SOUND = "special_event_item_sound";
	public static final String TAG_SPECIAL_EVENT_ITEM_SOUND_CHINESE = "special_event_item_sound_chinese";
	public static final String TAG_SPECIAL_EVENT_ITEM_TEXT = "special_event_item_text";
	public static final String TAG_SPECIAL_EVENT_ITEM_TEXT_CHINESE = "special_event_item_text_chinese";
	public static final String TAG_SPECIAL_EVENT_ITEM_CODE ="special_event_item_code";
	public static final String TAG_SPECIAL_EVENT_ITEM_VIDEO_URL ="special_event_item_video_url";
	public static final String TAG_SPECIAL_EVENT_VIDEO_START_TIME ="special_event_video_start_time";
	public static final String TAG_SPECIAL_EVENT_VIDEO_END_TIME ="special_event_video_end_time";
	
	// VERSION TAG
	public static final String TAG_ANDROID_LATEST_VERSION = "andriod_latest_version";
	public static final String TAG_ANDROID_CRITICAL_VERSION = "andriod_critical_version";

    // CATCH TAG
    public static final String TAG_CATCH_UID = "uid";
    public static final String TAG_CATCH_QR_CODE = "qr_code";
    public static final String TAG_CATCH_ID = "id";
    public static final String TAG_CATCH_FINISH = "finish";
    public static final String TAG_CATCH_REDEEM = "redeem";
    public static final String TAG_CATCH_DATETIME = "datetime";
    public static final String TAG_CATCH_NUMBER = "number";

	public static final String TAG_CATCH_BASE_URL = "http://timessquare.sllin.com/qr_game";
	public static final String TAG_CATCH_START_URL = "/start";
	public static final String TAG_CATCH_START_PARAM = "language";
	public static final String TAG_CATCH_NEXT_URL = "/next";
	public static final String TAG_CATCH_NEXT_PARAM1 = "language";
	public static final String TAG_CATCH_NEXT_PARAM2 = "uuid";
	public static final String TAG_CATCH_SCAN_URL = "/catch";
	public static final String TAG_CATCH_SCAN_PARAM1 = "language";
	public static final String TAG_CATCH_SCAN_PARAM2 = "uuid";
	public static final String TAG_CATCH_SCAN_PARAM3 = "qr_code";

    public static final String TAG_CATCH_CHARSET = "UTF-8";
    public static final String TAG_CATCH_CHARFORMAT = "text/html; charset=utf-8";
	public static final String TAG_CATCH_ACTION = "action:";
	public static final String TAG_CATCH_DISMISS = "dismiss";
	public static final String TAG_CATCH_POST = "post";
	public static final String TAG_CATCH_SCAN = "scan";


    public static final String TAG_CATCH_JSON_HTML = "html";
    public static final String TAG_CATCH_JSON_ACTION = "action";
    public static final String TAG_CATCH_JSON_PUSH = "push";
    public static final String TAG_CATCH_JSON_ALERT = "alert";
	public static final String TAG_CATCH_JSON_DEFAULT = "default";
	public static final String TAG_CATCH_JSON_CONTENT = "content";


	// WINTER DRAW TAGs
	private static final String API_AUTHENTICATION = "authentication";
	private static final String API_AUTHENTICATION_VALUE = "sllin";
	public static final String TAG_DRAW_SUCCESS = "draw_success";
	public static final String TAG_PRIZE_ID = "prize_id";
	public static final String TAG_PRIZE_NUM = "prize_no";
	public static final String TAG_PRIZE_NAME = "prize_name";
	public static final String TAG_PRIZE_NAME_ZH = "prize_name_zh";
	public static final String TAG_DRAW_CREATE_AT = "draw_create_at";
	public static final String TAG_RUNNING_NUM = "running_no";
	public static final String TAG_NEXT_RUNNING_NUM = "next_running_no";
	public static final String TAG_RESULT = "result";
	public static final String TAG_DATETIME = "datetime";
	public static final String TAG_TICKET_ID = "ticket_id";
	public static final String TAG_IS_COLLECTED = "is_collected";

	public static final String API_SECRET_AUTHENTICATION_VALUE = "4814923223523178";

    private static AsyncHttpClient client = new AsyncHttpClient();
    private static AsyncHttpClient clientSE = new AsyncHttpClient();

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler){
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return DATABASE_NEW_URL + DATABASE_VERSION + relativeUrl;
    }

	public static void postQR(String url, RequestParams params, AsyncHttpResponseHandler responseHandler){
		client.post(getAbsoluteUrlQR(url), params, responseHandler);
	}

	private static String getAbsoluteUrlQR(String relativeUrl) {
		return TAG_CATCH_BASE_URL + relativeUrl;
	}

	public static void winterDrawGet(String url, AsyncHttpResponseHandler responseHandler){
		Log.d("Database", DATABASE_URL + API_WINTER_DRAW_2016 + url);
		clientSE.addHeader(API_AUTHENTICATION, API_AUTHENTICATION_VALUE);
//		client.get( "http://www.sllin.com", null, responseHandler);
		clientSE.get( WINTER_TEMP + url, null, responseHandler);
	}

	public static void nys2017(String subLink, AsyncHttpResponseHandler handler){
		clientSE.addHeader(API_AUTHENTICATION, API_AUTHENTICATION_VALUE);
		clientSE.get(NYS_2017_URL + subLink, null, handler);
	}

	public static void nys2017(String subLink, RequestParams param, AsyncHttpResponseHandler handler){
		clientSE.addHeader(API_AUTHENTICATION, API_AUTHENTICATION_VALUE);
		clientSE.post(NYS_2017_URL + subLink, param, handler);
	}

	public static void nys2017NewDatabase(String sublink, AsyncHttpResponseHandler handler){
		clientSE.addHeader(API_AUTHENTICATION, API_SECRET_AUTHENTICATION_VALUE);
		clientSE.get(NYS_2017_URL + sublink, null, handler);
	}

//	public static void winterTemp(String url, AsyncHttpResponseHandler responseHandler){
//		Log.d("Database", url);
//		client.addHeader(API_AUTHENTICATION, API_AUTHENTICATION_VALUE);
//		client.get( url, null, responseHandler);
//	}

}
