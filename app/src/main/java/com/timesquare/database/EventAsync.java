package com.timesquare.database;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.timesquare.localdatabase.LocalDatabaseHelper;
import com.timessquare.R;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class EventAsync extends AsyncTask<String, Void, String>{
	
	private Context context;
	private String URL = Database.DATABASE_URL + Database.API_EVENT;
	private ProgressDialog loading;

	private LocalDatabaseHelper db;
	
	public EventAsync(Context context) {
		this.context = context;
		db = new LocalDatabaseHelper(context);
		loading = new ProgressDialog(context);
	}
	
	@Override
	protected void onPreExecute() {
		// show the loading dialog
		loading.setMessage(context.getResources().getString(R.string.loading));
		loading.setCancelable(false);
		loading.show();
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... arg0) {
		//get event JSON from server
		HttpGet httpGet = new HttpGet(URL);
		HttpClient httpClient = new DefaultHttpClient();
		String result = "error";
		
		try {
			HttpResponse httpResponse = httpClient.execute(httpGet);
			StatusLine statusLine = httpResponse.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				result = EntityUtils.toString(httpResponse.getEntity());
			}		
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
		// if no error, write event data to the local database
		if(!result.equals("error")){
			try {
				JSONObject jsonObject = new JSONObject(result);				
				Boolean error = jsonObject.getBoolean(Database.TAG_ERROR);
				if (!error){
					db.deleteAllEvent();
					JSONArray jsonArray = jsonObject.getJSONArray(Database.TAG_EVENT);
					for (int i=0; i<jsonArray.length(); i++){
						JSONObject jsonObject2 = jsonArray.getJSONObject(i);
						String event_id = jsonObject2.getString(Database.TAG_EVENT_ID);
						String event_date_time = jsonObject2.getString(Database.TAG_EVENT_DATE_TIME);
						String event_title = jsonObject2.getString(Database.TAG_EVENT_TITLE);
						String event_detail = jsonObject2.getString(Database.TAG_EVENT_DETAIL);
						
						ContentValues values = new ContentValues();
						values.put(LocalDatabaseHelper.EVENT_ID, event_id);
						values.put(LocalDatabaseHelper.EVENT_DATE_TIME, event_date_time);
						values.put(LocalDatabaseHelper.EVENT_TITLE, event_title);
						values.put(LocalDatabaseHelper.EVENT_DETAIL, event_detail);
						Log.i("event", "event_id: "+event_id+" event_date_time: "+event_date_time+" event_title: "+event_title+" event_detail: "+ event_detail);
//						context.getContentResolver().insert(CalendarProvider.CONTENT_URI, values);	
						db.addEvent(values);
					}				
				}				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		if (loading.isShowing()){
			loading.dismiss();
		}
	}

}
