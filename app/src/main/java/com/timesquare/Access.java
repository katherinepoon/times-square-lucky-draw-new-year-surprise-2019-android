//package com.timesquare;
//
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapFragment;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.LatLng;
//import com.sllin.imageloader.ImageLoader;
//import com.timessquare.R;
//
//import uk.co.senab.photoview.PhotoViewAttacher;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnTouchListener;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
///**
// * Access Page
// * - Show the map
// *
// */
//public class Access extends FragmentActivity implements OnMapReadyCallback{
//
//	private LinearLayout setting, map_root, contact_us_root;
//	private TextView menu, access_text, show_text, show_google_map, show_map_jpg;
//	private ImageView map_jpg;
//	private Fragment map_google;
//
//	private ImageLoader imageLoader;
//	PhotoViewAttacher attacher;
//
//	static final LatLng location = new LatLng(22.278167, 114.182252);
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.header);
//		View.inflate(this, R.layout.access, (ViewGroup) findViewById(R.id.header));
//
//		// Get Font Type
//		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font));
//
//		// initialize the object
//		TextView title = (TextView) findViewById(R.id.header_title);
//		TextView warning = (TextView) findViewById(R.id.access_warning);
//		ImageView warning_line = (ImageView) findViewById(R.id.access_warning_line);
//
//        map_google = (Fragment) getSupportFragmentManager().findFragmentById(R.id.access_map);
////		GoogleMap map = ((MapFragment) getFragmentManager().findFragmentById(R.id.access_map)).getMap();
////		map_google = (Fragment) getFragmentManager().findFragmentById(R.id.access_map);
//		map_jpg = (ImageView) findViewById(R.id.access_map_jpg);
//		access_text = (TextView) findViewById(R.id.access_text);
//		show_text = (TextView) findViewById(R.id.access_show_text);
//		show_google_map = (TextView) findViewById(R.id.access_show_map);
//		show_map_jpg = (TextView) findViewById(R.id.access_show_map_jpg);
//		setting = (LinearLayout) findViewById(R.id.header_setting);
//		map_root = (LinearLayout) findViewById(R.id.access_map_root);
//		contact_us_root = (LinearLayout) findViewById(R.id.access_contact_us);
//		TextView setting_text = (TextView) findViewById(R.id.header_setting_text);
//		menu = (TextView) findViewById(R.id.header_menu);
//		TextView hint = (TextView) findViewById(R.id.access_zoomHint);
//
//		findViewById(R.id.header).setBackgroundResource(R.drawable.access_bg);
//
//		// set variable
//		title.setTypeface(font);
//		warning.setTypeface(font);
//		setting_text.setTypeface(font);
//		menu.setTypeface(font);
//		hint.setTypeface(font);
//
//		title.setText(R.string.access);
////		warning.setVisibility(View.INVISIBLE);
////		warning_line.setVisibility(View.INVISIBLE);
//		map_google.getView().setVisibility(View.GONE);
//		map_root.setVisibility(View.GONE);
//
//        ((SupportMapFragment) map_google).getMapAsync(this);
//
//
//        createContactUsView();
//
//		imageLoader = new ImageLoader(this);
//		attacher = new PhotoViewAttacher(map_jpg);
//
//		buttonOn(show_text);
//
////		if (map == null){
////			Log.i("Access", "Map Error");
////		}else{
////			map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 18));
////		}
//		Log.i("Access", "All object is initialized.");
//
//		// Initialize the listener
//		OnTouchListener onTouchListener = new OnTouchListener() {
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				if (event.getAction() == MotionEvent.ACTION_DOWN){
//					v.setAlpha(0.3f);
//				}
//				if (event.getAction() == MotionEvent.ACTION_UP){
//					v.setAlpha(1f);
//				}
//				return false;
//			}
//		};
//
//		OnClickListener onClickListener = new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				switch (v.getId()){
//					case R.id.header_setting:
//						Access.this.startActivity(new Intent(Access.this, Setting.class));
//						break;
//					case R.id.header_menu:
//						Access.this.finish();
//						break;
//					case R.id.access_show_text:
//						access_text.setVisibility(View.VISIBLE);
//						map_google.getView().setVisibility(View.GONE);
//						map_root.setVisibility(View.GONE);
//						buttonOn(show_text);
//						buttonOff(show_google_map);
//						buttonOff(show_map_jpg);
//						break;
//					case R.id.access_show_map:
//						access_text.setVisibility(View.GONE);
//						map_google.getView().setVisibility(View.VISIBLE);
//						map_root.setVisibility(View.GONE);
//						buttonOff(show_text);
//						buttonOn(show_google_map);
//						buttonOff(show_map_jpg);
//						break;
//					case R.id.access_show_map_jpg:
//						access_text.setVisibility(View.GONE);
//						map_google.getView().setVisibility(View.GONE);
//						map_root.setVisibility(View.VISIBLE);
//						buttonOff(show_text);
//						buttonOff(show_google_map);
//						buttonOn(show_map_jpg);
//						break;
//					case R.id.access_warning:
//						AlertDialog.Builder builder1 = new AlertDialog.Builder(Access.this);
//						builder1.setMessage(Access.this.getResources().getString(R.string.access_statement_detail));
//						builder1.setCancelable(true);
//						builder1.setPositiveButton(Access.this.getResources().getString(R.string.back),
//								new DialogInterface.OnClickListener() {
//									public void onClick(DialogInterface dialog, int id) {
//										dialog.cancel();
//									}
//								});
//						AlertDialog alert11 = builder1.create();
//						alert11.show();
//						break;
//					default:
//						break;
//				}
//
//			}
//		};
//
//		// set listener
//		setting.setOnClickListener(onClickListener);
//		setting.setOnTouchListener(onTouchListener);
//		menu.setOnClickListener(onClickListener);
//		menu.setOnTouchListener(onTouchListener);
//		show_text.setOnClickListener(onClickListener);
//		show_google_map.setOnClickListener(onClickListener);
//		show_map_jpg.setOnClickListener(onClickListener);
//		warning.setOnClickListener(onClickListener);
//
//		Log.i("Access", "Listener is set.");
//	}
//
//	@Override
//	public void onMapReady(GoogleMap map) {
//		if (map == null){
//			Log.i("Access", "Map Error");
//		}else{
//            Log.i("Access", "Initializing Map");
//            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 18));
//		}
//	}
//
//	private void createContactUsView(){
////		web, Fb, ig , weibo , wechat
//		String[] method = getResources().getStringArray(R.array.access_contact_us);
//		final String[] links = getResources().getStringArray(R.array.access_contact_us_links);
//		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//		int height = (int)(45f * displayMetrics.density + 0.5f);
//
//		View.OnClickListener listener = new View.OnClickListener(){
//			@Override
//			public void onClick(View view) {
//				String link = view.getTag().toString();
//				if (link.startsWith("weixin")){
//					// special handle for wechat
//					AlertDialog dialog = new AlertDialog.Builder(Access.this)
//							.setMessage(R.string.access_wechat_official_account)
//							.setNeutralButton(R.string.back, null)
//							.setCancelable(true)
//							.create();
//					dialog.show();
//
//
//				}else {
//					Intent intent = new Intent(Intent.ACTION_VIEW);
//					intent.setData(Uri.parse(link));
//					startActivity(intent);
//				}
//			}
//		};
//
//		for (int i = 0; i < method.length; i++){
//			String item = method[i];
//			TextView textView = new TextView(this);
//			textView.setLayoutParams(new LinearLayout.LayoutParams(0, height, 1));
//			textView.setGravity(Gravity.CENTER);
//			textView.setText(item);
//			textView.setTag(links[i]);
//			textView.setOnClickListener(listener);
//
//			contact_us_root.addView(textView);
//
//		}
//	}
//
//
//	private void buttonOn(TextView textView){
//		textView.setTextColor(this.getResources().getColor(R.color.ThemeColor_black));
////		textView.setBackgroundColor(this.getResources().getColor(R.color.ThemeColor_deepBlue));
//	}
//
//	private void buttonOff(TextView textView){
//		textView.setTextColor(this.getResources().getColor(R.color.ThemeColor_deepBlue));
////		textView.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
//	}
//}
