package com.timesquare;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.timesquare.database.Database;
import com.timesquare.setting.ImagelistAdapter;
import com.timessquare.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Whatsup extends Activity{
	
	private TextView menu;
	private LinearLayout setting;
	private ListView list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header);
		View.inflate(this, R.layout.imagelist, (ViewGroup) findViewById(R.id.header));
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
		
		// Initialize the object
		setting = (LinearLayout) findViewById(R.id.header_setting);
		TextView setting_text = (TextView) findViewById(R.id.header_setting_text);
		TextView title = (TextView) findViewById(R.id.header_title);
		menu = (TextView) findViewById(R.id.header_menu);
		TextView warning = (TextView) findViewById(R.id.imagelist_warning);
		list = (ListView) findViewById(R.id.imagelist_list);
		
		// Set Variable
		setting_text.setTypeface(font);
		title.setTypeface(font);
		menu.setTypeface(font);
		warning.setTypeface(font);
		
 		title.setText(R.string.mainmenu_whatsup);
 		
 		
 		if (isOnline()){
 			new GetWhatsup(this).execute("");
		}else{
			Log.i("Whatsup", "No Internet Connection");
			Toast.makeText(this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
		}
 		
 		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
 		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_setting:
					Whatsup.this.startActivity(new Intent(Whatsup.this, Setting.class));
					break;
				case R.id.header_menu:
					Whatsup.this.finish();
					break;
				default:
					break;
				}
				
			}
		};
		setting.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		setting.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
	}
	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		if (requestCode == 1){
//			Whatsup.this.finish();
//		}
//	}
	
	/**
	 * Check if there are internet connection
	 * @return boolean
	 */
	public boolean isOnline(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	
	private class GetWhatsup extends AsyncTask<String, Void, String>{
		
		private String URL = Database.DATABASE_URL + Database.API_WHATSUP;
		
		private ProgressDialog loading;
		
		public GetWhatsup(Context context) {
			loading = new ProgressDialog(context);
		}
		
		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";
			
			try {
				// Send request and get data
				HttpResponse httpResponse = httpClient.execute(httpGet);
				result = EntityUtils.toString(httpResponse.getEntity());				
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {			
			if (loading.isShowing()){
				loading.dismiss();
			}
			try{
				// Check if the information get correctly
				if (!result.equals("error")){
					JSONObject jsonObject = new JSONObject(result);
					// Check if there are error in the response
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						// Get and write the whatsup information
						List<HashMap<String, String>> listData = new ArrayList<HashMap<String,String>>();
						JSONArray jsonArray = jsonObject.getJSONArray(Database.TAG_WHATSUP);
						for (int i=0; i < jsonArray.length() ; i++){
                            if (jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_VIC).equals("0")){
                                HashMap<String, String> data = new HashMap<String, String>();
                                data.put(Database.TAG_WHATSUP_ID, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_ID));
                                data.put(Database.TAG_WHATSUP_VIC, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_VIC));
                                data.put(Database.TAG_WHATSUP_TITLE, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_TITLE));
                                data.put(Database.TAG_WHATSUP_CONTENT, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_CONTENT));
                                data.put(Database.TAG_WHATSUP_IMAGE, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_IMAGE));
                                data.put(Database.TAG_WHATSUP_THUMB, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_THUMB));
                                data.put(Database.TAG_WHATSUP_END_DATE, jsonArray.getJSONObject(i).getString(Database.TAG_WHATSUP_END_DATE));
                                listData.add(data);
                            }
						}
						// use the information to set the list view
						ImagelistAdapter adapter = new ImagelistAdapter(Whatsup.this, listData, 0);
						list.setAdapter(adapter);						
					}else{
						Log.i("Whatsup", "Get Whatsup: "+jsonObject.getString(Database.TAG_MESSAGE));
					}
				}else{
					Log.i("Whatsup", "Get Whatsup: Get Response Error");
				}
			}catch (JSONException e) {
				e.printStackTrace();
			}
			super.onPostExecute(result);
		}
		
		
	}
	
	
}
