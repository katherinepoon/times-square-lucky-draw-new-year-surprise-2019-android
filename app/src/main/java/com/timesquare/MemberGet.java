package com.timesquare;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.timesquare.database.Database;
import com.timessquare.R;

public class MemberGet extends Activity {

	private TextView back, menu, code, name, pin, point, tel, date, email, point2014;

	// private ViewPager pager;
	// private LinearLayout pointer;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header_withback);
		View.inflate(this, R.layout.member_listview,
				(ViewGroup) findViewById(R.id.header_withback));

		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources()
				.getString(R.string.font));

		// Initialize the object
		TextView title = (TextView) findViewById(R.id.header_withback_title);
		back = (TextView) findViewById(R.id.header_withback_back);
		menu = (TextView) findViewById(R.id.header_withback_menu);
		code = (TextView) findViewById(R.id.imageViewer_warning);
		// pager = (ViewPager) findViewById(R.id.goodies_item_viewPager);
		// pointer = (LinearLayout) findViewById(R.id.goodies_item_pointer);
		name = (TextView) findViewById(R.id.name_);
		point = (TextView) findViewById(R.id.point);
		point2014 = (TextView) findViewById(R.id.point2014);
		tel = (TextView) findViewById(R.id.tel);
		email = (TextView) findViewById(R.id.email);
		date = (TextView) findViewById(R.id.date);
		pin = (TextView) findViewById(R.id.pin_);
		// Set Variable
		title.setTypeface(font);
		back.setTypeface(font);
		menu.setTypeface(font);

		// code.setGravity(Gravity.RIGHT);
		// code.setTextSize(14);
		title.setText(R.string.mainmenu_member);

		Intent intent = getIntent();

		if (isOnline()) {
			new GetMember(this, intent.getExtras().getString(
					Database.TAG_MEMBER_CODE)).execute("");
		} else {
			Log.i("MemberGet", "No Internet Connection");
			Toast.makeText(
					MemberGet.this,
					getResources()
							.getString(R.string.ErrorNoInternetConnection),
					Toast.LENGTH_SHORT).show();
		}

		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP) {
					v.setAlpha(1f);
				}
				return false;
			}
		};

		View.OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.header_withback_back:
					MemberGet.this.finish();
					break;
				case R.id.header_withback_menu:
					setResult(1);
					MemberGet.this.finish();
					break;
				default:
					break;
				}

			}
		};

		back.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		back.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
	}

	/**
	 * Check if there are internet connection
	 * 
	 * @return boolean
	 */
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	private class GetMember extends AsyncTask<String, Void, String> {

		private Context context;

		private String URL = Database.DATABASEVIC_URL
				+ Database.API_GET_MEMBER_BY_CARD;

		private ProgressDialog loading;

		public GetMember(Context context, String code) {
			loading = new ProgressDialog(context);
			URL += "/" + code;
			this.context = context;
			Log.i("test", URL);
		}

		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			HttpGet httpGet = new HttpGet(URL);
			httpGet.addHeader("authentication", "ojPhrwatQEY2d6etoWHn");
			HttpClient httpClient = new DefaultHttpClient();
			String result = "status";

			try {
				// Send request and get data
				HttpResponse httpResponse = httpClient.execute(httpGet);
				result = EntityUtils.toString(httpResponse.getEntity());
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (loading.isShowing()) {
				loading.dismiss();
			}
			try {
				// Check if the information get correctly
				if (!result.equals("status")) {
					JSONObject jsonObject = new JSONObject(result);
					
					String test = jsonObject.getString("data");
//					Log.i("123", member.toString());
					// Check if there are error in the response
					Log.i("test", test);
					if (test == "null") {

						Log.i("Member",
								"Get Member: "
										+ jsonObject
												.getString(Database.TAG_MESSAGE));
						((MemberGet) context).finish();

					} else {
						JSONObject member = jsonObject.getJSONObject("data");
						String a = member.getString(Database.TAG_MEMBER_EXPIREDATE);
						pin.setText(member.getString(Database.TAG_MEMBER));

						name.setText(member.getString(Database.TAG_MEMBER_NAME));
						point.setText(member
								.getString(Database.TAG_MEMBER_POINT) + " pts.");
						point2014.setText(member
								.getString("member_expiried_2014") + " pts.");
						date.setText("[ "
								+ a.substring(0,10)
								+ " ]");
						tel.setText(member.getString(Database.TAG_MEMBER_TEL));
						email.setText(member
								.getString(Database.TAG_MEMBER_EMAIL));

					}
				} else {

					Log.i("Member", "Get Member: Get Response Error");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			super.onPostExecute(result);

		}
	}
}