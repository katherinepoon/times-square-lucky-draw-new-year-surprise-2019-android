package com.timesquare;

import java.io.IOException;
import java.util.Locale;

import com.sllin.imageloader.ImageLoader;
import com.timesquare.database.Database;
import com.timessquare.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;

public class SpecialEventItem extends Activity{
	
	private ImageView back, menu, play, background;
	private TextView title, text, message;
	
	private String URL = Database.PATH_SPECIAL_EVENT;
	
	private Intent intent;
	
	private MediaPlayer mediaPlayer;
	private boolean isPlaying;
	
	private ImageLoader imageLoader;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.special_event_item);
		
		// Get Intent
		intent = getIntent();
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
				
		
		// Initial All the object
		TextView header_title = (TextView) findViewById(R.id.specialEventItem_header_title);
		back = (ImageView) findViewById(R.id.specialEventItem_header_back);
		menu = (ImageView) findViewById(R.id.specialEventItem_header_menu);
		play = (ImageView) findViewById(R.id.specialEventItem_playbutton);
		background = (ImageView) findViewById(R.id.specialEventItem_background);
		title = (TextView) findViewById(R.id.specialEventItem_title);
		text = (TextView) findViewById(R.id.specialEventItem_text);
		message = (TextView) findViewById(R.id.specialEventItem_message);
		
		mediaPlayer = new MediaPlayer();
		imageLoader = new ImageLoader(this);
		
		// Set the object
		header_title.setTypeface(font);
		title.setTypeface(font);
		text.setTypeface(font);
		message.setTypeface(font);
		
		imageLoader.DisplayImage(URL + "/" + intent.getExtras().getString(Database.TAG_SPECIAL_EVENT_ITEM_IMAGE), background);
		
		if (Locale.getDefault().getLanguage().equals("zh")){
			back.setBackgroundResource(R.drawable.back_special_chin);
			menu.setBackgroundResource(R.drawable.menu_special_chin);
			title.setText(intent.getExtras().getString(Database.TAG_SPECIAL_EVENT_ITEM_NAME));
			text.setText(intent.getExtras().getString(Database.TAG_SPECIAL_EVENT_ITEM_TEXT_CHINESE));
			URL += "/" + intent.getExtras().getString(Database.TAG_SPECIAL_EVENT_ITEM_SOUND_CHINESE);
		}else{
			back.setBackgroundResource(R.drawable.back_special_eng);
			menu.setBackgroundResource(R.drawable.menu_special_eng);
			title.setText(intent.getExtras().getString(Database.TAG_SPECIAL_EVENT_ITEM_NAME));
			text.setText(intent.getExtras().getString(Database.TAG_SPECIAL_EVENT_ITEM_TEXT));
			URL += "/" + intent.getExtras().getString(Database.TAG_SPECIAL_EVENT_ITEM_SOUND);
		}
		
		try {
			Log.i("test", URL);
			mediaPlayer.reset();
			mediaPlayer.setDataSource(URL);
			mediaPlayer.prepare();
			isPlaying = false;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
				
		OnClickListener onClickListener = new OnClickListener(){			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.specialEventItem_header_back:
					SpecialEventItem.this.finish();
					break;
				case R.id.specialEventItem_header_menu:
					setResult(1);
					SpecialEventItem.this.finish();
					break;
				case R.id.specialEventItem_playbutton:
					if (isPlaying){
						isPlaying = false;
						mediaPlayer.pause();
						play.setImageResource(R.drawable.play_button);
					}else{
						isPlaying = true;
						mediaPlayer.start();
						play.setImageResource(R.drawable.pause_button);
					}					
					break;
				default: 
					break;
				}
				
			}
		};
		
		back.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		play.setOnTouchListener(onTouchListener);
		
		back.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
		play.setOnClickListener(onClickListener);
	}
	
	
}
