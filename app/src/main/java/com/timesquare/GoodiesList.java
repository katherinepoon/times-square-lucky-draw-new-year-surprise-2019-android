package com.timesquare;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.timesquare.database.Database;
import com.timesquare.setting.ImagelistAdapter;
import com.timessquare.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class GoodiesList extends Activity {
	
	private TextView menu;
	private LinearLayout setting;
	private ListView list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header);
		View.inflate(this, R.layout.imagelist, (ViewGroup) findViewById(R.id.header));
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
		
		// Initialize the object
		setting = (LinearLayout) findViewById(R.id.header_setting);
		TextView setting_text = (TextView) findViewById(R.id.header_setting_text);
		TextView title = (TextView) findViewById(R.id.header_title);
		menu = (TextView) findViewById(R.id.header_menu);
		TextView warning = (TextView) findViewById(R.id.imagelist_warning);
		list = (ListView) findViewById(R.id.imagelist_list);
		
		// Set Variable
		setting_text.setTypeface(font);
		title.setTypeface(font);
		menu.setTypeface(font);
		warning.setTypeface(font);
		
 		title.setText(R.string.mainmenu_goodies);
 		if (isOnline()){
 			new GetGoodies(this).execute("");
		}else{
			Log.i("GoodiesList", "No Internet Connection");
			Toast.makeText(this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
		}
 		
 		
 		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
 		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_setting:
					GoodiesList.this.startActivity(new Intent(GoodiesList.this, Setting.class));
					break;
				case R.id.header_menu:
					setResult(1);
					GoodiesList.this.finish();
					break;
				default:
					break;
				}
				
			}
		};
		setting.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		setting.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 1){
			setResult(1);
			GoodiesList.this.finish();
		}
	}
	
	/**
	 * Check if there are internet connection
	 * @return boolean
	 */
	public boolean isOnline(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	private class GetGoodies extends AsyncTask<String, Void, String>{
		
		private String URL = Database.DATABASE_URL + Database.API_GOODIES;
		
		private ProgressDialog loading;
		
		public GetGoodies(Context context) {
			loading = new ProgressDialog(context);
		}
		
		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";
			
			try {
				// Send request and get data
				HttpResponse httpResponse = httpClient.execute(httpGet);
				result = EntityUtils.toString(httpResponse.getEntity());				
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {			
			if (loading.isShowing()){
				loading.dismiss();
			}
			try{
				// Check if the information get correctly
				if (!result.equals("error")){
					JSONObject jsonObject = new JSONObject(result);
					// Check if there are error in the response
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						// Get and write the goodies information
						List<HashMap<String, String>> listData = new ArrayList<HashMap<String,String>>();
						JSONArray jsonArray = jsonObject.getJSONArray(Database.TAG_GOODIES);
						for (int i=0; i < jsonArray.length() ; i++){
							HashMap<String, String> data = new HashMap<String, String>();
							data.put(Database.TAG_GOODIES_ID, jsonArray.getJSONObject(i).getString(Database.TAG_GOODIES_ID));
							data.put(Database.TAG_GOODIES_TITLE, jsonArray.getJSONObject(i).getString(Database.TAG_GOODIES_TITLE));
							data.put(Database.TAG_GOODIES_DESCRIPTION, jsonArray.getJSONObject(i).getString(Database.TAG_GOODIES_DESCRIPTION));
							data.put(Database.TAG_GOODIES_IMAGE, jsonArray.getJSONObject(i).getString(Database.TAG_GOODIES_IMAGE));
							data.put(Database.TAG_GOODIES_THUMB, jsonArray.getJSONObject(i).getString(Database.TAG_GOODIES_THUMB));
							data.put(Database.TAG_GOODIES_CODE, jsonArray.getJSONObject(i).getString(Database.TAG_GOODIES_CODE));
							data.put(Database.TAG_GOODIES_REDEEMABLE, jsonArray.getJSONObject(i).getString(Database.TAG_GOODIES_REDEEMABLE));
							data.put(Database.TAG_GOODIES_NEEDED_DAYS, jsonArray.getJSONObject(i).getString(Database.TAG_GOODIES_NEEDED_DAYS));
							Log.i("test", jsonArray.getJSONObject(i).getString(Database.TAG_GOODIES_THUMB));
							listData.add(data);
						}
						// use the information to set the list view
						ImagelistAdapter adapter = new ImagelistAdapter(GoodiesList.this, listData, 1);
						list.setAdapter(adapter);						
					}else{
						Log.i("Goodies", "Get Goodies: "+jsonObject.getString(Database.TAG_MESSAGE));
					}
				}else{
					Log.i("Goodies", "Get Goodies: Get Response Error");
				}
			}catch (JSONException e) {
				e.printStackTrace();
			}
			super.onPostExecute(result);
		}
		
		
	}
}
