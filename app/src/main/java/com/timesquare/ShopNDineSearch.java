package com.timesquare;


import com.timesquare.localdatabase.LocalDatabaseHelper;
import com.timesquare.setting.ShopSearchAdapter;
import com.timessquare.R;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class ShopNDineSearch extends Activity{
	
	private TextView back, menu;
	private EditText search;
	private ListView list;
	
	private ShopSearchAdapter adapter;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header_withback);
		View.inflate(this, R.layout.shopndine_search, (ViewGroup) findViewById(R.id.header_withback));
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 		
		
		// Initialize the object
		back = (TextView) findViewById(R.id.header_withback_back);
		TextView title = (TextView) findViewById(R.id.header_withback_title);
		menu = (TextView) findViewById(R.id.header_withback_menu);
		search = (EditText) findViewById(R.id.shopndine_search_editText);
		list = (ListView) findViewById(R.id.shopndine_search_list);
		
		// set variable
		back.setTypeface(font);
		title.setTypeface(font);
		menu.setTypeface(font);
		search.setTypeface(font);
		
		title.setText(R.string.mainmenu_shopndine);		
		
		LocalDatabaseHelper db = new LocalDatabaseHelper(this);
		adapter = new ShopSearchAdapter(this, db.getAllShop());
		
		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
 		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_withback_back:					
					ShopNDineSearch.this.finish();
					break;
				case R.id.header_withback_menu:
					setResult(1);
					ShopNDineSearch.this.finish();
					break;
				default:
					break;
				}
				
			}
		};
		TextWatcher textWatcher = new TextWatcher() {			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				adapter.getFilter().filter(s);				
			}			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}			
			@Override
			public void afterTextChanged(Editable s) {				
			}
		};
		
		back.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		back.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
		list.setAdapter(adapter);
		search.addTextChangedListener(textWatcher);
		
		
	};
	


}
