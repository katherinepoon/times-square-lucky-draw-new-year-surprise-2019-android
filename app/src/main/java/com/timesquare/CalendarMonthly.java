package com.timesquare;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Locale;

import com.timesquare.localdatabase.LocalDatabaseHelper;
import com.timesquare.setting.EventListAdapter;
import com.timessquare.R;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

public class CalendarMonthly extends Activity{
	
	private TextView back, menu, page_title;
	private ListView list;
	
	private Intent intent;
	private LocalDatabaseHelper db;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header_withback);
		View.inflate(this, R.layout.calendar_monthlyevent, (ViewGroup) findViewById(R.id.header_withback));
		
		// Get intent
		intent = getIntent();
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font));
		
		// Initialize the object
		TextView title = (TextView) findViewById(R.id.header_withback_title);
		back = (TextView) findViewById(R.id.header_withback_back);
		menu = (TextView) findViewById(R.id.header_withback_menu);
		page_title = (TextView) findViewById(R.id.calendar_monthlyevent_title);
		list = (ListView) findViewById(R.id.calendar_monthlyevent_eventlist);
		
		db = new LocalDatabaseHelper(this);
		
		// Set the font
		title.setTypeface(font);
		back.setTypeface(font);
		menu.setTypeface(font);
		page_title.setTypeface(font);
		
		title.setText(R.string.mainmenu_calendar);
		
		if (intent.getExtras().getInt("month")-1 < 0){
			page_title.setText(getResources().getString(R.string.Eventin) +" Times Square");
			EventListAdapter adapter = new EventListAdapter(this, db.getAllEvent());
			list.setAdapter(adapter);
		}else{
			java.util.Calendar cal = java.util.Calendar.getInstance();
			cal.set(java.util.Calendar.MONTH, intent.getExtras().getInt("month")-1);
			page_title.setText(getResources().getString(R.string.Eventin) +" "+ cal.getDisplayName(java.util.Calendar.MONTH, java.util.Calendar.LONG, Locale.getDefault()));
			DecimalFormat fomattter = new DecimalFormat("00");
			EventListAdapter adapter = new EventListAdapter(this, db.getEventByMonth(fomattter.format(cal.get(java.util.Calendar.MONTH)+1), cal.get(java.util.Calendar.YEAR)));
			list.setAdapter(adapter);
		}	
		
		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
		OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_withback_back:
					CalendarMonthly.this.finish();		
					break;
				case R.id.header_withback_menu:
					setResult(1);
					CalendarMonthly.this.finish();					
					break;
				default:
					break;
				}
				
			}
		};
		
		back.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		back.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 1){
			setResult(1);
			CalendarMonthly.this.finish();
		}
	}

}
