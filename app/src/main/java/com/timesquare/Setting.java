package com.timesquare;

import java.util.Locale;

import com.timessquare.R;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Setting extends Activity{
	
	private TextView menu, logout, english, chinese;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header);
		View.inflate(this, R.layout.setting, (ViewGroup) findViewById(R.id.header));
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
				
		// initialize the object
		LinearLayout setting = (LinearLayout) findViewById(R.id.header_setting);
		TextView title = (TextView) findViewById(R.id.header_title);
		menu = (TextView) findViewById(R.id.header_menu);
		TextView account = (TextView) findViewById(R.id.setting_account);
		logout = (TextView) findViewById(R.id.setting_logout);
		TextView langauge = (TextView) findViewById(R.id.setting_langauge);
		english = (TextView) findViewById(R.id.setting_langauge_english);
		chinese = (TextView) findViewById(R.id.setting_langauge_chinese);
		TextView remark = (TextView) findViewById(R.id.setting_remark);
		
		// set the object
		title.setTypeface(font);
		menu.setTypeface(font);
		account.setTypeface(font);
		logout.setTypeface(font);
		langauge.setTypeface(font);
		english.setTypeface(font);
		chinese.setTypeface(font);
		remark.setTypeface(font);
		
		setting.setVisibility(View.INVISIBLE);
		title.setText(R.string.setting);
		menu.setText(R.string.close);
		
		// get default language
		if (Locale.getDefault().getLanguage().equals("zh")){
			setChinese();
		}else{
			setEnglish();
		}
		
		// Initialize and set the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_menu:
					// Close the setting
					Setting.this.finish();
					break;
				case R.id.setting_logout:
					break;
				case R.id.setting_langauge_english:
					// english is set
					setEnglish();
					break;
				case R.id.setting_langauge_chinese:
					// chinese is set
					setChinese();
					break;
				default:
					break;
				}
			}
		};
		
		logout.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		
		menu.setOnClickListener(onClickListener);
		logout.setOnClickListener(onClickListener);
		english.setOnClickListener(onClickListener);
		chinese.setOnClickListener(onClickListener);
		
	}
	
	// Set the Locale to English
	private void setEnglish(){
		chinese.setTextColor(getResources().getColor(R.color.ThemeColor_DeepGreen));
		chinese.setBackgroundResource(R.drawable.twoway_right_false);
		english.setTextColor(getResources().getColor(R.color.ThemeColor_lightBlue));
		english.setBackgroundResource(R.drawable.twoway_left_true);
		Locale locale = new Locale("en");
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		SharedPreferences prefs = getSharedPreferences("setting", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(com.timesquare.setting.Setting.langauge, "en");		
		editor.commit();
		Log.i("Setting", "Locale set to English");
	}
	
	
	// Set the Locale to Chinese
	private void setChinese(){
		chinese.setTextColor(getResources().getColor(R.color.ThemeColor_lightBlue));
		chinese.setBackgroundResource(R.drawable.twoway_right_true);
		english.setTextColor(getResources().getColor(R.color.ThemeColor_DeepGreen));
		english.setBackgroundResource(R.drawable.twoway_left_false);	
		Locale locale = new Locale("zh");
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		SharedPreferences prefs = getSharedPreferences("setting", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(com.timesquare.setting.Setting.langauge, "zh");		
		editor.commit();
		Log.i("Setting", "Locale set to Chinese");
	}
	

	
}
