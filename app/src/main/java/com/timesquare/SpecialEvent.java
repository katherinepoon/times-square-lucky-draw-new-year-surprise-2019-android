package com.timesquare;


import java.io.IOException;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import com.keyes.youtube.OpenYouTubePlayerActivity;
import com.timesquare.database.Database;
import com.timesquare.setting.CameraPreview;
import com.timessquare.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SpecialEvent extends Activity {
	
	private LinearLayout setting;
	private FrameLayout qr;
	private TextView menu/*, qr_message*/;
	
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;
	ImageScanner scanner;	
	private boolean previewing = true;
	
	static {
        System.loadLibrary("iconv");
    } 
	
	// Camera Auto Focus
	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};
	
	// Camera Preview 
	PreviewCallback previewCb = new PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);
            
            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();
                
                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {
                	String code = sym.getData();
                	if (isAlphaDigit(code)){                		
                		if (isOnline()){
                			new getSpecialEventItem(SpecialEvent.this, code).execute("");
        				}else{
        					Log.i("SpecialEvent", "No Internet Connection");
        					Toast.makeText(SpecialEvent.this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
        				}                		
                	}else{
                		invalidQR();
                	}               	
                }
            }
        }
    };
	
    // Camera Focus
    AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };
    
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header);
		View.inflate(this, R.layout.new_special_event, (ViewGroup) findViewById(R.id.header));
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
		
		// Initialize the object
		setting = (LinearLayout) findViewById(R.id.header_setting);
		TextView setting_text = (TextView) findViewById(R.id.header_setting_text);
		TextView title = (TextView) findViewById(R.id.header_title);
		menu = (TextView) findViewById(R.id.header_menu);
		qr = (FrameLayout) findViewById(R.id.se_qr_container);
		TextView findQRTitle = (TextView) findViewById(R.id.se_findQRTitle);
		TextView scanning = (TextView) findViewById(R.id.se_scanning);
		
		// Set Variable
		setting_text.setTypeface(font);
		title.setTypeface(font);
		menu.setTypeface(font);
		findQRTitle.setTypeface(font);
		scanning.setTypeface(font);
		
		title.setText(R.string.specialEvent_title);
		
		// initial the camera
		mCamera = getCameraInstance();
		autoFocusHandler = new Handler();	
		
		scanner = new ImageScanner();
		scanner.setConfig(0, Config.X_DENSITY, 3);
		scanner.setConfig(0, Config.Y_DENSITY, 3);
		
		mPreview = new CameraPreview(getApplicationContext(), mCamera, previewCb, autoFocusCB);
		qr.addView(mPreview);
		
//		setContentView(R.layout.special_event);
//		
//		// Get Font Type
//		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
//				
//		
//		// Initial all the object
//		TextView setting_title = (TextView) findViewById(R.id.specialEvent_header_setting_text);
//		TextView title = (TextView) findViewById(R.id.specialEvent_header_title);
//		TextView warning = (TextView) findViewById(R.id.specialEvent_warning);
//		TextView qr_text = (TextView) findViewById(R.id.specialEvent_qr_text);		
// 		setting = (LinearLayout) findViewById(R.id.specialEvent_header_setting);
//		menu = (ImageView) findViewById(R.id.specialEvent_header_menu);
//		qr = (FrameLayout) findViewById(R.id.specialEvent_qrContainer);
//		qr_message = (TextView) findViewById(R.id.specialEvent_qr_message);
//		
//		// set the object
//		if (Locale.getDefault().getLanguage().equals("zh")){
//			menu.setBackgroundResource(R.drawable.menu_special_chin);
//		}else{
//			menu.setBackgroundResource(R.drawable.menu_special_eng);
//		}
//		setting_title.setTypeface(font);
//		title.setTypeface(font);
//		warning.setTypeface(font);
//		qr_text.setTypeface(font);
//		qr_message.setTypeface(font);		
//		
//		// initial the camera
//		mCamera = getCameraInstance();
//		autoFocusHandler = new Handler();	
//		
//		scanner = new ImageScanner();
//		scanner.setConfig(0, Config.X_DENSITY, 3);
//		scanner.setConfig(0, Config.Y_DENSITY, 3);
//		
//		mPreview = new CameraPreview(getApplicationContext(), mCamera, previewCb, autoFocusCB);
//		qr.addView(mPreview);
//		
		// Set Listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
		OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_setting:
					SpecialEvent.this.startActivity(new Intent(SpecialEvent.this, Setting.class));
					break;
				case R.id.header_menu:
					SpecialEvent.this.finish();
					break;
				default:
					break;
				}				
			}
		};
		
		setting.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		
		setting.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);		
	};
	
	@Override
	protected void onResume() {
		super.onResume();
		// if the camera is released, reset it
		if (mCamera == null){
			mCamera = getCameraInstance();
			autoFocusHandler = new Handler();		
			
			scanner = new ImageScanner();
			scanner.setConfig(0, Config.X_DENSITY, 3);
			scanner.setConfig(0, Config.Y_DENSITY, 3);
			
			mPreview = new CameraPreview(getApplicationContext(), mCamera, previewCb, autoFocusCB);
			qr.addView(mPreview);
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		// release the camera
		releaseCamera();
		qr.removeAllViews();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 1){
			SpecialEvent.this.finish();
		}
	}
	
	/**
	 * Check if there are internet connection
	 * @return boolean
	 */
	public boolean isOnline(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	
	
	// Create and Open Camera 
	public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e){
        }
        return c;
    }
	
	// Release the Camera
	private void releaseCamera(){
		if (mCamera != null){
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}
	
	// Check if the String contain digit and char only
	private boolean isAlphaDigit(String name) {
	    char[] chars = name.toCharArray();

	    for (char c : chars) {
	        if(!Character.isLetter(c) && !Character.isDigit(c)) {
	            return false;
	        }
	    }

	    return true;
	}
	
	private void invalidQR(){
//		qr_message.setText(getResources().getString(R.string.invalidQR));
//		qr_message.setText(getResources().getString(R.string.invalidQR));
		mCamera.setPreviewCallback(previewCb);
		mCamera.startPreview();
		previewing = true;
		mCamera.autoFocus(autoFocusCB);   		
		Log.i("Special Event", "QR: invalid QR");
	}
	
	private class getSpecialEventItem extends AsyncTask<String, Void, String>{
		
		private String URL = Database.DATABASE_URL + Database.API_SPECIAL_EVENT_ITEM;
		
		private ProgressDialog loading;
		
		public getSpecialEventItem(Context context, String code){
			URL += "/" + code;
			loading = new ProgressDialog(context);
		}
		
		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";
			
			try {
				// Send request and get data
				HttpResponse httpResponse = httpClient.execute(httpGet);
				result = EntityUtils.toString(httpResponse.getEntity());		
				Log.i("Special Event", "Server Response Get");
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);			
			try{
				// Check if the information get correctly
				if (!result.equals("error")){
					JSONObject jsonObject = new JSONObject(result);
					// Check if there are error in the response
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						if (jsonObject.getInt(Database.TAG_SPECIAL_EVENT_ITEM_TYPE) == 0){
							// get the special event and move to main menu
							Intent intent = new Intent(SpecialEvent.this, SpecialEventItem.class);
							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_ID, jsonObject.getInt(Database.TAG_SPECIAL_EVENT_ITEM_ID));
							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_NAME, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_NAME));
							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_IMAGE, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_IMAGE));
							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_SOUND, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_SOUND));
							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_SOUND_CHINESE, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_SOUND_CHINESE));
							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_TEXT, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_TEXT));
							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_TEXT_CHINESE, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_TEXT_CHINESE));
							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_CODE, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_CODE));
							SpecialEvent.this.startActivityForResult(intent, 1);
							Log.i("Special Event", "Data get and move to item page");
							
						}else if (jsonObject.getInt(Database.TAG_SPECIAL_EVENT_ITEM_TYPE) == 1){
//							Intent intent = new Intent(SpecialEvent.this, SpecialEventVideoItem.class);
//							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_ID, jsonObject.getInt(Database.TAG_SPECIAL_EVENT_ITEM_ID));
//							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_NAME, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_NAME));
//							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_VIDEO_URL, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_VIDEO_URL));
//							intent.putExtra(Database.TAG_SPECIAL_EVENT_ITEM_CODE, jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_CODE));
//							SpecialEvent.this.startActivityForResult(intent, 1);
//							Log.i("Special Event", "Data get and move to item page");			
							
							Intent intent = new Intent(null, Uri.parse("ytv://" + jsonObject.getString(Database.TAG_SPECIAL_EVENT_ITEM_VIDEO_URL)), SpecialEvent.this, OpenYouTubePlayerActivity.class);
							intent.putExtra(Database.TAG_SPECIAL_EVENT_VIDEO_START_TIME, jsonObject.getInt(Database.TAG_SPECIAL_EVENT_VIDEO_START_TIME));
							intent.putExtra(Database.TAG_SPECIAL_EVENT_VIDEO_END_TIME, jsonObject.getInt(Database.TAG_SPECIAL_EVENT_VIDEO_END_TIME));
							SpecialEvent.this.startActivityForResult(intent, 1);
						}
						
						
					}else{
						invalidQR();
					}
				}else{
					Log.i("Special Event", "Get Special Event Item: Get Response Error");
				}
			}catch (JSONException e) {
				e.printStackTrace();
			}		
			if (loading.isShowing()){
				loading.dismiss();
			}
		}
		
	}


}
