package com.timesquare;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.senab.photoview.PhotoViewAttacher;

import com.sllin.imageloader.ImageLoader;
import com.timesquare.database.Database;
import com.timesquare.localdatabase.LocalDatabaseHelper;
import com.timesquare.setting.Setting;
import com.timessquare.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class GoodiesItem extends Activity{
	
	private TextView back, menu, code;
	private ImageView image;
//	private ViewPager pager;
//	private LinearLayout pointer;
	
	private ImageLoader imageLoader;
	PhotoViewAttacher attacher;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header_withback);
		View.inflate(this, R.layout.image_viewer, (ViewGroup) findViewById(R.id.header_withback));
				
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
		
		// Initialize the object
		TextView title = (TextView) findViewById(R.id.header_withback_title);
		back = (TextView) findViewById(R.id.header_withback_back);
		menu = (TextView) findViewById(R.id.header_withback_menu);
		code = (TextView) findViewById(R.id.imageViewer_warning);
		TextView hint = (TextView) findViewById(R.id.imageViewer_zoomHint);
		image = (ImageView) findViewById(R.id.imageViewer_image);
//		pager = (ViewPager) findViewById(R.id.goodies_item_viewPager);
//		pointer = (LinearLayout) findViewById(R.id.goodies_item_pointer);
		
		// Set Variable
		title.setTypeface(font);
		back.setTypeface(font);
		menu.setTypeface(font);
		hint.setTypeface(font);
		
		code.setGravity(Gravity.RIGHT);
		code.setTextSize(14);
 		title.setText(R.string.mainmenu_goodies);
 		
 		imageLoader = new ImageLoader(this);
		attacher = new PhotoViewAttacher(image);
 		
 		Intent intent = getIntent();
 		
 		if (isOnline()){
 			new GetGoodies(this, intent.getExtras().getString(Database.TAG_GOODIES_CODE)).execute("");
		}else{
			Log.i("GoodiesItem", "No Internet Connection");
			Toast.makeText(GoodiesItem.this, getResources().getString(R.string.ErrorNoInternetConnection), Toast.LENGTH_SHORT).show();
		}
 		
 		
 		
 		
 		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
 		View.OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_withback_back:
					GoodiesItem.this.finish();
					break;
				case R.id.header_withback_menu:
					setResult(1);
					GoodiesItem.this.finish();
					break;
				default:
					break;
				}
				
			}
		};
		
//		ViewPager.OnPageChangeListener onPageChangeListener = new OnPageChangeListener() {			
//			@Override
//			public void onPageSelected(int position) {
//				for (int i=0 ; i<pointer.getChildCount(); i++){
//					pointer.getChildAt(i).setAlpha(0.3f);
//				}
//				pointer.getChildAt(position).setAlpha(1f);				
//			}
//			
//			@Override
//			public void onPageScrolled(int arg0, float arg1, int arg2) {
//				// TODO Auto-generated method stub				
//			}
//			
//			@Override
//			public void onPageScrollStateChanged(int arg0) {
//				// TODO Auto-generated method stub				
//			}
//		};
		
		
		
		back.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		back.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
//		pager.setOnPageChangeListener(onPageChangeListener);
	}
	
	/**
	 * Check if there are internet connection
	 * @return boolean
	 */
	public boolean isOnline(){
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	
	private class GetGoodies extends AsyncTask<String, Void, String>{
		
		private Context context;
		
		private String URL = Database.DATABASE_URL + Database.API_GET_GOODIES_BY_CODE;
		
		private ProgressDialog loading;
		private LocalDatabaseHelper db;
		
		public GetGoodies(Context context, String code) {
			loading = new ProgressDialog(context);
			URL += "/" + code;
			this.context = context;
			db = new LocalDatabaseHelper(context);
		}
		
		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			HttpGet httpGet = new HttpGet(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";
			
			try {
				// Send request and get data
				HttpResponse httpResponse = httpClient.execute(httpGet);
				result = EntityUtils.toString(httpResponse.getEntity());				
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {			
			if (loading.isShowing()){
				loading.dismiss();
			}
			try{
				// Check if the information get correctly
				if (!result.equals("error")){
					JSONObject jsonObject = new JSONObject(result);
					// Check if there are error in the response
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						// Get and write the goodies information		
						List<HashMap<String, String>> listData = new ArrayList<HashMap<String,String>>();
						HashMap<String, String> data = new HashMap<String, String>();
						data.put(Database.TAG_GOODIES_DESCRIPTION, jsonObject.getString(Database.TAG_GOODIES_DESCRIPTION));
						data.put(Database.TAG_GOODIES_IMAGE, jsonObject.getString(Database.TAG_GOODIES_IMAGE));
						listData.add(data);
						if (db.checkIfExistGoodies(jsonObject.getString(Database.TAG_GOODIES_ID))){
							String customer_code = db.getCodeByID(jsonObject.getString(Database.TAG_GOODIES_ID));
							if (customer_code != null){
								code.setText(customer_code);
							}							
						}else{
							new RegCustomerGoodiesCode(context).execute(jsonObject.getString(Database.TAG_GOODIES_ID));
						}
						
						imageLoader.DisplayImage(Database.PATH_IMAGE +"/"+ listData.get(0).get(Database.TAG_GOODIES_IMAGE), image, true);
						SharedPreferences prefs = context.getSharedPreferences("setting", Context.MODE_PRIVATE);		
						if (prefs.getString(Setting.langauge, "en") == "en"){
							image.setContentDescription(listData.get(0).get(Database.TAG_GOODIES_DESCRIPTION));			
						}else{
							image.setContentDescription(listData.get(0).get(Database.TAG_GOODIES_DESCRIPTION_CHI));								
						}
						
//						ViewPagerAdapter adapter = new ViewPagerAdapter(GoodiesItem.this, listData, 0);
//						pager.setAdapter(adapter);
//						
//						for (int i=0; i< pager.getAdapter().getCount() ; i++){
//				 			ImageView point = new ImageView(context);
//				 			point.setImageResource(R.drawable.pointer);
//				 			point.setAdjustViewBounds(true);
//				 			pointer.addView(point, i);
//				 		}
//						for (int i=0 ; i<pointer.getChildCount(); i++){
//							pointer.getChildAt(i).setAlpha(0.3f);
//						}
//						pointer.getChildAt(0).setAlpha(1f);	
											
					}else{
						Log.i("Goodies", "Get Goodies: "+jsonObject.getString(Database.TAG_MESSAGE));
						((GoodiesItem)context).finish();
					}
				}else{
					Log.i("Goodies", "Get Goodies: Get Response Error");
				}
			}catch (JSONException e) {
				e.printStackTrace();
			}
			super.onPostExecute(result);
		}		
	}
	
	private class RegCustomerGoodiesCode extends AsyncTask<String, Void, String>{
		
		
		private String URL = Database.DATABASE_URL + Database.API_REG_CUSTOMER_GOODIES_CODE ; 
		
		private ProgressDialog loading;
		private LocalDatabaseHelper db;
		
		private String goodies_id;
		
		public RegCustomerGoodiesCode(Context context) {
			loading = new ProgressDialog(context);
			db = new LocalDatabaseHelper(context);
		}
		
		@Override
		protected void onPreExecute() {
			// show the loading dialog
			loading.setMessage(getResources().getString(R.string.loading));
			loading.setCancelable(false);
			loading.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... values) {
			HttpPost httpPost = new HttpPost(URL);
			HttpClient httpClient = new DefaultHttpClient();
			String result = "error";
			List<NameValuePair> params = new ArrayList<NameValuePair>();			
			// set params
			if(values.length >= 1 ){
				params.add(new BasicNameValuePair(Database.TAG_GOODIES_ID, values[0]));
				params.add(new BasicNameValuePair(Database.TAG_DEVICE_TYPE, "A"));
				goodies_id = values[0];
			}
			try {
				// send request
				httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				HttpResponse httpResponse = httpClient.execute(httpPost);
				result = EntityUtils.toString(httpResponse.getEntity());				
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (loading.isShowing()){
				loading.dismiss();
			}
			Log.i("jhy", result);
			try {
				if (!result.equals("error")){
					JSONObject jsonObject = new JSONObject(result);
					if (!jsonObject.getBoolean(Database.TAG_ERROR)){
						ContentValues value = new ContentValues();
						value.put(LocalDatabaseHelper.GOODIES_ID, goodies_id);
						value.put(LocalDatabaseHelper.GOODIES_COSTOMER_CODE, jsonObject.getString(Database.TAG_GOODIES_CUSTOMER_CODE));
						db.addGoodies(value);
						code.setText(jsonObject.getString(Database.TAG_GOODIES_CUSTOMER_CODE));
					}
				}				
			} catch (JSONException e) {
				e.printStackTrace();
			}			
			super.onPostExecute(result);
		}

		
	}
	
}
