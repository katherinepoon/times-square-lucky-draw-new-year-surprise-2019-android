package com.timesquare.localdatabase;

import java.util.ArrayList;
import java.util.List;

public class ShopCategory {
	
	private String shop_category_id;
	private String shop_category_name;
	private List<Shop> shops = new ArrayList<Shop>();
	
	// Initial
	public ShopCategory(String shop_category_id, String shop_category_name, List<Shop> shops) {
		this.shop_category_id = shop_category_id;
		this.shop_category_name = shop_category_name;
		this.shops = shops;
	}
	
	// Set
	public void setShopCategoryID(String shop_category_id){
		this.shop_category_id = shop_category_id;
	}
	
	public void setShopCategoryName(String shop_category_name){
		this.shop_category_name = shop_category_name;
	}
	
	public void setShopList(List<Shop> shops){
		this.shops = shops;
	}
	
	// Get
	public String getShopCategoryID(){
		return this.shop_category_id;
	}

	public String getShopCategoryName(){
		return this.shop_category_name;
	}
	
	public List<Shop> getShopList(){
		return this.shops;
	}

}
