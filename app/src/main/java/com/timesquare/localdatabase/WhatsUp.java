package com.timesquare.localdatabase;

public class WhatsUp {
	
	private String whatsup_title;
	private String whatsup_image;
	private String whatsup_thumb;

	public WhatsUp(String whatsup_title, String whatsup_image, String whatsup_thumb){
		this.whatsup_title = whatsup_title;
		this.whatsup_image = whatsup_image;
		this.whatsup_thumb = whatsup_thumb;		
	}
	
	public void setWhatsUpTitle(String whatsup_title){
		this.whatsup_title = whatsup_title;
	}
	
	public void setWhatsUpImage(String whatsup_image){
		this.whatsup_image = whatsup_image;
	}
	
	public void setWhatsUpThumb(String whatsup_thumb){
		this.whatsup_thumb = whatsup_thumb;
	}
	
	public String getWhatsUpTitle(){
		return this.whatsup_title;
	}
	
	public String getWhatsUpImage(){
		return this.whatsup_image;
	}
	
	public String getWhatsUpThumb(){
		return this.whatsup_thumb;
	}
}
