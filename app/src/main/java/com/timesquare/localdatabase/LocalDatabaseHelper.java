package com.timesquare.localdatabase;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils.InsertHelper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class LocalDatabaseHelper extends SQLiteOpenHelper{
	
	//-------------- Database Setting ---------------//
	private static final int DATABASE_VERSION = 2;
	private static final String DATABASE_NAME = "timesquare";
	
	//-------------- Table Setting ---------------//
	private static final String DATABASE_TABLE_EVENT = "event";
	private static final String DATABASE_TABLE_SHOP = "shop";
	private static final String DATABASE_TABLE_GOODIES = "goodies";
	
	// event Table attribute
	public static final String EVENT_ID = "event_id";
	public static final String EVENT_DATE_TIME = "event_date_time";
	public static final String EVENT_TITLE = "event_title";
	public static final String EVENT_DETAIL = "event_detail";
	
	// shop Table attribute
	public static final String SHOP_ID = "shop_id";
	public static final String SHOP_NUMBER = "shop_number";
	public static final String SHOP_NAME = "shop_name";
	public static final String SHOP_FLOOR_ID = "shop_floor_id";
	public static final String SHOP_CATEGORY_ID = "shop_category_id";
	
	// goodies Table attribute
	public static final String GOODIES_ID = "goodies_id";
	public static final String GOODIES_COSTOMER_CODE = "goodies_costomer_code";
	
	//-------------- Create Table SQL statement ------------------//
	private static final String CREATE_TABLE_EVENT = "CREATE TABLE "+DATABASE_TABLE_EVENT+"("+EVENT_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+EVENT_DATE_TIME+" TIMESTAMP, "+EVENT_TITLE+" TEXT, "+EVENT_DETAIL+" TEXT)";
	private static final String CREATE_TABLE_SHOP = "CREATE TABLE "+DATABASE_TABLE_SHOP+"("+SHOP_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+SHOP_NUMBER+" TEXT, "+SHOP_NAME+" TEXT, "+SHOP_FLOOR_ID+" TEXT, "+
			SHOP_CATEGORY_ID+" TEXT)";
	private static final String CREATE_TABLE_GOODIES = "CREATE TABLE "+DATABASE_TABLE_GOODIES+"("+GOODIES_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+GOODIES_COSTOMER_CODE+" TEXT)"; 
	
	public LocalDatabaseHelper(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_EVENT);	
		db.execSQL(CREATE_TABLE_SHOP);
		db.execSQL(CREATE_TABLE_GOODIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE_EVENT);
		db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE_SHOP);
		db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE_GOODIES);
		onCreate(db);		
	}
	
	//---------------------------- EVENT TABLE METHOD --------------------------//
	
	public void addEvent(ContentValues values){
		SQLiteDatabase db = this.getWritableDatabase();
		db.insert(DATABASE_TABLE_EVENT, null, values);
		db.close();
		Log.i("Local Database", "Add Event: data added");
	}	
	
	public List<Event> getAllEvent(){
		SQLiteDatabase db = this.getReadableDatabase();
		
		List<Event> events = new ArrayList<Event>();
		String selectQuery = "SELECT * FROM "+DATABASE_TABLE_EVENT + " GROUP BY " + EVENT_TITLE;
		Cursor c = db.rawQuery(selectQuery, new String[]{});
		if (c.moveToFirst()){
			do{
				Event event = new Event(c.getInt(c.getColumnIndex(EVENT_ID)), c.getString(c.getColumnIndex(EVENT_DATE_TIME)), c.getString(c.getColumnIndex(EVENT_TITLE)), c.getString(c.getColumnIndex(EVENT_DETAIL)));
				events.add(event);
			}while (c.moveToNext());
		}
		
		Log.i("Local Database", "Get Event: Data get " + c.getCount());
		db.close();
		return events;
	}
	
	public List<Event> getEventByDate(String date){
		SQLiteDatabase db = this.getReadableDatabase();
		
		List<Event> events = new ArrayList<Event>();
		String selectQuery = "SELECT * FROM "+DATABASE_TABLE_EVENT + " WHERE "+EVENT_DATE_TIME + ">  ? AND "+EVENT_DATE_TIME+" < ?";
		Cursor c = db.rawQuery(selectQuery, new String[]{date+ " 00:00:00", date+" 23:59:59"});
		if (c.moveToFirst()){
			do{
				Event event = new Event(c.getInt(c.getColumnIndex(EVENT_ID)), c.getString(c.getColumnIndex(EVENT_DATE_TIME)), c.getString(c.getColumnIndex(EVENT_TITLE)), c.getString(c.getColumnIndex(EVENT_DETAIL)));
				events.add(event);
			}while (c.moveToNext());
		}
		
		Log.i("Local Database", "Get Event: Data get " + c.getCount());
		db.close();
		return events;
	}
	
	public List<Event> getEventByMonth(String month, int year){
		SQLiteDatabase db = this.getReadableDatabase();
		
		List<Event> events = new ArrayList<Event>();
		String selectQuery = "SELECT * FROM "+DATABASE_TABLE_EVENT + " WHERE "+EVENT_DATE_TIME + ">  ? AND "+EVENT_DATE_TIME+" < ?";
		Cursor c = db.rawQuery(selectQuery, new String[]{year+"-"+month+"-01"+" 00:00:00", year+"-"+month+"-31"+" 23:59:59"});
		if (c.moveToFirst()){
			do{
				Event event = new Event(c.getInt(c.getColumnIndex(EVENT_ID)), c.getString(c.getColumnIndex(EVENT_DATE_TIME)), c.getString(c.getColumnIndex(EVENT_TITLE)), c.getString(c.getColumnIndex(EVENT_DETAIL)));
				events.add(event);
			}while (c.moveToNext());
		}
		
		Log.i("Local Database", "Get Event: Data get " + c.getCount());
		db.close();
		return events;
	}
	
	public void deleteAllEvent(){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DATABASE_TABLE_EVENT, null, null);
		db.close();
		Log.i("Local Database", "All data deleted");
	}

	//--------------------------SHOP TABLE METHOD ------------------------------//
	public void addShop(ContentValues values){
		SQLiteDatabase db = this.getWritableDatabase();
		db.insert(DATABASE_TABLE_SHOP, null, values);
		db.close();
		Log.i("Local Database", "Add Shop: data added");
	}
	
	public void addShop(List<ContentValues> values){
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		for (int i=0; i<values.size(); i++){
			db.insert(DATABASE_TABLE_SHOP, null, values.get(i));
		}		
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();
		Log.i("Local Database", "Add Shop: data added");
	}
	
	public List<Shop> getAllShop(){
		SQLiteDatabase db = this.getReadableDatabase();
		
		List<Shop> shops = new ArrayList<Shop>();
		String selectQuery = "SELECT * FROM "+DATABASE_TABLE_SHOP;
		Cursor c = db.rawQuery(selectQuery, null);
		if (c.moveToFirst()){
			do{
				Shop s = new Shop(c.getString(c.getColumnIndex(SHOP_ID)), c.getString(c.getColumnIndex(SHOP_NUMBER)), c.getString(c.getColumnIndex(SHOP_NAME)), 
						c.getString(c.getColumnIndex(SHOP_FLOOR_ID)), c.getString(c.getColumnIndex(SHOP_CATEGORY_ID)));
				shops.add(s);
			}while (c.moveToNext());
		}
		
		Log.i("Local Database", "Get Shop: Data get " + c.getCount());
		db.close();
		return shops;
	}
	
	public List<Shop> getShopByCateID(String shop_category_id){
		SQLiteDatabase db = this.getReadableDatabase();
		
		List<Shop> shops = new ArrayList<Shop>();
		String selectQuery = "SELECT * FROM "+DATABASE_TABLE_SHOP + " WHERE "+SHOP_CATEGORY_ID +"= ?";
		Cursor c = db.rawQuery(selectQuery, new String[]{shop_category_id});
		if (c.moveToFirst()){
			do{
				Shop s = new Shop(c.getString(c.getColumnIndex(SHOP_ID)), c.getString(c.getColumnIndex(SHOP_NUMBER)), c.getString(c.getColumnIndex(SHOP_NAME)), 
						c.getString(c.getColumnIndex(SHOP_FLOOR_ID)), c.getString(c.getColumnIndex(SHOP_CATEGORY_ID)));
				shops.add(s);
			}while (c.moveToNext());
		}
		
		Log.i("Local Database", "Get Shop: Data get " + c.getCount());
		db.close();
		return shops;
	}
	
	public List<Shop> getShopByFloorID(String shop_floor_id){
		SQLiteDatabase db = this.getReadableDatabase();
		
		List<Shop> shops = new ArrayList<Shop>();
		String selectQuery = "SELECT * FROM "+DATABASE_TABLE_SHOP + " WHERE "+SHOP_FLOOR_ID +"= ?";
		Cursor c = db.rawQuery(selectQuery, new String[]{shop_floor_id});
		if (c.moveToFirst()){
			do{
				Shop s = new Shop(c.getString(c.getColumnIndex(SHOP_ID)), c.getString(c.getColumnIndex(SHOP_NUMBER)), c.getString(c.getColumnIndex(SHOP_NAME)), 
						c.getString(c.getColumnIndex(SHOP_FLOOR_ID)), c.getString(c.getColumnIndex(SHOP_CATEGORY_ID)));
				shops.add(s);
			}while (c.moveToNext());
		}
		
		Log.i("Local Database", "Get Shop: Data get " + c.getCount());
		db.close();
		return shops;
	}
	
	public void deleteAllShop(){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DATABASE_TABLE_SHOP, null, null);
		db.close();
		Log.i("Local Database", "All data deleted");
	}
	
	//-------------------------- GOODIES METHOD ------------------------------//
	
	public boolean checkIfExistGoodies(String goodies_id){
		SQLiteDatabase db = this.getReadableDatabase();
		
		String selectQuery = "SELECT * FROM "+DATABASE_TABLE_GOODIES+" WHERE "+GOODIES_ID+"= ? ";
		Cursor c = db.rawQuery(selectQuery, new String[]{goodies_id});
		boolean exists = c.getCount() > 0;
		Log.i("Local Database", "check goodies exist");
		db.close();
		return exists;	
	}
	
	public String getCodeByID(String goodies_id){
		SQLiteDatabase db = this.getReadableDatabase();
		
		String selectQuery = "SELECT * FROM "+DATABASE_TABLE_GOODIES+" WHERE "+GOODIES_ID+"= ? ";
		Cursor c = db.rawQuery(selectQuery, new String[]{goodies_id});
		String code = null;
		if (c.moveToFirst()){
			code = c.getString(c.getColumnIndex(GOODIES_COSTOMER_CODE));
		}			
		Log.i("Local Database", "Goodies Customer Code Get");
		db.close();
		return code;
	}
	

	public void addGoodies(ContentValues values){
		SQLiteDatabase db = this.getWritableDatabase();
		db.insert(DATABASE_TABLE_GOODIES, null, values);
		db.close();
		Log.i("Local Database", "Add Goodies: data added");
	}
}
