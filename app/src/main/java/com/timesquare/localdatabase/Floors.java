package com.timesquare.localdatabase;

import java.util.ArrayList;
import java.util.List;

public class Floors {
	
	private String floor_id;
	private String floor_name;
	private List<Shop> shops = new ArrayList<Shop>();
	
	// Initial 
	public Floors(String floor_id, String floor_name, List<Shop> shops) {
		this.floor_id = floor_id;
		this.floor_name = floor_name;
		this.shops = shops;
	}
	
	// Set
	public void setFloorID(String floor_id){
		this.floor_id = floor_id;
	}
	
	public void setFloorName(String floor_name){
		this.floor_name = floor_name;
	}
	
	public void setShopList(List<Shop> shops){
		this.shops = shops;
	}
	
	// Get
	public String getFloorID(){
		return this.floor_id;
	}

	public String getFloorName(){
		return this.floor_name;
	}
	
	public List<Shop> getShopList(){
		return this.shops;
	}

}
