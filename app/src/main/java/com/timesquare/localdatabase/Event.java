package com.timesquare.localdatabase;

public class Event {
	
	private int event_id;
	private String event_date_time;
	private String event_title;
	private String event_detail;
	
	public Event(int event_id, String event_date_time, String event_title, String event_detail) {
		this.event_id = event_id;
		this.event_date_time = event_date_time;
		this.event_title = event_title;
		this.event_detail = event_detail;
	}
	
	// set
	public void setEventId(int event_id){
		this.event_id = event_id;
	}
	
	public void setEventDateTime(String event_date_time){
		this.event_date_time = event_date_time;
	}
	
	public void setEventTitle(String event_title){
		this.event_title = event_title;
	}
	
	public void setEventDetail(String event_detail){
		this.event_detail = event_detail;
	}

	// get
	public int getEventID(){
		return this.event_id;
	}
	
	public String getEventDateTime(){
		return this.event_date_time;
	}
	
	public String getEventTitle(){
		return this.event_title;
	}
	
	public String getEventDetail(){
		return this.event_detail;
	}
}
