package com.timesquare.localdatabase;

public class Shop {

	private String shop_id;
	private String shop_number;
	private String shop_name;
	private String shop_floor_id;
	private String shop_category_id;
	
	// initial
	public Shop(String shop_id, String shop_number, String shop_name, String shop_floor_id, String shop_category_id) {
		this.shop_id = shop_id;
		this.shop_number = shop_number;
		this.shop_name = shop_name;
		this.shop_floor_id = shop_floor_id;
		this.shop_category_id = shop_category_id;
	}
	
	// set
	public void setShopID(String shop_id){
		this.shop_id = shop_id;
	}
	
	public void setShopNumber(String shop_number){
		this.shop_number = shop_number;
	}
	
	public void setShopName(String shop_name){
		this.shop_name = shop_name;
	}
	
	public void setShopFloorID(String shop_floor_id){
		this.shop_floor_id = shop_floor_id;
	}
	
	public void setShopCategoryID(String shop_category_id){
		this.shop_category_id = shop_category_id;
	}
	
	// get
	public String getShopID(){
		return this.shop_id;
	}
	
	public String getShopNumber(){
		return this.shop_number;
	}
	
	public String getShopName(){
		return this.shop_name;
	}
	
	public String getShopFloorID(){
		return this.shop_floor_id;
	}
	
	public String getShopCategoryID(){
		return this.shop_category_id;
	}
	
}
