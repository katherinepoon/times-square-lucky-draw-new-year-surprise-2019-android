package com.timesquare;

import java.util.GregorianCalendar;
import java.util.Locale;

import com.timesquare.database.Database;
import com.timessquare.R;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

public class CalendarDetail extends Activity{
	
	private TextView back, menu, event_title, event_time, event_detail, addToCalendar;
	private Intent intent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header_withback);
		View.inflate(this, R.layout.calendar_eventdetail, (ViewGroup) findViewById(R.id.header_withback));
		
		// Get intent
		intent = getIntent();
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font));
		
		// Initialize the object
		TextView title = (TextView) findViewById(R.id.header_withback_title);
		back = (TextView) findViewById(R.id.header_withback_back);
		menu = (TextView) findViewById(R.id.header_withback_menu);
		event_title = (TextView) findViewById(R.id.calendar_eventDetail_title);
		event_time = (TextView) findViewById(R.id.calendar_eventDetail_time);
		event_detail = (TextView) findViewById(R.id.calendar_eventDetail_detail);
		addToCalendar = (TextView) findViewById(R.id.calendar_eventDetail_addToCalendar);
		
		// Set the font
		title.setTypeface(font);
		back.setTypeface(font);
		menu.setTypeface(font);
		event_title.setTypeface(font);
		event_time.setTypeface(font);
		event_detail.setTypeface(font);
		addToCalendar.setTypeface(font);
		
		title.setText(R.string.mainmenu_calendar);
		event_title.setText(intent.getExtras().getString(Database.TAG_EVENT_TITLE));
		event_time.setText(intent.getExtras().getString(Database.TAG_EVENT_DATE_TIME));
		event_detail.setText(intent.getExtras().getString(Database.TAG_EVENT_DETAIL));
		
		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
		OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_withback_back:
					CalendarDetail.this.finish();
					break;
				case R.id.header_withback_menu:
					setResult(1);
					CalendarDetail.this.finish();					
					break;
				case R.id.calendar_eventDetail_addToCalendar:
					Intent addCalIntent = new Intent(Intent.ACTION_INSERT);
					addCalIntent.setType("vnd.android.cursor.item/event"); 
					addCalIntent.setData(CalendarContract.Events.CONTENT_URI);
					addCalIntent.putExtra(Events.TITLE, event_time.getText().toString());
					addCalIntent.putExtra(Events.DESCRIPTION, event_detail.getText().toString());
					GregorianCalendar calDate = new GregorianCalendar(Integer.parseInt(intent.getExtras().getString(Database.TAG_EVENT_DATE_TIME).substring(0, 4)), 
							Integer.parseInt(intent.getExtras().getString(Database.TAG_EVENT_DATE_TIME).substring(5, 7)), 
							Integer.parseInt(intent.getExtras().getString(Database.TAG_EVENT_DATE_TIME).substring(8, 10)));
					addCalIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true); 
					addCalIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calDate.getTimeInMillis()); 
					addCalIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calDate.getTimeInMillis()); 
					CalendarDetail.this.startActivity(addCalIntent);
					
					break;
				default:
					break;
				}
				
			}
		};
		
		back.setOnTouchListener(onTouchListener);
		menu.setOnTouchListener(onTouchListener);
		addToCalendar.setOnTouchListener(onTouchListener);
		back.setOnClickListener(onClickListener);
		menu.setOnClickListener(onClickListener);
		addToCalendar.setOnClickListener(onClickListener);
		
		
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1){
			CalendarDetail.this.finish();
		}
	}
	
}
