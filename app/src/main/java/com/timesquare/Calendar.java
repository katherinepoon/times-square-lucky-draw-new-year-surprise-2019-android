package com.timesquare;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.szugyi.circlemenu.view.CircleImageView;
import com.szugyi.circlemenu.view.CircleLayout;
import com.szugyi.circlemenu.view.CircleLayout.OnCenterClickListener;
import com.szugyi.circlemenu.view.CircleLayout.OnItemClickListener;
import com.szugyi.circlemenu.view.CircleLayout.OnItemSelectedListener;
import com.szugyi.circlemenu.view.CircleLayout.OnRotationFinishedListener;
import com.timesquare.database.EventAsync;
import com.timesquare.localdatabase.LocalDatabaseHelper;
import com.timesquare.setting.EventListAdapter;
import com.timessquare.R;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Calendar extends Activity{
	
	private LinearLayout setting;
	private TextView header_menu, datePicker_year, date, month, monthlyEvent, year, chineseDate, addEvent, noEvent, tsEvent;
	private ListView eventList;
	private CircleLayout datePicker_date, datePicker_month;
	
	private EventListAdapter adapter;
	private SimpleDateFormat format;	
	private java.util.Calendar cal;
	
	private LocalDatabaseHelper db;
	
	// List of date image (unselected)
	private int[] date_img = {R.drawable.date01, R.drawable.date02, R.drawable.date03, R.drawable.date04, R.drawable.date05, R.drawable.date06, 
			R.drawable.date07, R.drawable.date08, R.drawable.date09, R.drawable.date10, R.drawable.date11, R.drawable.date12, R.drawable.date13, 
			R.drawable.date14, R.drawable.date15, R.drawable.date16, R.drawable.date17, R.drawable.date18, R.drawable.date19, R.drawable.date20, 
			R.drawable.date21, R.drawable.date22, R.drawable.date23, R.drawable.date24, R.drawable.date25, R.drawable.date26, R.drawable.date27,
			R.drawable.date28, R.drawable.date29, R.drawable.date30, R.drawable.date31, };
	// List of date image (selected)
	private int[] date_selected_img = {R.drawable.date01_selected, R.drawable.date02_selected, R.drawable.date03_selected, R.drawable.date04_selected, R.drawable.date05_selected
			, R.drawable.date06_selected, R.drawable.date07_selected, R.drawable.date08_selected, R.drawable.date09_selected, R.drawable.date10_selected,
			R.drawable.date11_selected, R.drawable.date12_selected, R.drawable.date13_selected, R.drawable.date14_selected, R.drawable.date15_selected, 
			R.drawable.date16_selected, R.drawable.date17_selected, R.drawable.date18_selected, R.drawable.date19_selected, R.drawable.date20_selected, 
			R.drawable.date21_selected, R.drawable.date22_selected, R.drawable.date23_selected, R.drawable.date24_selected, R.drawable.date25_selected, 
			R.drawable.date26_selected, R.drawable.date27_selected, R.drawable.date28_selected, R.drawable.date29_selected, R.drawable.date30_selected, 
			R.drawable.date31_selected, };
	// List of date string
	private int[] date_string = {R.string.date01, R.string.date02, R.string.date03, R.string.date04, R.string.date05, R.string.date06, R.string.date07, 
			R.string.date08, R.string.date09, R.string.date10, R.string.date11, R.string.date12, R.string.date13, R.string.date14, R.string.date15, 
			R.string.date16, R.string.date17, R.string.date18, R.string.date19, R.string.date20, R.string.date21, R.string.date22, R.string.date23, 
			R.string.date24, R.string.date25, R.string.date26, R.string.date27, R.string.date28, R.string.date29, R.string.date30, R.string.date31 };
	private int[] date_id = {R.id.calendar_datepicker_date_01, R.id.calendar_datepicker_date_02, R.id.calendar_datepicker_date_03, R.id.calendar_datepicker_date_04,
			R.id.calendar_datepicker_date_05, R.id.calendar_datepicker_date_06, R.id.calendar_datepicker_date_07, R.id.calendar_datepicker_date_08, R.id.calendar_datepicker_date_09, 
			R.id.calendar_datepicker_date_10, R.id.calendar_datepicker_date_11, R.id.calendar_datepicker_date_12, R.id.calendar_datepicker_date_13, R.id.calendar_datepicker_date_14, 
			R.id.calendar_datepicker_date_15, R.id.calendar_datepicker_date_16, R.id.calendar_datepicker_date_17, R.id.calendar_datepicker_date_18, R.id.calendar_datepicker_date_19, 
			R.id.calendar_datepicker_date_20, R.id.calendar_datepicker_date_21, R.id.calendar_datepicker_date_22, R.id.calendar_datepicker_date_23, R.id.calendar_datepicker_date_24, 
			R.id.calendar_datepicker_date_25, R.id.calendar_datepicker_date_26, R.id.calendar_datepicker_date_27, R.id.calendar_datepicker_date_28 };
	// List of month image
	private int[] month_img = {R.drawable.month01, R.drawable.month02, R.drawable.month03, R.drawable.month04, R.drawable.month05, R.drawable.month06, 
			R.drawable.month07, R.drawable.month08, R.drawable.month09, R.drawable.month10, R.drawable.month11, R.drawable.month12};
	// List of month string
	private int[] month_string = {R.string.month01, R.string.month02, R.string.month03, R.string.month04, R.string.month05, R.string.month06, 
			R.string.month07, R.string.month08, R.string.month09, R.string.month10, R.string.month11, R.string.month12};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.header);
		View.inflate(this, R.layout.calendar, (ViewGroup) findViewById(R.id.header));
		
		// Get Font Type
		Typeface font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font)); 
		
		// Initialize the object
		setting = (LinearLayout) findViewById(R.id.header_setting);
		TextView header_title = (TextView) findViewById(R.id.header_title);
		TextView header_settingText = (TextView) findViewById(R.id.header_setting_text);
		header_menu = (TextView) findViewById(R.id.header_menu);
		date = (TextView) findViewById(R.id.calendar_date);
		month = (TextView) findViewById(R.id.calendar_month);
		year = (TextView) findViewById(R.id.calendar_year);
		chineseDate = (TextView) findViewById(R.id.calendar_chineseDate);
		monthlyEvent = (TextView) findViewById(R.id.calendar_monthlyEvent);
		addEvent = (TextView) findViewById(R.id.calendar_addEvent);
		tsEvent = (TextView) findViewById(R.id.calendar_TSEvent);
		noEvent = (TextView) findViewById(R.id.calendar_noEvent);
		datePicker_date = (CircleLayout) findViewById(R.id.calendar_datepicker_date);
		datePicker_month = (CircleLayout) findViewById(R.id.calendar_datepicker_month);
		datePicker_year = (TextView) findViewById(R.id.calendar_datepicker_year);
		eventList = (ListView) findViewById(R.id.calendar_eventList);
		cal = java.util.Calendar.getInstance();
		
		format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		db = new LocalDatabaseHelper(this);
		
		// Set the font
		header_title.setTypeface(font);
		header_settingText.setTypeface(font);
		header_menu.setTypeface(font);
		date.setTypeface(font);
		month.setTypeface(font);
		year.setTypeface(font);
		chineseDate.setTypeface(font);
		monthlyEvent.setTypeface(font);
		tsEvent.setTypeface(font);
		addEvent.setTypeface(font);
		noEvent.setTypeface(font);
		datePicker_year.setTypeface(font);
		
		datePicker_year.bringToFront();
		
		header_title.setText(R.string.mainmenu_calendar);
		DecimalFormat fomattter = new DecimalFormat("00");
		date.setText(fomattter.format(cal.get(java.util.Calendar.DAY_OF_MONTH)));
		month.setText(cal.getDisplayName(java.util.Calendar.MONTH, java.util.Calendar.SHORT, Locale.getDefault()));
		datePicker_year.setText(Integer.toString(cal.get(java.util.Calendar.YEAR)));												
		year.setText(Integer.toString(cal.get(java.util.Calendar.YEAR)));	
		
		AccessibilityManager am = (AccessibilityManager) getSystemService(ACCESSIBILITY_SERVICE);
		boolean isAccessibilityEnabled = am.isEnabled();
		
		if (isAccessibilityEnabled){
			datePicker_year.setText(this.getResources().getString(R.string.calendar_select_date));
			datePicker_date.setEnabled(false);
			datePicker_date.setVisibility(View.GONE);
			datePicker_month.setEnabled(false);
			datePicker_month.setVisibility(View.GONE);
		}else{
			datePicker_year.setText(Integer.toString(cal.get(java.util.Calendar.YEAR)));	
		}
		
		setDateOnMonthChange(cal.get(java.util.Calendar.MONTH));
		
		Calendar.this.datePicker_month.rotateButtons(-(360/12)*(cal.get(java.util.Calendar.MONTH)));
		Calendar.this.datePicker_date.rotateButtons(-(360/cal.getActualMaximum(java.util.Calendar.DAY_OF_MONTH))*(cal.get(java.util.Calendar.DAY_OF_MONTH)));
		
		new EventAsync(this).execute("");
		
		adapter = new EventListAdapter(this, db.getEventByDate(format.format(cal.getTime())));
		eventList.setAdapter(adapter);
		
		updateEventList();
		
		for (int i=0 ; i<datePicker_date.getChildCount() ; i++){
			nullCallBack((ImageView)datePicker_date.getChildAt(i));
		}
		for (int i=0 ; i<datePicker_month.getChildCount() ; i++){
			nullCallBack((ImageView)datePicker_month.getChildAt(i));
		}
		
		// Initialize the listener
		OnTouchListener onTouchListener = new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN){
					v.setAlpha(0.3f);
				}
				if (event.getAction() == MotionEvent.ACTION_UP){
					v.setAlpha(1f);
				}
				return false;
			}
		};
		
		OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()){
				case R.id.header_setting:
					Calendar.this.startActivity(new Intent(Calendar.this, Setting.class));
					break;
				case R.id.header_menu:
					setResult(2);
					Calendar.this.finish();	
					break;
				case R.id.calendar_monthlyEvent:
					Intent intent = new Intent(Calendar.this, CalendarMonthly.class);
					intent.putExtra("month", cal.get(java.util.Calendar.MONTH)+1);
					Calendar.this.startActivityForResult(intent, 1);
					break;
				case R.id.calendar_TSEvent:
					Intent intent_ts = new Intent(Calendar.this, CalendarMonthly.class);
					intent_ts.putExtra("month", 0);
					Calendar.this.startActivityForResult(intent_ts, 1);
					break;
				case R.id.calendar_datepicker_year:
					int y = cal.get(java.util.Calendar.YEAR);
					int m = cal.get(java.util.Calendar.MONTH);
					int d = cal.get(java.util.Calendar.DAY_OF_MONTH);
					DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {						
						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear,
								int dayOfMonth) {
							Calendar.this.datePicker_year.setText(Integer.toString(year));												
							Calendar.this.year.setText(Integer.toString(year));
							Calendar.this.month.setText(cal.getDisplayName(java.util.Calendar.MONTH, java.util.Calendar.SHORT, Locale.getDefault()));
							Calendar.this.date.setText(Integer.toString(dayOfMonth));
							
							cal.set(year, monthOfYear, dayOfMonth);		
							rotateCalendar(1, 1);
						}
					};					
					DatePickerDialog datePickerDialog = new DatePickerDialog(Calendar.this, onDateSetListener, y, m, d);
					datePickerDialog.setTitle(getResources().getString(R.string.calendar_dateDialog));
					datePickerDialog.show();
					break;
				default:
					break;
				}
				
			}
		};
		
		OnItemSelectedListener date_OnItemSelectedListener = new OnItemSelectedListener() {			
			@Override
			public void onItemSelected(View view, String name) {
				date.setText(name);
				
			}
		};
		
		OnRotationFinishedListener date_OnRotationFinishedListener = new OnRotationFinishedListener() {			
			@Override
			public void onRotationFinished(View view, String name) {
				cal.set(java.util.Calendar.DAY_OF_MONTH, Integer.parseInt(name));
				
//				int newChildCount = datePicker_date.getChildCount();
//				for (int i=0 ; i<datePicker_date.getChildCount(); i++){
//					newChildCount--;
//				}			
//				for (int i=0 ; i< newChildCount; i++){
//					((CircleImageView)datePicker_date.getChildAt(i)).setImageResource(date_img[i]);
//				}
//				((ImageView)view).setImageResource(date_selected_img[Integer.parseInt(name)-1]);				
//				
				updateEventList();
			}
		};
		
		OnItemSelectedListener month_OnItemSelectedListener = new OnItemSelectedListener() {			
			@Override
			public void onItemSelected(View view, String name) {
				month.setText(name);
				switch(view.getId()){
				case R.id.calendar_datepicker_month_01:
					setDateOnMonthChange(1);
					break;
				case R.id.calendar_datepicker_month_02:
					setDateOnMonthChange(2);
					break;
				case R.id.calendar_datepicker_month_03:
					setDateOnMonthChange(3);
					break;
				case R.id.calendar_datepicker_month_04:
					setDateOnMonthChange(4);
					break;
				case R.id.calendar_datepicker_month_05:
					setDateOnMonthChange(5);
					break;
				case R.id.calendar_datepicker_month_06:
					setDateOnMonthChange(6);
					break;
				case R.id.calendar_datepicker_month_07:
					setDateOnMonthChange(7);
					break;
				case R.id.calendar_datepicker_month_08:
					setDateOnMonthChange(8);
					break;
				case R.id.calendar_datepicker_month_09:
					setDateOnMonthChange(9);
					break;
				case R.id.calendar_datepicker_month_10:
					setDateOnMonthChange(10);
					break;
				case R.id.calendar_datepicker_month_11:
					setDateOnMonthChange(11);
					break;
				case R.id.calendar_datepicker_month_12:
					setDateOnMonthChange(12);
					break;
				default:
					break;
				}
				updateEventList();
			}
		};
		
		OnRotationFinishedListener month_OnRotationFinishedListener = new OnRotationFinishedListener() {			
			@Override
			public void onRotationFinished(View view, String name) {
			}
		};
		
		setting.setOnTouchListener(onTouchListener);
		header_menu.setOnTouchListener(onTouchListener);
		monthlyEvent.setOnTouchListener(onTouchListener);
		addEvent.setOnTouchListener(onTouchListener);
		tsEvent.setOnTouchListener(onTouchListener);
		datePicker_year.setOnTouchListener(onTouchListener);
		
		setting.setOnClickListener(onClickListener);
		header_menu.setOnClickListener(onClickListener);
		monthlyEvent.setOnClickListener(onClickListener);
		addEvent.setOnClickListener(onClickListener);	
		tsEvent.setOnClickListener(onClickListener);	
		datePicker_year.setOnClickListener(onClickListener);
		
		datePicker_date.setOnItemSelectedListener(date_OnItemSelectedListener);
		datePicker_date.setOnRotationFinishedListener(date_OnRotationFinishedListener);
		datePicker_month.setOnItemSelectedListener(month_OnItemSelectedListener);
		datePicker_month.setOnRotationFinishedListener(month_OnRotationFinishedListener);		
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 1){
			Calendar.this.finish();
		}
	}
	
	private void setDateOnMonthChange(int month){
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.set(java.util.Calendar.YEAR, cal.get(java.util.Calendar.YEAR));
		calendar.set(java.util.Calendar.MONTH, month-1);		
		
		int daysOfMonth = calendar.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);
		int currentDay = cal.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);
		
		if (currentDay < daysOfMonth){
			for (int i = currentDay+1 ; i <= daysOfMonth ; i++){
				if (datePicker_date.getChildAt(i-1).getVisibility() == View.GONE){
					datePicker_date.getChildAt(i-1).setVisibility(View.VISIBLE);
				}
//				Calendar.this.datePicker_date.rotateButtons(360/month);
			}
		}else if (currentDay > daysOfMonth){
			for (int i = currentDay ; i > daysOfMonth ; i--){
				if (datePicker_date.getChildAt(i-1).getVisibility() == View.VISIBLE){
					datePicker_date.getChildAt(i-1).setVisibility(View.GONE);
				}
				datePicker_date.rotateViewToCenter((CircleImageView) datePicker_date.getChildAt(0), true);
			}
//			cal.set(java.util.Calendar.DAY_OF_MONTH, 1);
//			date.setText("01");
		}		
		cal.set(java.util.Calendar.MONTH, month-1);
	}
	
	private void updateEventList(){
		((EventListAdapter)eventList.getAdapter()).setDate(db.getEventByDate(format.format(cal.getTime())));
		((EventListAdapter)eventList.getAdapter()).notifyDataSetChanged();
		if (((EventListAdapter)eventList.getAdapter()).getCount() > 0 ){
			eventList.setVisibility(View.VISIBLE);
			noEvent.setVisibility(View.GONE);
		}else{
			eventList.setVisibility(View.GONE);
			noEvent.setVisibility(View.VISIBLE);
		}
	}
	
	private void rotateCalendar(int date, int month){
		Log.i("rotate", "rotate");
		datePicker_month.rotateViewToCenter((CircleImageView) datePicker_month.getChildAt(month), true);
	}
	
	private void nullCallBack(ImageView imageView){
		Drawable d1 = imageView.getBackground();
		Drawable d2 = imageView.getDrawable();
		if (d1 != null)	{
			d1.setCallback(null);
			d1 = null;
		}
		if (d2 != null) {
			d2.setCallback(null);
			d2 = null;
		}		
	}
}
